package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.ReferenceRequest;
import ru.kpfu.itis.group11501.vitise.repository.ReferenceRequestRepository;
import ru.kpfu.itis.group11501.vitise.service.ReferenceRequestService;

import java.util.List;

/**
 * Created by Марат on 06.05.2017.
 */
@Service
public class ReferenceRequestServiceImpl implements ReferenceRequestService {
    private final ReferenceRequestRepository referenceRequestRepository;

    @Autowired
    public ReferenceRequestServiceImpl(ReferenceRequestRepository referenceRequestRepository) {
        this.referenceRequestRepository = referenceRequestRepository;
    }

    @Override
    public void add(ReferenceRequest referenceRequest) {
        referenceRequestRepository.save(referenceRequest);
    }

    @Override
    public void remove(ReferenceRequest referenceRequest) {
        referenceRequestRepository.delete(referenceRequest);
    }

    @Override
    public ReferenceRequest getOne(Long id) {
        return referenceRequestRepository.findOne(id);
    }

    @Override
    public List<ReferenceRequest> getAllActive() {
        return referenceRequestRepository.findByIsActiveTrue();
    }
}
