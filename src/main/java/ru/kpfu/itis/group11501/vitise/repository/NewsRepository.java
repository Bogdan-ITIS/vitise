package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.itis.group11501.vitise.model.News;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.List;

/**
 * Created by Марат on 22.03.2017.
 */
public interface NewsRepository extends JpaRepository<News, Long> {
    String query = "select DISTINCT n from News n, Colleagues c " +
            "where n.isPersonal = :is_personal or (c.isActive = true and " +
            "(c.receiver = :current_user and n.author = c.sender) " +
            "or (c.sender = :current_user and n.author = c.receiver))";

    @Query(query)
    List<News> findByIsPersonal(@Param("is_personal") boolean isPersonal, @Param("current_user") User currentUser);

    @Query(query)
    List<News> findByIsPersonal(@Param("is_personal") boolean isPersonal,
                                @Param("current_user") User currentUser,
                                Pageable pageRequest);


    List<News> findByAuthorId(Long id);
}
