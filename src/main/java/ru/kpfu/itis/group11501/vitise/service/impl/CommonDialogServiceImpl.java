package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.CommunicationRelation;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.conversation.Conversation;
import ru.kpfu.itis.group11501.vitise.model.enums.StatusName;
import ru.kpfu.itis.group11501.vitise.service.CommonDialogService;
import ru.kpfu.itis.group11501.vitise.service.CommunicationRelationService;
import ru.kpfu.itis.group11501.vitise.service.ConversationService;
import ru.kpfu.itis.group11501.vitise.util.messages.CommonDialog;
import ru.kpfu.itis.group11501.vitise.util.messages.CommunicationRelationAdapter;
import ru.kpfu.itis.group11501.vitise.util.messages.ConversationAdapter;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Author: Svintenok Kate
 * Date: 06.04.2017
 * Group: 11-501
 * Project: vITISe
 */
@Service
public class CommonDialogServiceImpl implements CommonDialogService {

    private final ConversationService conversationService;
    private final CommunicationRelationService relationService;
    private final Map<String, StatusName> roleFilter;
    private final Pattern commonDialogPattern = Pattern.compile("/topic/messages/(?<id>[1-9][0-9]*)");

    @Autowired
    public CommonDialogServiceImpl(ConversationService conversationService, CommunicationRelationService relationService) {
        this.conversationService = conversationService;
        this.relationService = relationService;
        this.roleFilter = new HashMap<>();
        roleFilter.put("from_students", StatusName.STUDENT);
        roleFilter.put("from_teachers", StatusName.TEACHER);
        roleFilter.put("from_deanery", StatusName.DEANERY);
    }

    @Override
    public List<CommonDialog> getDialogs(User user, String filter) {

        List<CommonDialog> dialogs = new ArrayList<>();

        if (filter.equals("all")) {
            for (Conversation conversation : conversationService.getConversations(user)) {
                dialogs.add(new ConversationAdapter(conversation));
            }
            for (CommunicationRelation relation : relationService.getAll(user)) {
                dialogs.add(new CommunicationRelationAdapter(relation));
            }
            Collections.sort(dialogs);
        } else if (filter.equals("unread")) {
            for (Conversation conversation : conversationService.getConversations(user)) {
                if (conversation.getNewMessagesCount() != 0)
                    dialogs.add(new ConversationAdapter(conversation));

            }
            for (CommunicationRelation relation : relationService.getAll(user)) {
                if (relation.getNewMessagesCount() != 0)
                    dialogs.add(new CommunicationRelationAdapter(relation));
            }
            Collections.sort(dialogs);
        } else if (roleFilter.containsKey(filter)) {
            for (CommunicationRelation relation : relationService.getAll(user, roleFilter.get(filter))) {
                dialogs.add(new CommunicationRelationAdapter(relation));
            }
        } else {
            return null;
        }
        return dialogs;
    }

    @Override
    public int newMessagesCount(User user) {
        return this.relationService.newMessagesCount(user) + conversationService.newMessagesCount(user);
    }

    @Override
    public Pattern getCommonDialogPattern() {
        return commonDialogPattern;
    }
}