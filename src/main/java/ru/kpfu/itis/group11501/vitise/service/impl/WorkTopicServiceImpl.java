package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.DirectionOfScientificActivity;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.WorkTopic;
import ru.kpfu.itis.group11501.vitise.model.WorkTopicRequest;
import ru.kpfu.itis.group11501.vitise.repository.UserRepository;
import ru.kpfu.itis.group11501.vitise.repository.WorkTopicRepository;
import ru.kpfu.itis.group11501.vitise.repository.WorkTopicRequestRepository;
import ru.kpfu.itis.group11501.vitise.service.UserService;
import ru.kpfu.itis.group11501.vitise.service.WorkTopicService;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 04.05.2017
 * Group: 11-501
 * Project: vITISe
 */
@Service
public class WorkTopicServiceImpl implements WorkTopicService {

    private final WorkTopicRepository topicRepository;
    private final WorkTopicRequestRepository requestRepository;
    private final UserService userService;
    private final UserRepository userRepository;

    @Autowired
    public WorkTopicServiceImpl(WorkTopicRepository topicRepository,
                                WorkTopicRequestRepository requestRepository,
                                UserService userService, UserRepository userRepository) {
        this.topicRepository = topicRepository;
        this.requestRepository = requestRepository;
        this.userService = userService;
        this.userRepository = userRepository;
    }


    @Override
    public void addTopic(WorkTopic topic) {
        topicRepository.save(topic);
    }

    @Override
    public WorkTopic getTopic(Long id) {
        return topicRepository.getOne(id);
    }

    @Override
    public List<WorkTopic> getAllFreeTopics(User currentUser) {
        List<WorkTopic> workTopics = topicRepository.findAllByBusyOrderByName(false);
        return this.setTopicsInfo(workTopics, currentUser);
    }

    @Override
    public List<WorkTopic> getAllFreeTopics(User currentUser, String name, User creator, DirectionOfScientificActivity direction) {
        List<WorkTopic> workTopics = topicRepository.findAllByNameAndCreatorAndDirection(name, creator, direction);
        return this.setTopicsInfo(workTopics, currentUser);
    }

    @Override
    public List<WorkTopic> getAllProfessorTopics(User user) {
        List<WorkTopic> workTopics = topicRepository.findAllByCreatorOrderByName(user);

        for (WorkTopic topic : workTopics) {
            if (topic.getBusy()) {
                topic.setStudent(requestRepository.findOneConfirmedByTopic(topic).getStudent());
            } else {
                topic.setActiveRequests(this.getTopicActiveRequests(topic));
            }
        }
        return workTopics;
    }

    @Override
    public List<User> getAllProfessors() {
        return userRepository.findAllTopicCreators();
    }

    @Override
    public void addTopicRequest(WorkTopicRequest topicRequest) {
        requestRepository.save(topicRequest);
    }

    @Override
    public WorkTopicRequest getTopicRequest(WorkTopic topic, User user) {
        return requestRepository.findOneByTopicIdAndStudentId(topic.getId(), user.getId());
    }

    @Override
    public List<WorkTopicRequest> getActiveStudentRequests(User user) {
        return requestRepository.findAllByStudentIdAndConfirmed(user.getId(), null);
    }

    @Override
    public List<WorkTopicRequest> getStudentRequests(User user) {
        return requestRepository.findAllByStudentIdOrderByDateAsc(user.getId());
    }

    @Override
    public WorkTopicRequest getConfirmedStudentRequest(User user) {
        return requestRepository.findOneConfirmedByStudent(user);
    }

    @Override
    public boolean isStudentHaveTopic(User user) {
        return this.getConfirmedStudentRequest(user) != null;
    }

    @Override
    public WorkTopicRequest getTopicRequest(Long id) {
        return requestRepository.findOne(id);
    }

    @Override
    public void approveRequest(WorkTopicRequest topicRequest) {
        topicRequest.setConfirmed(true);
        requestRepository.save(topicRequest);

        WorkTopic workTopic = topicRequest.getTopic();
        for (WorkTopicRequest request : this.getTopicActiveRequests(workTopic)) {
            request.setConfirmed(false);
            requestRepository.save(request);
        }

        for (WorkTopicRequest request : this.getActiveStudentRequests(topicRequest.getStudent())) {
            request.setConfirmed(false);
            requestRepository.save(request);
        }

        workTopic.setBusy(true);
        topicRepository.save(workTopic);
    }

    @Override
    public void rejectRequest(WorkTopicRequest topicRequest) {
        topicRequest.setConfirmed(false);
        requestRepository.save(topicRequest);
    }

    @Override
    public List<WorkTopicRequest> getActiveProfessorRequests(User user) {
        return requestRepository.findAllActiveByTopicCreatorId(user);
    }

    @Override
    public void deleteTopicRequest(WorkTopicRequest topicRequest) {
        requestRepository.delete(topicRequest);
    }

    private List<WorkTopicRequest> getTopicActiveRequests(WorkTopic topic) {
        return requestRepository.findAllByTopicIdAndConfirmedOrderByDateAsc(topic.getId(), null);
    }

    private List<WorkTopic> setTopicsInfo(List<WorkTopic> workTopics, User currentUser) {
        for (WorkTopic topic : workTopics)
            topic.setActiveRequests(this.getTopicActiveRequests(topic));
        if (userService.isStudent(currentUser)) {
            for (WorkTopic topic : workTopics)
                topic.setCurrentUserRequest(this.getTopicRequest(topic, currentUser));
        }
        return workTopics;
    }
}