package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.vitise.model.ReferenceRequest;

import java.util.List;

/**
 * Created by Марат on 06.05.2017.
 */
public interface ReferenceRequestRepository extends JpaRepository<ReferenceRequest, Long> {
    List<ReferenceRequest> findByIsActiveTrue();
}
