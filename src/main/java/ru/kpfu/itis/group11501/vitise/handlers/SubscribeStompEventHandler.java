package ru.kpfu.itis.group11501.vitise.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import ru.kpfu.itis.group11501.vitise.model.CommunicationRelation;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.conversation.Conversation;
import ru.kpfu.itis.group11501.vitise.pojo.StompSession;
import ru.kpfu.itis.group11501.vitise.service.CommonDialogService;
import ru.kpfu.itis.group11501.vitise.service.CommunicationRelationService;
import ru.kpfu.itis.group11501.vitise.service.ConversationService;
import ru.kpfu.itis.group11501.vitise.service.StompSessionService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Bogdan Popov on 04.05.2017.
 */
@Component
public class SubscribeStompEventHandler implements ApplicationListener<SessionSubscribeEvent> {
    private final Pattern dialogPattern;
    private final Pattern commonDialogPattern;
    private final Pattern conversationPattern;
    @Autowired
    @Qualifier("clientOutboundChannel")
    private MessageChannel clientOutboundChannel;
    private CommunicationRelationService communicationRelationService;
    private ConversationService conversationService;
    private StompSessionService stompSessionService;
    private CommonDialogService commonDialogService;


    @Autowired
    public SubscribeStompEventHandler(CommunicationRelationService communicationRelationService,
                                      ConversationService conversationService, StompSessionService stompSessionService,
                                      CommonDialogService commonDialogService) {
        this.communicationRelationService = communicationRelationService;
        this.conversationService = conversationService;
        this.dialogPattern = communicationRelationService.getDialogPattern();
        this.stompSessionService = stompSessionService;
        this.commonDialogService = commonDialogService;
        this.commonDialogPattern = commonDialogService.getCommonDialogPattern();
        this.conversationPattern = conversationService.getConversationPattern();
    }

    @Override
    public void onApplicationEvent(SessionSubscribeEvent event) {
        StompHeaderAccessor headers = StompHeaderAccessor.wrap(event.getMessage());
        String destination = headers.getDestination();
        String sessionId = headers.getSessionId();
        User user = (User) ((UsernamePasswordAuthenticationToken) headers.getUser()).getPrincipal();
        Matcher dialogMatcher = dialogPattern.matcher(destination);
        Matcher commonDialogMatcher = commonDialogPattern.matcher(destination);
        Matcher conversationMatcher = conversationPattern.matcher(destination);
        if (dialogMatcher.matches()) {
            CommunicationRelation communicationRelation = communicationRelationService.getOne(Long.valueOf(dialogMatcher.group("id")));
            if (!communicationRelationService.hasAccessToChat(user, communicationRelation)) {
                abortSubscription(sessionId, "You haven't access to this dialog");
            } else {
                StompSession stompSession = stompSessionService.createSession(sessionId, destination, user);
                stompSessionService.addSession(stompSession);
            }
        } else if (commonDialogMatcher.matches()) {
            Long id = Long.valueOf(commonDialogMatcher.group("id"));
            if (!id.equals(user.getId())) {
                abortSubscription(sessionId, "You can receive only yours messages");
            } else {
                StompSession stompSession = stompSessionService.createSession(sessionId, destination, user);
                stompSessionService.addSession(stompSession);
            }
        } else if (conversationMatcher.matches()) {
            Conversation conversation = conversationService.getConversation(Long.valueOf(conversationMatcher.group("id")));
            if (conversationService.getMember(user, conversation) == null) {
                abortSubscription(sessionId, "You are not a participant of this conversation");
            } else {
                StompSession stompSession = stompSessionService.createSession(sessionId, destination, user);
                stompSessionService.addSession(stompSession);
            }
        }
    }

    private void abortSubscription(String sessionId, String abortMessage) {
        StompHeaderAccessor headerAccessor = StompHeaderAccessor.create(StompCommand.ABORT);
        headerAccessor.setMessage(abortMessage);
        headerAccessor.setSessionId(sessionId);
        this.clientOutboundChannel.send(MessageBuilder.createMessage(new byte[0], headerAccessor.getMessageHeaders()));
    }
}
