package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.Course;
import ru.kpfu.itis.group11501.vitise.model.Subject;
import ru.kpfu.itis.group11501.vitise.model.TeachersSubjects;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.List;

/**
 * Created by Наталья on 04.05.2017.
 */
public interface CourseService {
    void add(Course course, Subject subject, TeachersSubjects teachersSubjects);

    List<Course> getCourses();

    Course getOne(Long id);

    List<User> getTeachers(Subject subject);

    List<User> getStudents(Course course);

    List<Course> getMyCourses(User currentUser);
}

