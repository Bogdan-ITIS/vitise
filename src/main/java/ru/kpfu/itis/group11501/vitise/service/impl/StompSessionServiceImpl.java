package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.CommunicationRelation;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.conversation.Conversation;
import ru.kpfu.itis.group11501.vitise.model.conversation.ConversationMember;
import ru.kpfu.itis.group11501.vitise.pojo.StompSession;
import ru.kpfu.itis.group11501.vitise.repository.StompSessionRepository;
import ru.kpfu.itis.group11501.vitise.service.CommunicationRelationService;
import ru.kpfu.itis.group11501.vitise.service.ConversationService;
import ru.kpfu.itis.group11501.vitise.service.StompSessionService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Bogdan Popov on 05.05.2017.
 */
@Service
public class StompSessionServiceImpl implements StompSessionService {
    private StompSessionRepository stompSessionRepository;
    private CommunicationRelationService communicationRelationService;
    private ConversationService conversationService;

    @Autowired
    public StompSessionServiceImpl(StompSessionRepository stompSessionRepository, ConversationService conversationService,
                                   CommunicationRelationService communicationRelationService) {
        this.stompSessionRepository = stompSessionRepository;
        this.communicationRelationService = communicationRelationService;
        this.conversationService = conversationService;
    }

    @Override
    public void addSession(StompSession stompSession) {
        stompSessionRepository.addSession(stompSession);
    }

    @Override
    public void deleteSession(String sessionId) {
        stompSessionRepository.deleteSession(sessionId);
    }

    @Override
    public StompSession getSession(String sessionId) {
        return stompSessionRepository.getStompSession(sessionId);
    }

    @Override
    public List<Matcher> getDestinationsMatcherByPattern(String sessionId, Pattern pattern) {
        Set<String> set = getSession(sessionId).getDestinations();
        if (set == null) {
            return new ArrayList<>();
        } else {
            List<Matcher> destinationList = new ArrayList<>();
            for (String destination : set) {
                Matcher matcher = pattern.matcher(destination);
                if (matcher.matches()) {
                    destinationList.add(matcher);
                }
            }
            return destinationList;
        }
    }

    @Override
    public void updateMessagingForSession(String sessionId) {
        StompSession stompSession = getSession(sessionId);
        if (stompSession != null) {
            Pattern dialogPattern = communicationRelationService.getDialogPattern();
            Pattern conversationPattern = conversationService.getConversationPattern();
            List<Matcher> dialogMatcherList = getDestinationsMatcherByPattern(sessionId, dialogPattern);
            List<Matcher> conversationMatcherList = getDestinationsMatcherByPattern(sessionId, conversationPattern);
            for (Matcher matcher : dialogMatcherList) {
                String idString = matcher.group("id");
                Long id = Long.valueOf(idString);
                CommunicationRelation communicationRelation = communicationRelationService.getOne(id);
                communicationRelationService.updateReadingLog(communicationRelation, stompSession.getUser());
            }
            for (Matcher matcher : conversationMatcherList) {
                String idString = matcher.group("id");
                Long id = Long.valueOf(idString);
                Conversation conversation = conversationService.getConversation(id);
                ConversationMember conversationMember = conversationService.getMember(stompSession.getUser(), conversation);
                conversationService.updateReadingLog(conversationMember);
            }
        }
    }

    @Override
    public StompSession createSession(String sessionId, String destination, User user) {
        StompSession stompSession = new StompSession();
        stompSession.setSessionId(sessionId);
        Set<String> destinations = new HashSet<>();
        destinations.add(destination);
        stompSession.setDestinations(destinations);
        stompSession.setUser(user);
        return stompSession;
    }

    @Override
    public void deleteDestination(String sessionId, String destination) {
        Set<String> destinations = stompSessionRepository.getStompSession(sessionId).getDestinations();
        destinations.remove(destination);
    }

    @Override
    public void updateMessagingForSession(String sessionId, String destination) {
        StompSession stompSession = getSession(sessionId);
        if (stompSession != null) {
            Pattern dialogPattern = communicationRelationService.getDialogPattern();
            Pattern conversationPattern = conversationService.getConversationPattern();
            Matcher matcherDialog = dialogPattern.matcher(destination);
            Matcher matcherConversation = conversationPattern.matcher(destination);
            if (matcherDialog.matches()) {
                stompSession.getDestinations().remove(destination);
                CommunicationRelation communicationRelation = communicationRelationService.getOne(Long.valueOf(matcherDialog.group("id")));
                communicationRelationService.updateReadingLog(communicationRelation, stompSession.getUser());
            } else if (matcherConversation.matches()) {
                stompSession.getDestinations().remove(destination);
                Conversation conversation = conversationService.getConversation(Long.valueOf(matcherConversation.group("id")));
                ConversationMember conversationMember = conversationService.getMember(stompSession.getUser(), conversation);
                conversationService.updateReadingLog(conversationMember);
            }
        }
    }

    @Override
    public List<User> getAllUser(String destination) {
        return stompSessionRepository.getAllUser(destination);
    }
}
