package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.News;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.repository.NewsRepository;
import ru.kpfu.itis.group11501.vitise.service.NewsService;
import ru.kpfu.itis.group11501.vitise.service.UserService;

import java.util.List;

/**
 * Created by Марат on 22.03.2017.
 */
@Service
public class NewsServiceImpl implements NewsService {
    private static final int PAGE_SIZE = 10;
    private final UserService userService;
    private final NewsRepository newsRepository;

    @Autowired
    public NewsServiceImpl(NewsRepository newsRepository, UserService userService) {
        this.newsRepository = newsRepository;
        this.userService = userService;
    }

    @Override
    public void add(News news) {
        newsRepository.save(news);
    }

    @Override
    public void remove(News news) {
        newsRepository.delete(news);
    }

    @Override
    public News get(Long id) {
        return newsRepository.getOne(id);
    }


    @Override
    public List<News> getAllByAuthor(User author) {
        return newsRepository.findByAuthorId(author.getId());
    }

    @Override
    public List<News> getAllPublicNews(User currentUser) {
        return newsRepository.findByIsPersonal(false, currentUser);
    }

    @Override
    public List<News> getAllPublicNews(User currentUser, int page) {
        PageRequest pageRequest = new PageRequest(page - 1, PAGE_SIZE, Sort.Direction.DESC, "pubDate");
        List<News> newsList = newsRepository.findByIsPersonal(false, currentUser, pageRequest);
        for (News news : newsList)
            userService.setStatus(news.getAuthor());
        return newsList;
    }

    @Override
    public int getLastPagePublicNewsNumber(User currentUser) {
        return (int) Math.ceil((double) newsRepository.findByIsPersonal(false, currentUser).size() / PAGE_SIZE);
    }
}
