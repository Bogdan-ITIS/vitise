package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.*;
import ru.kpfu.itis.group11501.vitise.repository.CourseRepository;
import ru.kpfu.itis.group11501.vitise.repository.CoursesRequestsRepository;
import ru.kpfu.itis.group11501.vitise.repository.SubjectRepository;
import ru.kpfu.itis.group11501.vitise.repository.TeachersSubjectsRepository;
import ru.kpfu.itis.group11501.vitise.service.CourseService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Наталья on 04.05.2017.
 */
@Service
public class CourseServiceImpl implements CourseService {
    private final CourseRepository courseRepository;
    private final TeachersSubjectsRepository teachersSubjectsRepository;
    private final SubjectRepository subjectRepository;
    private final CoursesRequestsRepository coursesRequestsRepository;

    @Autowired
    public CourseServiceImpl(CourseRepository courseRepository, TeachersSubjectsRepository teachersSubjectsRepository, SubjectRepository subjectRepository, CoursesRequestsRepository coursesRequestsRepository) {
        this.courseRepository = courseRepository;
        this.teachersSubjectsRepository = teachersSubjectsRepository;
        this.subjectRepository = subjectRepository;
        this.coursesRequestsRepository = coursesRequestsRepository;
    }

    @Override
    public void add(Course course, Subject subject, TeachersSubjects teachersSubjects) {
        subject = subjectRepository.save(subject);
        teachersSubjects.setSubject(subject);
        teachersSubjectsRepository.save(teachersSubjects);
        course.setSubject(subject);
        courseRepository.save(course);
    }

    @Override
    public List<Course> getCourses() {
        return courseRepository.findAll();
    }

    @Override
    public Course getOne(Long id) {
        return courseRepository.findOne(id);
    }

    @Override
    public List<User> getTeachers(Subject subject) {
        List<TeachersSubjects> list = teachersSubjectsRepository.findAllBySubjectId(subject.getId());
        List<User> userList = new ArrayList<>();
        for (TeachersSubjects teachersSubjects : list) {
            userList.add(teachersSubjects.getTeacher());
        }
        return userList;
    }

    @Override
    public List<User> getStudents(Course course) {
        List<CoursesRequests> requestsList = coursesRequestsRepository.findAllByCourseAndIsActive(course, true);
        List<User> list = new ArrayList<>();
        for (CoursesRequests labReq : requestsList) {
            list.add(labReq.getUser());
        }
        return list;
    }

    @Override
    public List<Course> getMyCourses(User currentUser) {
        List<CoursesRequests> list = coursesRequestsRepository.findAllByIsActiveAndUser(true, currentUser);
        List<Course> courses = new ArrayList<>();
        for (CoursesRequests request : list) {
            courses.add(request.getCourse());
        }
        return courses;
    }
}
