package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.CommunicationRelation;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.enums.StatusName;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Наталья on 06.04.2017.
 */
public interface CommunicationRelationService {

    void createCommunicationRelations(User user, User currentUser);

    List<CommunicationRelation> getAll(User user);

    List<CommunicationRelation> getAll(User user, StatusName status);

    CommunicationRelation getOne(Long id);

    CommunicationRelation getOne(User user, User currentUser);

    CommunicationRelation getOneWithInfo(Long id, User user);

    void updateReadingLog(CommunicationRelation relation, User user);

    int newMessagesCount(User user);

    boolean hasAccessToChat(User user, CommunicationRelation communicationRelation);

    Pattern getDialogPattern();
}
