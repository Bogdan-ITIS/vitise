package ru.kpfu.itis.group11501.vitise.model;

import javax.persistence.*;

/**
 * Created by Наталья on 04.05.2017.
 */
@Entity
@Table(name = "courses_requests")
public class CoursesRequests {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "coursesid")
    private Course course;

    @ManyToOne
    @JoinColumn(name = "usersid")
    private User user;

    @Column(name = "is_active")
    private Boolean isActive;

    public CoursesRequests() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
