package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.itis.group11501.vitise.model.DirectionOfScientificActivity;
import ru.kpfu.itis.group11501.vitise.model.Status;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {

    User findOneByEmail(String email);

    @Query("select user from User user WHERE user.isActive is null and " +
            "exists (select us from UsersStatus us where us.status = :status and us.user = user)")
    List<User> findNewByStatus(@Param("status") Status status);

    List<User> findAllByIsActive(Boolean isActive);

    User findOneByToken(String token);


    @Query("select user from User user WHERE " +
            "exists (select topic from WorkTopic topic where topic.creator = user)")
    List<User> findAllTopicCreators();

    @Query("select distinct u from User u, DirectionOfScientificActivityUser d, UsersStatus us where d.user = u " +
            "and d.directionOfScientificActivity.id = :dir and us.user = u and us.status = :status and u.isActive = :isActive")
    List<User> findAllByDirectionAndStatusAndIsActive(@Param("dir") Long directionId,
                                        @Param("status") Status status, @Param("isActive") Boolean isActive);

    @Query("select distinct u from User u, UsersStatus us where us.user = u and us.status = :status")
    List<User> findAllByStatus(@Param("status") Status status);
}
