package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.Laboratory;
import ru.kpfu.itis.group11501.vitise.model.LaboratoryRequests;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.repository.LaboratoryRequestsRepository;
import ru.kpfu.itis.group11501.vitise.service.LaboratoryRequestsService;
import ru.kpfu.itis.group11501.vitise.service.UserService;

import java.util.List;

/**
 * Created by Наталья on 05.05.2017.
 */
@Service
public class LaboratoryRequestsServiceImpl implements LaboratoryRequestsService {
    private final LaboratoryRequestsRepository laboratoryRequestsRepository;
    private final UserService userService;

    @Autowired
    public LaboratoryRequestsServiceImpl(LaboratoryRequestsRepository laboratoryRequestsRepository, UserService userService) {
        this.laboratoryRequestsRepository = laboratoryRequestsRepository;
        this.userService = userService;
    }

    @Override
    public LaboratoryRequests getRequest(Laboratory laboratory, User user) {
        try {
            return laboratoryRequestsRepository.findOneByLaboratoryAndUser(laboratory, user);
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public int getState(LaboratoryRequests laboratoryRequests, User currentUser) {
        if (!existsRequest(currentUser)) {
            if (laboratoryRequests == null) {
                return 0;
            } else if (laboratoryRequests.getActive() == null) {
                return 1;
            } else if (laboratoryRequests.getActive()) {
                return 2;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }

    private boolean existsRequest(User currentUser) {
        try {
            LaboratoryRequests laboratoryRequests = laboratoryRequestsRepository.findAnyRequest(currentUser);
            return laboratoryRequests != null;
        } catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public void delete(Long id) {
        laboratoryRequestsRepository.delete(id);
    }

    @Override
    public void add(User currentUser, Laboratory laboratory) {
        LaboratoryRequests request = new LaboratoryRequests();
        request.setActive(null);
        request.setUser(currentUser);
        request.setLaboratory(laboratory);
        laboratoryRequestsRepository.save(request);
    }

    @Override
    public LaboratoryRequests getRequest(Long id) {
        return laboratoryRequestsRepository.findOne(id);
    }

    @Override
    public void update(LaboratoryRequests labRequest) {
        laboratoryRequestsRepository.save(labRequest);
    }

    @Override
    public List<LaboratoryRequests> getRequests() {
        List<LaboratoryRequests> list = laboratoryRequestsRepository.findAllByIsActive(null);
        for (LaboratoryRequests request : list) {
            userService.setStatus(request.getUser());
        }
        return list;
    }
}
