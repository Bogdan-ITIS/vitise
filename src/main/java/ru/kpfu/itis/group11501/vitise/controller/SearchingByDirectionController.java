package ru.kpfu.itis.group11501.vitise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.itis.group11501.vitise.model.Course;
import ru.kpfu.itis.group11501.vitise.model.DirectionOfScientificActivity;
import ru.kpfu.itis.group11501.vitise.model.Laboratory;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.enums.SelectType;
import ru.kpfu.itis.group11501.vitise.util.SearchingResponse;
import ru.kpfu.itis.group11501.vitise.service.CourseService;
import ru.kpfu.itis.group11501.vitise.service.DirectionOfScientificActivityService;
import ru.kpfu.itis.group11501.vitise.service.LaboratoryService;
import ru.kpfu.itis.group11501.vitise.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bogdan Popov on 12.05.2017.
 */

@Controller
public class SearchingByDirectionController {
    private final CourseService courseService;
    private final LaboratoryService laboratoryService;
    private final UserService userService;
    private final DirectionOfScientificActivityService directionOfScientificActivityService;

    @Autowired
    public SearchingByDirectionController(CourseService courseService, LaboratoryService laboratoryService, UserService userService, DirectionOfScientificActivityService directionOfScientificActivityService) {
        this.courseService = courseService;
        this.laboratoryService = laboratoryService;
        this.userService = userService;
        this.directionOfScientificActivityService = directionOfScientificActivityService;
    }

    @RequestMapping("/search")
    public String searchEntities(Model model) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        SelectType [] entities = new SelectType [] {SelectType.LABORATORY, SelectType.LECTURER};
        model.addAttribute("entities", entities);
        List<DirectionOfScientificActivity> directionOfScientificActivities
                = directionOfScientificActivityService.findAll();
        model.addAttribute("directions", directionOfScientificActivities);
        model.addAttribute("currentUser", currentUser);
        return "searching_by_direction";
    }

    @RequestMapping("/search/{entity}/{direction}")
    @ResponseBody
    public List<SearchingResponse> searchByDirection(@PathVariable(name = "entity") String entity,
                                    @PathVariable(name = "direction", required = false) String direction) {
        List<SearchingResponse> searchingResponses = new ArrayList<>();
        if (entity.equals(SelectType.LECTURER.getSimpleName())) {
            List<User> users =  userService.getLecturersByDirection(direction);
            searchingResponses = userService.getSearchingResponses(users, SelectType.LECTURER);
        }
        else if (entity.equals(SelectType.LABORATORY.getSimpleName())) {
            List<Laboratory> laboratories = laboratoryService.getLaboratoriesByDirection(direction);
            searchingResponses = laboratoryService.getSearchingResponses(laboratories, SelectType.LABORATORY);
        }
        return searchingResponses;
    }
}
