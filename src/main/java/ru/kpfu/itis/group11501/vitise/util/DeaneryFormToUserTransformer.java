package ru.kpfu.itis.group11501.vitise.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.function.Function;

/**
 * Created by Наталья on 28.04.2017.
 */
public class DeaneryFormToUserTransformer implements Function<DeaneryForm, User> {

    private static final PasswordEncoder encoder = new BCryptPasswordEncoder();

    private String firstUpperCase(String word) {
        if (word == null || word.isEmpty()) return "";
        return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
    }

    @Override
    public User apply(DeaneryForm deaneryForm) {
        String password = PasswordGenerator.generate();
        User user = new User();
        user.setName(firstUpperCase(deaneryForm.getName()));
        user.setSurname(firstUpperCase(deaneryForm.getSurname()));
        user.setEmail(deaneryForm.getEmail());
        user.setPassword(encoder.encode(password));
        user.setThirdName(firstUpperCase(deaneryForm.getThirdName()));
        user.setTelephoneNumber(deaneryForm.getTelephoneNumber());
        user.setPassMustBeChanged(false);
        user.setActive(true);
        user.setPhotoName("mock");
        user.setGeneratingPassword(password);
        user.setConfirmed(true);
        return user;
    }

}
