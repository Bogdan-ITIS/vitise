package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.itis.group11501.vitise.model.Laboratory;

import java.util.List;

/**
 * Created by Наталья on 04.05.2017.
 */
public interface LaboratoryRepository extends JpaRepository<Laboratory, Long> {

    @Query("select l from Laboratory l where l.direction.id = :idDirection")
    List<Laboratory> findAllByDirection(@Param("idDirection") Long idDirection);
}
