package ru.kpfu.itis.group11501.vitise.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Author: Svintenok Kate
 * Date: 04.05.2017
 * Group: 11-501
 * Project: vITISe
 */
@Entity
@Table(name = "topic_request")
public class WorkTopicRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private Boolean confirmed;

    @ManyToOne
    @JoinColumn(name = "topic_id")
    private WorkTopic topic;

    @ManyToOne
    @JoinColumn(name = "student_id")
    private User student;

    @Column(name = "date", insertable = false)
    private Date date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(Boolean confirmed) {
        this.confirmed = confirmed;
    }

    public WorkTopic getTopic() {
        return topic;
    }

    public void setTopic(WorkTopic topic) {
        this.topic = topic;
    }

    public User getStudent() {
        return student;
    }

    public void setStudent(User student) {
        this.student = student;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}