package ru.kpfu.itis.group11501.vitise.repository;

import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.pojo.StompSession;

import java.util.List;

/**
 * Created by Bogdan Popov on 05.05.2017.
 */
public interface StompSessionRepository {
    void addSession(StompSession stompSession);

    void deleteSession(String sessionId);

    StompSession getStompSession(String sessionId);

    List<User> getAllUser(String destination);

    boolean containsDestination(Long id, String destination);
}
