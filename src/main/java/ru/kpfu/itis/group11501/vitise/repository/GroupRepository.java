package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.itis.group11501.vitise.model.Group;
import ru.kpfu.itis.group11501.vitise.model.Semester;

import java.util.List;

public interface GroupRepository extends JpaRepository<Group, Long> {
    Group findOneByName(String name);

    @Query("select g from Group g where g.semester = :semOne or g.semester = :semTwo")
    List<Group> getGroupBySemester(@Param("semOne") Semester semOne, @Param("semTwo") Semester semTwo);

}
