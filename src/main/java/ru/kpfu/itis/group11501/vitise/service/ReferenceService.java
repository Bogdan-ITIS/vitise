package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.Reference;

/**
 * Created by Марат on 06.05.2017.
 */
public interface ReferenceService {
    void add(Reference reference);

    Reference getOne(Long id);

    void remove(Reference reference);
}
