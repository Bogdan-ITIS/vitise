package ru.kpfu.itis.group11501.vitise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.conversation.Conversation;
import ru.kpfu.itis.group11501.vitise.model.conversation.ConversationMember;
import ru.kpfu.itis.group11501.vitise.model.conversation.ConversationMessage;
import ru.kpfu.itis.group11501.vitise.model.conversation.ConversationMessageFile;
import ru.kpfu.itis.group11501.vitise.model.conversation.enums.ActiveStatusName;
import ru.kpfu.itis.group11501.vitise.pojo.Message;
import ru.kpfu.itis.group11501.vitise.pojo.ResponseMessage;
import ru.kpfu.itis.group11501.vitise.pojo.enums.MessagingType;
import ru.kpfu.itis.group11501.vitise.service.ConversationMessageFileService;
import ru.kpfu.itis.group11501.vitise.service.ConversationService;
import ru.kpfu.itis.group11501.vitise.service.StompSessionService;
import ru.kpfu.itis.group11501.vitise.service.UserService;
import ru.kpfu.itis.group11501.vitise.util.FileUploader;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Author: Svintenok Kate
 * Date: 29.03.2017
 * Group: 11-501
 * Project: vITISe
 */
@Controller
public class ConversationController {
    private final UserService userService;
    private final ConversationService conversationService;
    private final ConversationMessageFileService conversationMessageFileService;
    private final SimpMessagingTemplate messagingTemplate;
    private final StompSessionService stompSessionService;

    @Autowired
    public ConversationController(UserService userService,
                                  ConversationService conversationService, ConversationMessageFileService conversationMessageFileService, SimpMessagingTemplate messagingTemplate, StompSessionService stompSessionService) {
        this.userService = userService;
        this.conversationService = conversationService;
        this.conversationMessageFileService = conversationMessageFileService;
        this.messagingTemplate = messagingTemplate;
        this.stompSessionService = stompSessionService;
    }

    @RequestMapping(value = "/messages/conversation/create", method = RequestMethod.POST)
    public String createConversation(@RequestParam(name = "conversation_name") String name,
                                     @RequestParam(name = "members", required = false) Long[] membersId) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<User> members = new ArrayList<>();
        System.out.println("----------------------------------------------" + name);
        if (membersId != null) {
            for (Long id : membersId)
                members.add(userService.getUser(id));
            Conversation conversation = conversationService.createConversation(name, currentUser, members);
            return "redirect:/messages/conversation/" + conversation.getId();
        }
        return "redirect:/messages/all";
    }

    @RequestMapping(value = "/messages/conversation/{id}")
    public String conversationPage(@PathVariable("id") Long id, Model model) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (conversationService.getMember(currentUser, conversationService.getConversation(id)) == null)
            return "handle404";
        Conversation conversation = conversationService.getConversationWithInfo(id, currentUser);
        ConversationMember member = conversationService.getMember(currentUser, conversation);
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("conversation", conversation);
        model.addAttribute("message_form", new ConversationMessage());
        model.addAttribute("available_users", conversationService.getAvailableUsers(conversation));

        conversationService.updateReadingLog(member);
        return "conversation";
    }

    @MessageMapping("/conversation/send/{id}")
    public void conversationSendMessage(@DestinationVariable Long id,
                                        Message message, Principal principal) {

        Conversation conversation = conversationService.getConversation(id);
        User currentUser = (User) ((UsernamePasswordAuthenticationToken) principal).getPrincipal();
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault());
        ConversationMessage conversationMessage = new ConversationMessage();
        conversationMessage.setText(message.getContent());
        conversationMessage.setUser(currentUser);
        conversationMessage.setConversation(conversation);
        conversationService.addMessage(conversationMessage);
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setNameMessaging(conversation.getName());
        responseMessage.setType(MessagingType.CONVERSATION);
        responseMessage.setDate(dateFormat.format(date));
        responseMessage.setSender(userService.convertToPOJO(currentUser));
        responseMessage.setContent(message.getContent());
        responseMessage.setIdMessaging(String.valueOf(conversation.getId()));
        String destination = "/topic/conversation/" + id;
        messagingTemplate.convertAndSend(destination, responseMessage);
        List<User> conversationMembers = conversationService.getMembers(conversation);
        List<User> users = stompSessionService.getAllUser(destination);
        for (User user : users) {
            conversationService.updateReadingLog(conversationService.getMember(user, conversation));
        }
        conversationMembers.removeAll(users);
        for (User conversationMember : conversationMembers) {
            messagingTemplate.convertAndSend("/topic/messages/" + conversationMember.getId(), responseMessage);
        }
    }

    @RequestMapping(value = "/messages/conversation/{id}/upload")
    public String uploadFile(@PathVariable(name = "id") Long id,
                             @RequestParam(value = "file", required = false) MultipartFile file,
                             HttpServletRequest request) {
        User user = (User) ((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getPrincipal();
        Conversation conversation = conversationService.getConversation(id);
        ConversationMessage message = conversationService.getLastMessageByUserInConversation(user, conversation);
        ConversationMessageFile conversationMessageFile = new ConversationMessageFile();
        conversationMessageFile.setConversationMessage(message);
        String name = FileUploader.uploadFile(file);
        conversationMessageFile.setFilename(name);
        conversationMessageFileService.add(conversationMessageFile);
        return "redirect:" + request.getHeader("Referer");
    }

    @RequestMapping(value = "/messages/conversation/{id}/change", method = RequestMethod.POST)
    public String conversationChange(@PathVariable("id") Long id,
                                     @RequestParam(name = "name") String name,
                                     @RequestParam(name = "users_id") Long[] usersId) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Conversation conversation = conversationService.getConversationWithInfo(id, currentUser);
        conversation.setName(name);
        conversationService.saveConversation(conversation);

        for (User user : this.needToDelete(conversation.getMembers(), usersId)) {
            ConversationMember member = conversationService.getMember(user, conversation);
            conversationService.memberChangeStatus(member, ActiveStatusName.DELETED);
        }
        return "redirect:/messages/conversation/" + id;
    }

    @RequestMapping(value = "/messages/conversation/{id}/add", method = RequestMethod.POST)
    public String conversationAddMember(@PathVariable("id") Long id,
                                        @RequestParam(name = "users_id") Long[] usersId) {
        if (usersId != null) {
            for (Long userId : usersId)
                conversationService.addMember(conversationService.getConversation(id), userService.getUser(userId));
        }
        return "redirect:/messages/conversation/" + id;
    }

    @RequestMapping(value = "/messages/conversation/{id}/change_status", method = RequestMethod.POST)
    public String conversationChangeStatus(@PathVariable("id") Long id,
                                           @RequestParam(name = "user_id") Long userId) {
        ConversationMember member =
                conversationService.getMember(userService.getUser(userId), conversationService.getConversation(id));
        if (member.getActiveStatusName() == ActiveStatusName.ACTIVE)
            conversationService.memberChangeStatus(member, ActiveStatusName.LEFT_OUT);
        else
            conversationService.memberChangeStatus(member, ActiveStatusName.ACTIVE);
        return "redirect:/messages/conversation/" + id;
    }

    private List<User> needToDelete(List<User> members, Long[] usersId) {
        List<User> membersToDelete = new ArrayList<>(members);
        for (User member : members) {
            for (Long userId : usersId)
                if (member.getId().equals(userId))
                    membersToDelete.remove(member);
        }
        return membersToDelete;
    }
}