package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.Status;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.enums.SelectType;
import ru.kpfu.itis.group11501.vitise.model.enums.StatusName;
import ru.kpfu.itis.group11501.vitise.util.SearchingResponse;
import ru.kpfu.itis.group11501.vitise.pojo.UserPOJO;

import java.util.List;

public interface UserService {

    void save(User user);

    User getUser(String email);

    User getUser(Long id);

    User findOne(Long id);

    User getAdmin();

    List<Status> getStatus(User user);

    List getNewUsers();

    List<User> getNewUsers(Status status);

    void approveUser(Long id);

    void deleteUser(Long id);

    boolean isWorker(User user);

    boolean isStudent(User user);

    void changeState(Long aLong);

    List<User> filterUserListByCriteria(Boolean isActive, List<StatusName> statusIdList, String[] args);

    User setStatus(User user);

    List<User> getUsers(String[] args);

    User getUserByToken(String token);

    List<User> searchNewUserByNameAndStatus(String[] args, Status status);

    List<UserPOJO> convertToPOJO(List<User> userList);

    UserPOJO convertToPOJO(User user);

    List<User> getLecturersByDirection(String direction);

    List<SearchingResponse> getSearchingResponses(List<User> users, SelectType type);
}
