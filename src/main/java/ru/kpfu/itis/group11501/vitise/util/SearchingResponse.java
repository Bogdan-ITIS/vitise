package ru.kpfu.itis.group11501.vitise.util;

import ru.kpfu.itis.group11501.vitise.model.enums.SelectType;

/**
 * Created by Bogdan Popov on 12.05.2017.
 */
public abstract class SearchingResponse {
    protected SelectType type;

    public abstract Long getId();

    public abstract void setId(Long id);

    public abstract String getName();

    public abstract void setName(String name);

    public SelectType getType() {
        return type;
    }

    public void setType(SelectType type) {
        this.type = type;
    }
}
