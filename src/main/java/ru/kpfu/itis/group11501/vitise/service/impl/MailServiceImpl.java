package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.service.MailService;
import ru.kpfu.itis.group11501.vitise.util.EmailType;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * Author: Svintenok Kate
 * Date: 21.04.2017
 * Group: 11-501
 * Project: vITISe
 */

@Service("mailService")
public class MailServiceImpl implements MailService {

    @Autowired
    private JavaMailSender mailSender;

    @Async
    @Override
    public void sendEmail(User user, String host, EmailType type) {
        MimeMessagePreparator preparator = getMessagePreparator(user, host, type);
        try {
            mailSender.send(preparator);
        } catch (MailException ex) {
            System.err.println(ex.getMessage());
        }
    }


    private MimeMessagePreparator getMessagePreparator(User user, String host, EmailType type) {

        MimeMessagePreparator preparator = new MimeMessagePreparator() {

            public void prepare(MimeMessage mimeMessage) throws Exception {
                mimeMessage.setFrom("vItise");


                if (type == EmailType.MAIl_CONFIRMATION) {
                    mimeMessage.setSubject("Пожалуйста, подтвердите email");

                    if (!user.isConfirmed()) {
                        mimeMessage.setRecipient(Message.RecipientType.TO,
//                         new InternetAddress("your-address@gmail.com"));
                                new InternetAddress(user.getEmail()));

                        String message = "<p>Для подтверждения адреса электронной почты перейдите по "
                                + "<a href=\"http://" + host + "/" + user.getToken() + "/confirm\">ссылке</a>.</p><br>"
                                + "<i>vItise</i>";

                        mimeMessage.setText(message, "UTF-8", "html");
                    } else {
                        mimeMessage.setRecipient(Message.RecipientType.TO,
//                         new InternetAddress("your-address@gmail.com"));
                                new InternetAddress(user.getNewEmail()));

                        String message = "<p>Для изменения адреса электронной почты перейдите по "
                                + "<a href=\"http://" + host + "/" + user.getToken() + "/confirm\">ссылке</a>.</p><br>"
                                + "<i>vItise</i>";

                        mimeMessage.setText(message, "UTF-8", "html");
                    }
                } else if (type == EmailType.DEANERY_REGISTRATION) {
                    mimeMessage.setSubject("Уведомление о регистрации");

                    mimeMessage.setRecipient(Message.RecipientType.TO,
//                         new InternetAddress("your-address@gmail.com"));
                            new InternetAddress(user.getEmail()));

                    String message = "<p>Здравствуйте, " + user.getFullName() +
                            "! Вы были зарегестрированы на сайте vItise.<br>Ваш логин: " + user.getEmail() +
                            "<br>Ваш пароль: " + user.getGeneratingPassword() +
                            "<br>Вы можете войти в свой кабинет по <a href=\"http://" + host + "/sign_in\">ссылке</a>.</p><br>"
                            + "<i>vItise</i>";

                    mimeMessage.setText(message, "UTF-8", "html");

                    user.setGeneratingPassword(null);
                } else if (type == EmailType.PASSWORD_RECOVERY) {
                    mimeMessage.setSubject("Восстановление пароля");

                    mimeMessage.setRecipient(Message.RecipientType.TO,
//                         new InternetAddress("your-address@gmail.com"));
                            new InternetAddress(user.getEmail()));

                    String message = "<p>Здравствуйте, " + user.getFullName() +
                            "! на логин " + user.getEmail() + " был направлен запрос о восстановлении пароля.<br>" +
                            "Если вы не посылали запрос, просто проигнорируйте это письмо.<br>" +
                            "<br>Ваш новый пароль: " + user.getGeneratingPassword() +
                            "<br>Чтобы пароль вступил в силу, перейдите по <a href=\"http://" + host +
                            "/password_recovery/" + user.getToken() + "/confirm\">ссылке</a>.</p><br>"
                            + "<i>vItise</i>";

                    mimeMessage.setText(message, "UTF-8", "html");

                    user.setGeneratingPassword(null);
                }

            }
        };
        return preparator;
    }

}
