package ru.kpfu.itis.group11501.vitise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.enums.StatusName;
import ru.kpfu.itis.group11501.vitise.service.MailService;
import ru.kpfu.itis.group11501.vitise.service.StatusService;
import ru.kpfu.itis.group11501.vitise.service.UserService;
import ru.kpfu.itis.group11501.vitise.util.DeaneryForm;
import ru.kpfu.itis.group11501.vitise.util.DeaneryFormToUserTransformer;
import ru.kpfu.itis.group11501.vitise.util.EmailType;
import ru.kpfu.itis.group11501.vitise.util.SendEmailTask;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.concurrent.Executor;
import java.util.function.Function;

/**
 * Created by Наталья on 08.04.2017.
 */
@Controller
@RequestMapping("/admin")
public class SignUpDeaneryController {

    private final UserService userService;
    private final StatusService statusService;
    private final Function<DeaneryForm, User> deaneryFormToUserTransformer;
    private final MailService mailService;
    private final Executor executor;

    @Autowired
    public SignUpDeaneryController(UserService userService, StatusService statusService,
                                   MailService mailService, @Qualifier("getAsyncExecutor") Executor executor) {
        this.userService = userService;
        this.statusService = statusService;
        this.mailService = mailService;
        this.executor = executor;
        this.deaneryFormToUserTransformer = new DeaneryFormToUserTransformer();
    }

    @RequestMapping(value = "/add_deanery", method = RequestMethod.GET)
    public String signUpPage(Model model) {
        model.addAttribute("userForm", new DeaneryForm());
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "add_deanery";
    }

    @RequestMapping(value = "/add_deanery", method = RequestMethod.POST)
    public String signUp(@ModelAttribute("userForm") @Valid DeaneryForm deaneryForm,
                         BindingResult bindingResult, Model model, HttpServletRequest request) {
        if (!bindingResult.hasErrors()) {
            User user = deaneryFormToUserTransformer.apply(deaneryForm);
            userService.save(user);
            executor.execute(
                    new SendEmailTask(user, request.getHeader("Host"), EmailType.DEANERY_REGISTRATION, mailService));

            user = userService.getUser(deaneryForm.getEmail());
            statusService.addUsersStatus(user, StatusName.DEANERY);
            return "redirect:/profile";
        }
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "add_deanery";
    }
}
