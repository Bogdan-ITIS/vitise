package ru.kpfu.itis.group11501.vitise.pojo;

/**
 * Created by Bogdan Popov on 26.04.2017.
 */
public class Message {
    private String content;

    public Message() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
