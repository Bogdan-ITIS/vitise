package ru.kpfu.itis.group11501.vitise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.service.ColleaguesService;
import ru.kpfu.itis.group11501.vitise.service.CommonDialogService;
import ru.kpfu.itis.group11501.vitise.util.messages.CommonDialog;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 06.04.2017
 * Group: 11-501
 * Project: vITISe
 */
@Controller
@RequestMapping("/messages")
public class CommonDialogController {

    private final ColleaguesService colleaguesService;
    private final CommonDialogService commonDialogService;

    @Autowired
    public CommonDialogController(ColleaguesService colleaguesService,
                                  CommonDialogService commonDialogService) {
        this.colleaguesService = colleaguesService;
        this.commonDialogService = commonDialogService;
    }

    @RequestMapping(value = "/{filter}", method = RequestMethod.GET)
    public String getAllDialogs(@PathVariable(name = "filter", required = false) String filter, Model model,
                                HttpServletRequest request) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("admin", false);
        if (request.isUserInRole("ROLE_ADMIN")) {
            model.addAttribute("admin", true);
        }
        model.addAttribute("colleagues", colleaguesService.getColleagueUsers(currentUser));
        model.addAttribute("currentUser", currentUser);
        List<CommonDialog> commonDialogs = commonDialogService.getDialogs(currentUser, filter);
        model.addAttribute("dialogs", commonDialogs);
        return "messages";
    }

    @RequestMapping(value = "/get_number")
    @ResponseBody
    public Integer getMessagesNumber(@RequestParam(name = "messages", required = false) int messages) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new Integer(commonDialogService.newMessagesCount(currentUser));
    }
}