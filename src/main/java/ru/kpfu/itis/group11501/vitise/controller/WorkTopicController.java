package ru.kpfu.itis.group11501.vitise.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.kpfu.itis.group11501.vitise.model.DirectionOfScientificActivity;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.WorkTopic;
import ru.kpfu.itis.group11501.vitise.model.WorkTopicRequest;
import ru.kpfu.itis.group11501.vitise.service.DirectionOfScientificActivityService;
import ru.kpfu.itis.group11501.vitise.service.UserService;
import ru.kpfu.itis.group11501.vitise.service.WorkTopicService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * Author: Svintenok Kate
 * Date: 05.05.2017
 * Group: 11-501
 * Project: vITISe
 */
@Controller
@RequestMapping("/work_topics")
public class WorkTopicController {
    private final UserService userService;
    private final WorkTopicService topicService;
    private final DirectionOfScientificActivityService directionService;

    public WorkTopicController(UserService userService, WorkTopicService topicService,
                               DirectionOfScientificActivityService directionService) {
        this.userService = userService;
        this.topicService = topicService;
        this.directionService = directionService;
    }


    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String topicsPage(Model model, HttpServletRequest request,
                             @RequestParam(name = "name", required = false) String name,
                             @RequestParam(name = "professor", required = false) Long creatorId,
                             @RequestParam(name = "direction", required = false) Long directionId) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userService.setStatus(currentUser);
        model.addAttribute("currentUser", currentUser);

        if (name != null) {
            User creator = null;
            DirectionOfScientificActivity direction = null;
            if (creatorId != 0)
                creator = userService.findOne(creatorId);
            if (directionId != 0)
                direction = directionService.findOne(directionId);
            model.addAttribute("workTopics", topicService.getAllFreeTopics(currentUser, name, creator, direction));

            model.addAttribute("topicName", name);
            model.addAttribute("topicProfessor", creatorId);
            model.addAttribute("topicDirection", directionId);
        } else {
            model.addAttribute("workTopics", topicService.getAllFreeTopics(currentUser));
        }

        model.addAttribute("directions", directionService.findAll());
        model.addAttribute("professors", topicService.getAllProfessors());
        if (request.isUserInRole("ROLE_STUDENT")) {
            model.addAttribute("isStudentHaveTopic", topicService.isStudentHaveTopic(currentUser));
            model.addAttribute("userRequestsCount", topicService.getActiveStudentRequests(currentUser).size());
        }
        return "work_topics";
    }

    @RequestMapping(value = "/choose", method = RequestMethod.POST)
    public String chooseTopic(@RequestParam(value = "topic_id") Long topicId, HttpServletRequest request) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        WorkTopicRequest topicRequest = new WorkTopicRequest();
        topicRequest.setTopic(topicService.getTopic(topicId));
        topicRequest.setStudent(currentUser);
        topicService.addTopicRequest(topicRequest);
        return "redirect:" + request.getHeader("Referer");
    }

    @RequestMapping(value = "/delete_choose", method = RequestMethod.POST)
    public String deleteRequest(@RequestParam(value = "request_id") Long requestId, HttpServletRequest request) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        WorkTopicRequest topicRequest = topicService.getTopicRequest(requestId);
        topicService.deleteTopicRequest(topicRequest);

        return "redirect:" + request.getHeader("Referer");
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String professorTopicsPage(Model model, HttpServletRequest request) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("currentUser", currentUser);
        if (request.isUserInRole("ROLE_WORKER")) {
            model.addAttribute("work_topics", topicService.getAllProfessorTopics(currentUser));
            model.addAttribute("directions", directionService.findAll());
            model.addAttribute("activeRequests", topicService.getActiveProfessorRequests(currentUser));
            return "professor_work_topics";
        }
        if (topicService.getConfirmedStudentRequest(currentUser) != null)
            model.addAttribute("studentTopic", topicService.getConfirmedStudentRequest(currentUser).getTopic());
        model.addAttribute("userRequests", topicService.getStudentRequests(currentUser));
        return "student_work_topics";
    }

    @RequestMapping(value = "/request/{id}/approve", method = RequestMethod.POST)
    public String approveRequest(@PathVariable(name = "id") Long id, HttpServletRequest request) {
        topicService.approveRequest(topicService.getTopicRequest(id));
        return "redirect:" + request.getHeader("Referer");
    }

    @RequestMapping(value = "/request/{id}/reject", method = RequestMethod.POST)
    public String rejectRequest(@PathVariable(name = "id") Long id, HttpServletRequest request) {
        topicService.rejectRequest(topicService.getTopicRequest(id));
        return "redirect:" + request.getHeader("Referer");
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createTopic(Model model, HttpServletRequest request, @RequestParam Map<String, String> allRequestParams) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        WorkTopic topic = new WorkTopic();
        topic.setCreator(currentUser);
        topic.setName(allRequestParams.get("name"));
        topic.setDirection(directionService.findOne(Long.valueOf(allRequestParams.get("direction"))));
        topicService.addTopic(topic);
        return "redirect:" + request.getHeader("Referer");
    }

    @RequestMapping(value = "/get_requests_number")
    @ResponseBody
    public Integer getRequestsNumber(@RequestParam(name = "topic_requests", required = false) int topic_requests) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new Integer(topicService.getActiveProfessorRequests(currentUser).size());
    }
}