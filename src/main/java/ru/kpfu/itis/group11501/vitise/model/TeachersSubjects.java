package ru.kpfu.itis.group11501.vitise.model;

import javax.persistence.*;

/**
 * Created by Наталья on 04.05.2017.
 */
@Entity
@Table(name = "teachers_subjects")
public class TeachersSubjects {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "subjectsid")
    private Subject subject;

    @ManyToOne
    @JoinColumn(name = "teacherid")
    private User teacher;

    public TeachersSubjects() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public User getTeacher() {
        return teacher;
    }

    public void setTeacher(User teacher) {
        this.teacher = teacher;
    }
}
