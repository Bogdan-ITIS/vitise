package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.Event;
import ru.kpfu.itis.group11501.vitise.model.EventsUsers;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.repository.EventRepository;
import ru.kpfu.itis.group11501.vitise.repository.EventsUsersRepository;
import ru.kpfu.itis.group11501.vitise.service.EventService;
import ru.kpfu.itis.group11501.vitise.service.UserService;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 22.03.2017
 * Group: 11-501
 * Project: vITISe
 */
@Service
public class EventServiceImpl implements EventService {
    private static final int PAGE_SIZE = 10;
    private final EventRepository eventRepository;
    private final EventsUsersRepository eventsUsersRepository;
    private final UserService userService;


    @Autowired
    public EventServiceImpl(EventRepository eventRepository,
                            EventsUsersRepository eventsUsersRepository,
                            UserService userService) {
        this.eventRepository = eventRepository;
        this.eventsUsersRepository = eventsUsersRepository;
        this.userService = userService;
    }

    @Override
    public void add(Event event) {
        eventRepository.save(event);
    }

    @Override
    public Event getEvent(Long id) {
        return eventRepository.findOne(id);
    }

    @Override
    public void removeEvent(Event event) {
        eventRepository.delete(event);
    }

    @Override
    public List<Event> getAllPublicEvents(User currentUser) {
        List<Event> events = eventRepository.findByIsPublic(true, currentUser);
        this.addSubscriptionInfo(events, currentUser);
        return events;
    }

    @Override
    public List<Event> getAllPublicEvents(User currentUser, int page) {
        PageRequest pageRequest = new PageRequest(page - 1, PAGE_SIZE, Sort.Direction.DESC, "pubDate");
        List<Event> events = eventRepository.findByIsPublic(true, currentUser, pageRequest);
        this.addSubscriptionInfo(events, currentUser);
        return events;
    }

    @Override
    public List<Event> getUserEvents(User user, User currentUser) {
        List<Event> events = eventRepository.findByAuthorId(user.getId());
        this.addSubscriptionInfo(events, currentUser);
        return events;
    }

    @Override
    public List<Event> getMyEvents(User currentUser) {
        List<Event> eventList = new ArrayList<>();
        List<EventsUsers> eventsUsersList = eventsUsersRepository.findAllByUserId(currentUser.getId());
        for (EventsUsers e : eventsUsersList) {
            eventList.add(e.getEvent());
        }
        this.addSubscriptionInfo(eventList, currentUser);
        return eventList;
    }


    @Override
    public void changeSubscribeStatus(Event event, User user) {
        EventsUsers eventsUsers = eventsUsersRepository.findOneByEventIdAndUserId(event.getId(), user.getId());
        if (eventsUsers == null) {
            eventsUsers = new EventsUsers();
            eventsUsers.setEvent(event);
            eventsUsers.setUser(user);
            eventsUsersRepository.save(eventsUsers);
        } else {
            eventsUsersRepository.delete(eventsUsers);
        }
    }

    @Override
    public boolean subscribeStatus(Event event, User user) {
        if (eventsUsersRepository.findOneByEventIdAndUserId(event.getId(), user.getId()) == null)
            return false;
        return true;
    }

    private List<Event> addSubscriptionInfo(List<Event> events, User currentUser) {
        for (Event event : events) {
            userService.setStatus(event.getAuthor());
            event.setSubscriptionsCount(eventsUsersRepository.countByEventId(event.getId()));
            if (subscribeStatus(event, currentUser))
                event.setSubscribeStatus(true);
        }
        return events;
    }

    @Override
    public int getLastPagePublicEventsNumber(User currentUser) {
        return (int) Math.ceil((double) eventRepository.findByIsPublic(false, currentUser).size() / PAGE_SIZE);
    }
}