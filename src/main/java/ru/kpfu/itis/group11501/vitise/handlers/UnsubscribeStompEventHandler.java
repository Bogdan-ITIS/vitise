package ru.kpfu.itis.group11501.vitise.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;
import ru.kpfu.itis.group11501.vitise.service.StompSessionService;

/**
 * Created by Bogdan Popov on 05.05.2017.
 */
public class UnsubscribeStompEventHandler implements ApplicationListener<SessionUnsubscribeEvent> {
    private StompSessionService stompSessionService;

    @Autowired
    public UnsubscribeStompEventHandler(StompSessionService stompSessionService) {
        this.stompSessionService = stompSessionService;
    }

    @Override
    public void onApplicationEvent(SessionUnsubscribeEvent event) {
        StompHeaderAccessor headers = StompHeaderAccessor.wrap(event.getMessage());
        String sessionId = headers.getSessionId();
        String destination = headers.getDestination();
        stompSessionService.updateMessagingForSession(sessionId, destination);
        stompSessionService.deleteDestination(sessionId, destination);
    }
}
