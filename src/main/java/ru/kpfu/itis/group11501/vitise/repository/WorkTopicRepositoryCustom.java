package ru.kpfu.itis.group11501.vitise.repository;

import ru.kpfu.itis.group11501.vitise.model.DirectionOfScientificActivity;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.WorkTopic;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 10.05.2017
 * Group: 11-501
 * Project: vITISe
 */
public interface WorkTopicRepositoryCustom {
    List<WorkTopic> findAllByNameAndCreatorAndDirection(String name, User creator, DirectionOfScientificActivity direction);
}
