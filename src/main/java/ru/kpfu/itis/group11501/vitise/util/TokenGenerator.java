package ru.kpfu.itis.group11501.vitise.util;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Author: Svintenok Kate
 * Date: 21.04.2017
 * Group: 11-501
 * Project: vITISe
 */
public class TokenGenerator {

    private static final SecureRandom random = new SecureRandom();

    public static String generate() {
        return new BigInteger(130, random).toString(32);
    }
}