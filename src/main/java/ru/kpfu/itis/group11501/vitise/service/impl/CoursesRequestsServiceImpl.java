package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.Course;
import ru.kpfu.itis.group11501.vitise.model.CoursesRequests;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.repository.CoursesRequestsRepository;
import ru.kpfu.itis.group11501.vitise.service.CoursesRequestsService;
import ru.kpfu.itis.group11501.vitise.service.UserService;

import java.util.List;

/**
 * Created by Наталья on 05.05.2017.
 */
@Service
public class CoursesRequestsServiceImpl implements CoursesRequestsService {
    private final CoursesRequestsRepository coursesRequestsRepository;
    private final UserService userService;

    @Autowired
    public CoursesRequestsServiceImpl(CoursesRequestsRepository coursesRequestsRepository, UserService userService) {
        this.coursesRequestsRepository = coursesRequestsRepository;
        this.userService = userService;
    }

    @Override
    public CoursesRequests getRequest(Course course, User user) {
        try {
            return coursesRequestsRepository.findOneByCourseAndUser(course, user);
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public int getState(CoursesRequests coursesRequests) {
        if (coursesRequests == null) {
            return 0;
        } else if (coursesRequests.getActive() == null) {
            return 1;
        } else if (coursesRequests.getActive()) {
            return 2;
        } else {
            return -1;
        }
    }

    @Override
    public void delete(Long id) {
        coursesRequestsRepository.delete(id);
    }

    @Override
    public void add(User currentUser, Course course) {
        CoursesRequests request = new CoursesRequests();
        request.setActive(null);
        request.setCourse(course);
        request.setUser(currentUser);
        coursesRequestsRepository.save(request);
    }

    @Override
    public List<CoursesRequests> getRequests() {
        List<CoursesRequests> list = coursesRequestsRepository.findAllByIsActive(null);
        for (CoursesRequests request : list) {
            userService.setStatus(request.getUser());
        }
        return list;
    }

    @Override
    public CoursesRequests getRequest(Long id) {
        return coursesRequestsRepository.findOne(id);
    }

    @Override
    public void update(CoursesRequests courseRequest) {
        coursesRequestsRepository.save(courseRequest);
    }
}
