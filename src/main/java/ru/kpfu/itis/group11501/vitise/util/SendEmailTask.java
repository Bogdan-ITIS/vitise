package ru.kpfu.itis.group11501.vitise.util;

import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.service.MailService;

/**
 * Author: Svintenok Kate
 * Date: 22.04.2017
 * Group: 11-501
 * Project: vITISe
 */
public class SendEmailTask implements Runnable {

    private User user;
    private String host;
    private EmailType type;
    private MailService mailService;

    public SendEmailTask(User user, String host, EmailType type, MailService mailService) {
        this.user = user;
        this.host = host;
        this.type = type;
        this.mailService = mailService;
    }

    @Override
    public void run() {
        mailService.sendEmail(user, host, type);
    }
}