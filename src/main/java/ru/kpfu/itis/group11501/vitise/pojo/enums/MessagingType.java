package ru.kpfu.itis.group11501.vitise.pojo.enums;

/**
 * Created by Bogdan Popov on 05.05.2017.
 */
public enum MessagingType {
    COMMUNICATIONRELATIONS("dialog"),
    CONVERSATION("conversation");

    private String simpleName;

    MessagingType(String simpleName) {
        this.simpleName = simpleName;
    }

    public String getSimpleName() {
        return simpleName;
    }


    @Override
    public String toString() {
        return getSimpleName();
    }
}
