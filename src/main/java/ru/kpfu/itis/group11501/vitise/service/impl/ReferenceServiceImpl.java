package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.Reference;
import ru.kpfu.itis.group11501.vitise.repository.ReferenceRepository;
import ru.kpfu.itis.group11501.vitise.service.ReferenceService;

/**
 * Created by Марат on 06.05.2017.
 */
@Service
public class ReferenceServiceImpl implements ReferenceService {
    private final ReferenceRepository referenceRepository;

    @Autowired
    public ReferenceServiceImpl(ReferenceRepository referenceRepository) {
        this.referenceRepository = referenceRepository;
    }

    @Override
    public void add(Reference reference) {
        referenceRepository.save(reference);
    }

    @Override
    public Reference getOne(Long id) {
        return referenceRepository.findOne(id);
    }

    @Override
    public void remove(Reference reference) {
        referenceRepository.delete(reference);
    }
}
