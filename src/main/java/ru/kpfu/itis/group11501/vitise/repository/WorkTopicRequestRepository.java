package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.WorkTopic;
import ru.kpfu.itis.group11501.vitise.model.WorkTopicRequest;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 04.05.2017
 * Group: 11-501
 * Project: vITISe
 */
public interface WorkTopicRequestRepository extends JpaRepository<WorkTopicRequest, Long> {
    WorkTopicRequest findOneByTopicIdAndStudentId(Long topicId, Long studentId);

    List<WorkTopicRequest> findAllByStudentIdOrderByDateAsc(Long studentId);

    List<WorkTopicRequest> findAllByStudentIdAndConfirmed(Long studentId, Boolean confirmed);

    List<WorkTopicRequest> findAllByTopicIdAndConfirmedOrderByDateAsc(Long topicId, Boolean confirmed);

    @Query("select request from WorkTopicRequest request " +
            "WHERE request.confirmed is null and request.topic.creator = :creator ORDER BY request.date ASC")
    List<WorkTopicRequest> findAllActiveByTopicCreatorId(@Param("creator") User creator);


    @Query("select request from WorkTopicRequest request " +
            "WHERE request.confirmed = true and request.topic = :topic")
    WorkTopicRequest findOneConfirmedByTopic(@Param("topic") WorkTopic topic);

    @Query("select request from WorkTopicRequest request " +
            "WHERE request.confirmed = true and request.student = :student")
    WorkTopicRequest findOneConfirmedByStudent(@Param("student") User student);
}
