package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.util.messages.CommonDialog;

import java.util.List;
import java.util.regex.Pattern;

/**
 * Author: Svintenok Kate
 * Date: 06.04.2017
 * Group: 11-501
 * Project: vITISe
 */
public interface CommonDialogService {
    List<CommonDialog> getDialogs(User user, String filter);

    int newMessagesCount(User user);

    Pattern getCommonDialogPattern();
}
