package ru.kpfu.itis.group11501.vitise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.service.MailService;
import ru.kpfu.itis.group11501.vitise.service.UserService;
import ru.kpfu.itis.group11501.vitise.util.*;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.Executor;

@Controller
public class SignInController {

    private static final PasswordEncoder encoder = new BCryptPasswordEncoder();
    private final MailService mailService;
    private final UserService userService;
    private final Executor executor;

    @Autowired
    public SignInController(MailService mailService, UserService userService, @Qualifier("getAsyncExecutor") Executor executor) {
        this.mailService = mailService;
        this.userService = userService;
        this.executor = executor;
    }

    @RequestMapping("/sign_in")
    public String getSignIn(@RequestParam(value = "error", required = false) Boolean error,
                            Model model) {
        if (Boolean.TRUE.equals(error)) {
            model.addAttribute("error", error);
        }
        model.addAttribute("authForm", new AuthForm());
        return "sign_in";
    }

    @RequestMapping(value = "/password_recovery", method = RequestMethod.POST)
    public String recoverPassword(@RequestParam(name = "email") String email, HttpServletRequest request) {

        User user = userService.getUser(email);
        if (user != null) {
            String password = PasswordGenerator.generate();
            user.setGeneratingPassword(password);
            user.setToken(TokenGenerator.generate());
            user.setNewPassword(encoder.encode(password));
            userService.save(user);
            executor.execute(
                    new SendEmailTask(user, request.getHeader("Host"), EmailType.PASSWORD_RECOVERY, mailService));
        }

        return "redirect:/sign_in";
    }

    @RequestMapping(value = "/password_recovery/{token}/confirm")
    public String confirmPasswordRecovery(@PathVariable("token") String token) {

        User user = userService.getUserByToken(token);
        if (user == null)
            return "handle404";
        user.setPassword(user.getNewPassword());
        user.setNewPassword(null);
        user.setToken(null);
        userService.save(user);
        return "redirect:/sign_in";
    }

}
