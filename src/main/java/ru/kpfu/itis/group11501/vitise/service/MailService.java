package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.util.EmailType;

/**
 * Author: Svintenok Kate
 * Date: 21.04.2017
 * Group: 11-501
 * Project: vITISe
 */
public interface MailService {
    void sendEmail(User user, String host, EmailType type);
}
