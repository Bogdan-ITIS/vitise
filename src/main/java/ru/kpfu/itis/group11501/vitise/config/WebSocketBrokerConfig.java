package ru.kpfu.itis.group11501.vitise.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;
import ru.kpfu.itis.group11501.vitise.repository.StompSessionRepository;
import ru.kpfu.itis.group11501.vitise.repository.impl.StompSessionRepositoryImpl;

import java.security.Principal;
import java.util.Map;


/**
 * Created by Bogdan Popov on 23.04.2017.
 */

@Configuration
@EnableWebSocketMessageBroker
@ComponentScan("ru.kpfu.itis.group11501.vitise")
public class WebSocketBrokerConfig extends AbstractWebSocketMessageBrokerConfigurer {

    @Override
    public void registerStompEndpoints(StompEndpointRegistry stompEndpointRegistry) {
        stompEndpointRegistry.addEndpoint("/messaging", "/online").setHandshakeHandler(new UsernameHandshakeHandler()).withSockJS();
    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        registry.setApplicationDestinationPrefixes("/app")
                .enableSimpleBroker("/topic", "/queue");
    }

    @Bean(name = "stompSessionRepository")
    public StompSessionRepository createStompSessionRepository() {
        return new StompSessionRepositoryImpl();
    }

    private class UsernameHandshakeHandler extends DefaultHandshakeHandler {

        @Override
        protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
            return request.getPrincipal();
        }

    }
}
