package ru.kpfu.itis.group11501.vitise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.group11501.vitise.model.CommunicationRelation;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.UserMessage;
import ru.kpfu.itis.group11501.vitise.model.UserMessageFile;
import ru.kpfu.itis.group11501.vitise.pojo.Message;
import ru.kpfu.itis.group11501.vitise.pojo.ResponseMessage;
import ru.kpfu.itis.group11501.vitise.pojo.enums.MessagingType;
import ru.kpfu.itis.group11501.vitise.service.*;
import ru.kpfu.itis.group11501.vitise.util.FileUploader;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Наталья on 06.04.2017.
 */
@Controller
@RequestMapping("/messages")
public class CommunicationRelationsController {
    private final Pattern pattern = Pattern.compile("^\\S+$");
    private final CommunicationRelationService communicationRelationsService;
    private final UserMessageService userMessageService;
    private final ConversationService conversationService;
    private final ColleaguesService colleaguesService;
    private final UserService userService;
    private final UserMessageFileService userMessageFileService;
    private final SimpMessagingTemplate messagingTemplate;
    private final StompSessionService stompSessionService;

    @Autowired
    public CommunicationRelationsController(CommunicationRelationService communicationRelationsService,
                                            UserMessageService userMessageService,
                                            ConversationService conversationService,
                                            ColleaguesService colleaguesService,
                                            UserService userService, UserMessageFileService userMessageFileService,
                                            SimpMessagingTemplate messagingTemplate, StompSessionService stompSessionService) {
        this.communicationRelationsService = communicationRelationsService;
        this.userMessageService = userMessageService;
        this.conversationService = conversationService;
        this.colleaguesService = colleaguesService;
        this.userService = userService;
        this.userMessageFileService = userMessageFileService;
        this.messagingTemplate = messagingTemplate;
        this.stompSessionService = stompSessionService;
    }

    @RequestMapping(value = "/dialog/{id}", method = RequestMethod.GET)
    public String getDialogPage(@PathVariable(name = "id") Long id, Model model) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        CommunicationRelation communicationRelation = communicationRelationsService.getOne(id);
        if (communicationRelation == null || !communicationRelationsService.hasAccessToChat(currentUser, communicationRelation))
            return "handle404";
        communicationRelation = communicationRelationsService.getOneWithInfo(id, currentUser);
        model.addAttribute("relation", communicationRelation);
        model.addAttribute("currentUser", currentUser);
        communicationRelationsService.updateReadingLog(communicationRelation, currentUser);
        return "chat_page";
    }

    @MessageMapping("/dialog/send/{id}")
    public void sendMessage(Message message, Principal principal, @DestinationVariable Long id) {

        User user = (User) ((UsernamePasswordAuthenticationToken) principal).getPrincipal();
        Date date = new Date();
        UserMessage usersMessages = new UserMessage();
        usersMessages.setMessage(message.getContent());
        CommunicationRelation communicationRelation = communicationRelationsService.getOne(id);
        usersMessages.setCommunicationRelation(communicationRelation);
        usersMessages.setUser(user);
        userMessageService.addUsersMessages(usersMessages);
        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setContent(message.getContent());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.getDefault());
        responseMessage.setDate(dateFormat.format(date));
        responseMessage.setSender(userService.convertToPOJO(user));
        responseMessage.setIdMessaging(String.valueOf(id));
        responseMessage.setType(MessagingType.COMMUNICATIONRELATIONS);
        responseMessage.setNameMessaging(user.getFullName());
        String destination = "/topic/dialog/" + id;
        String allMessagesDestination = "/topic/messages/";
        messagingTemplate.convertAndSend(destination, responseMessage);
        List<User> users = stompSessionService.getAllUser(destination);
        for (User destinationUser : users) {
            communicationRelationsService.updateReadingLog(communicationRelation, destinationUser);
        }
        if (!users.contains(communicationRelation.getFirst())) {
            messagingTemplate.convertAndSend(allMessagesDestination + communicationRelation.getFirst().getId(), responseMessage);
        }
        if (!users.contains(communicationRelation.getSecond())) {
            messagingTemplate.convertAndSend(allMessagesDestination + communicationRelation.getSecond().getId(), responseMessage);
        }
    }

    @RequestMapping(value = "/dialog/{id}/upload")
    public String uploadFile(@PathVariable(name = "id") Long id,
                             @RequestParam(value = "file", required = false) MultipartFile file,
                             HttpServletRequest request) {
        User user = (User) ((UsernamePasswordAuthenticationToken) request.getUserPrincipal()).getPrincipal();
        CommunicationRelation communicationRelation = communicationRelationsService.getOne(id);
        UserMessage usersMessages = userMessageService.getLastFromUserInDialog(user, communicationRelation);
        UserMessageFile userMessageFile = new UserMessageFile();
        userMessageFile.setUserMessage(usersMessages);
        String name = FileUploader.uploadFile(file);
        userMessageFile.setFilename(name);
        userMessageFileService.add(userMessageFile);
        return "redirect:" + request.getHeader("Referer");
    }

    @RequestMapping(value = "/dialog/create/{id}", method = RequestMethod.POST)
    public String createDialog(@PathVariable(name = "id") Long userId) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.getUser(userId);
        CommunicationRelation communicationRelation = communicationRelationsService.getOne(currentUser, user);
        if (communicationRelation != null) {
            return "redirect:/messages/dialog/" + communicationRelation.getId();
        }
        communicationRelationsService.createCommunicationRelations(user, currentUser);
        communicationRelation = communicationRelationsService.getOne(user, currentUser);
        return "redirect:/messages/dialog/" + communicationRelation.getId();
    }
}
