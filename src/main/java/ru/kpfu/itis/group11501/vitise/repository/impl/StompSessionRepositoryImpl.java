package ru.kpfu.itis.group11501.vitise.repository.impl;

import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.pojo.StompSession;
import ru.kpfu.itis.group11501.vitise.repository.StompSessionRepository;

import java.util.*;

/**
 * Created by Bogdan Popov on 05.05.2017.
 */
public class StompSessionRepositoryImpl implements StompSessionRepository {
    private Map<String, StompSession> storage = new Hashtable<>();
    private Map<String, Vector<User>> destinationUsers = new Hashtable<>();
    private Map<Long, Vector<StompSession>> usersSessions = new Hashtable<>();

    @Override
    public void addSession(StompSession stompSession) {
        String sessionId = stompSession.getSessionId();
        StompSession alreadyCreatedSession = storage.get(sessionId);
        User user = stompSession.getUser();
        Long id = user.getId();
        if (!usersSessions.containsKey(id)) {
            usersSessions.put(id, new Vector<>());
        }
        usersSessions.get(id).add(stompSession);
        for (String destination : stompSession.getDestinations()) {
            if (!destinationUsers.containsKey(destination)) {
                destinationUsers.put(destination, new Vector<>());
            }
            Vector<User> users = destinationUsers.get(destination);
            if (!users.contains(user)) {
                users.add(user);
            }
        }
        if (alreadyCreatedSession == null) {
            storage.put(sessionId, stompSession);
        } else {
            Set<String> destinations = alreadyCreatedSession.getDestinations();
            for (String destination : stompSession.getDestinations()) {
                destinations.add(destination);
            }
        }
        System.out.println(storage.toString());
        System.out.println(destinationUsers.toString());
        System.out.println(usersSessions.toString());
    }

    @Override
    public void deleteSession(String sessionId) {
        StompSession stompSession = storage.remove(sessionId);
        Long id = stompSession.getUser().getId();
        usersSessions.get(id).remove(stompSession);
        for (String destination : stompSession.getDestinations()) {
            if (!containsDestination(id, destination)) {
                destinationUsers.get(destination).remove(stompSession.getUser());
            }
        }
    }

    @Override
    public StompSession getStompSession(String sessionId) {
        return storage.get(sessionId);
    }

    @Override
    public List<User> getAllUser(String destination) {
        return destinationUsers.get(destination);
    }

    @Override
    public boolean containsDestination(Long id, String destination) {
        List<StompSession> sessionList = usersSessions.get(id);
        for (StompSession session : sessionList) {
            Set<String> destinations = session.getDestinations();
            if (destinations.contains(destination)) {
                return true;
            }
        }
        return false;
    }
}
