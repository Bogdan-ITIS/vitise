package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.Laboratory;
import ru.kpfu.itis.group11501.vitise.model.LaboratoryRequests;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.enums.SelectType;
import ru.kpfu.itis.group11501.vitise.util.LaboratoriesSearchingResponseAdapter;
import ru.kpfu.itis.group11501.vitise.util.SearchingResponse;
import ru.kpfu.itis.group11501.vitise.repository.LaboratoryRepository;
import ru.kpfu.itis.group11501.vitise.repository.LaboratoryRequestsRepository;
import ru.kpfu.itis.group11501.vitise.service.LaboratoryService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Наталья on 04.05.2017.
 */
@Service
public class LaboratoryServiceImpl implements LaboratoryService {
    private final LaboratoryRepository laboratoryRepository;
    private final LaboratoryRequestsRepository laboratoryRequestsRepository;

    @Autowired
    public LaboratoryServiceImpl(LaboratoryRepository laboratoryRepository, LaboratoryRequestsRepository laboratoryRequestsRepository) {
        this.laboratoryRepository = laboratoryRepository;
        this.laboratoryRequestsRepository = laboratoryRequestsRepository;
    }

    @Override
    public void add(Laboratory laboratory) {
        laboratoryRepository.save(laboratory);
    }

    @Override
    public List<Laboratory> getAll() {
        return laboratoryRepository.findAll();
    }

    @Override
    public Laboratory getOne(Long id) {
        return laboratoryRepository.findOne(id);
    }

    @Override
    public List<User> getStudentsFromLaboratory(Laboratory laboratory) {
        List<LaboratoryRequests> requestsList = laboratoryRequestsRepository.findAllByLaboratoryAndIsActive(laboratory, true);
        List<User> list = new ArrayList<>();
        for (LaboratoryRequests labReq : requestsList) {
            list.add(labReq.getUser());
        }
        return list;
    }

    @Override
    public Laboratory getLaboratory(User currentUser) {
        try {
            return laboratoryRequestsRepository.findOneByUserAndIsActive(currentUser, true).getLaboratory();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public List<SearchingResponse> getSearchingResponses(List<Laboratory> laboratories, SelectType type) {
        List<SearchingResponse> searchingResponses = new ArrayList<>();
        for (Laboratory laboratory : laboratories) {
            SearchingResponse searchingResponse = new LaboratoriesSearchingResponseAdapter(laboratory, type);
            searchingResponses.add(searchingResponse);
        }
        return searchingResponses;
    }

    @Override
    public List<Laboratory> getLaboratoriesByDirection(String direction) {
        try {
            Long idDirection = Long.valueOf(direction);
            return laboratoryRepository.findAllByDirection(idDirection);
        }
        catch (NumberFormatException e) {
            return laboratoryRepository.findAll();
        }
    }
}
