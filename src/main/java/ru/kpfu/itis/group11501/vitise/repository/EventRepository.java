package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.itis.group11501.vitise.model.Event;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 22.03.2017
 * Group: 11-501
 * Project: vITISe
 */
public interface EventRepository extends JpaRepository<Event, Long> {
    String query = "select DISTINCT e from Event e, Colleagues c " +
            "where e.isPublic = :is_public or (c.isActive = true and " +
            "(c.receiver = :current_user and e.author = c.sender) " +
            "or (c.sender = :current_user and e.author = c.receiver))";

    @Query(query)
    List<Event> findByIsPublic(@Param("is_public") boolean isPublic,
                               @Param("current_user") User currentUser);

    @Query(query)
    List<Event> findByIsPublic(@Param("is_public") boolean isPublic,
                               @Param("current_user") User currentUser, Pageable pageRequest);


    List<Event> findByAuthorId(Long id);
}
