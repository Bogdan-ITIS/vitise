package ru.kpfu.itis.group11501.vitise.pojo;

import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Bogdan Popov on 05.05.2017.
 */
public class StompSession {
    private Set<String> destinations = new HashSet<>();
    private User user;
    private String sessionId;

    public StompSession() {
    }

    public Set<String> getDestinations() {
        return destinations;
    }

    public void setDestinations(Set<String> destinations) {
        this.destinations = destinations;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
