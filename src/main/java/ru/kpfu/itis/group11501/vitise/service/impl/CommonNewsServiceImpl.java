package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.Event;
import ru.kpfu.itis.group11501.vitise.model.News;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.repository.NewsRepository;
import ru.kpfu.itis.group11501.vitise.service.CommonNewsService;
import ru.kpfu.itis.group11501.vitise.service.EventService;
import ru.kpfu.itis.group11501.vitise.service.NewsService;
import ru.kpfu.itis.group11501.vitise.service.UserService;
import ru.kpfu.itis.group11501.vitise.util.CommonNews;
import ru.kpfu.itis.group11501.vitise.util.EventsAdapter;
import ru.kpfu.itis.group11501.vitise.util.NewsAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Наталья on 04.04.2017.
 */
@Service
public class CommonNewsServiceImpl implements CommonNewsService {
    private static final int PAGE_SIZE = 10;
    private final EventService eventService;
    private final NewsService newsService;
    private final NewsRepository newsRepository;
    private final UserService userService;


    public CommonNewsServiceImpl(EventService eventService, NewsService newsService,
                                 NewsRepository newsRepository, UserService userService) {
        this.eventService = eventService;
        this.newsService = newsService;
        this.newsRepository = newsRepository;
        this.userService = userService;
    }

    @Override
    public List<CommonNews> getAllUsersNewsAndEvents(User user, User currentUser) {
        List<Event> events = eventService.getUserEvents(user, currentUser);
        List<News> newsList = newsService.getAllByAuthor(user);
        List<CommonNews> commonNewsList = joinNewsWithEvents(events, newsList);
        for (CommonNews commonNews : commonNewsList)
            userService.setStatus(commonNews.getAuthor());
        return commonNewsList;
    }


    @Override
    public List<CommonNews> getAllUsersNewsAndEvents(User user, User currentUser, int page) {
        return this.getPage(this.getAllUsersNewsAndEvents(user, currentUser), page);
    }

    @Override
    public int getLastPageCommonNewsesNumber(User currentUser) {
        return (int) Math.ceil((double) this.getAllCommonNews(currentUser).size() / PAGE_SIZE);
    }

    @Override
    public List<CommonNews> getAllCommonNews(User currentUser, int page) {
        return this.getPage(this.getAllCommonNews(currentUser), page);
    }

    private List<CommonNews> getAllCommonNews(User currentUser) {
        List<Event> events = eventService.getAllPublicEvents(currentUser);
        List<News> newsList = newsRepository.findByIsPersonal(false, currentUser);
        List<CommonNews> commonNewsList = joinNewsWithEvents(events, newsList);
        for (CommonNews commonNews : commonNewsList)
            userService.setStatus(commonNews.getAuthor());
        return commonNewsList;
    }

    private List<CommonNews> joinNewsWithEvents(List<Event> events, List<News> newsList) {
        List<CommonNews> commonNewsList = new ArrayList<>();
        for (Event e : events) {
            commonNewsList.add(new EventsAdapter(e));
        }
        for (News n : newsList) {
            commonNewsList.add(new NewsAdapter(n));
        }
        Collections.sort(commonNewsList);
        return commonNewsList;
    }

    private List<CommonNews> getPage(List<CommonNews> commonNewsList, int page) {
        int firstIndex = PAGE_SIZE * (page - 1);
        if (firstIndex >= commonNewsList.size())
            return null;
        int lastIndex = firstIndex + PAGE_SIZE > commonNewsList.size() ? commonNewsList.size() : firstIndex + PAGE_SIZE;
        return commonNewsList.subList(firstIndex, lastIndex);
    }
}
