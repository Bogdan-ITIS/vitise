package ru.kpfu.itis.group11501.vitise.repository.impl;

import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.group11501.vitise.model.DirectionOfScientificActivity;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.WorkTopic;
import ru.kpfu.itis.group11501.vitise.model.WorkTopic_;
import ru.kpfu.itis.group11501.vitise.repository.WorkTopicRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 10.05.2017
 * Group: 11-501
 * Project: vITISe
 */
@Transactional
public class WorkTopicRepositoryImpl implements WorkTopicRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<WorkTopic> findAllByNameAndCreatorAndDirection(String name, User creator, DirectionOfScientificActivity direction) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<WorkTopic> query = criteriaBuilder.createQuery(WorkTopic.class);
        Root<WorkTopic> root = query.from(WorkTopic.class);

        Predicate p = null;
        if (!name.isEmpty()) {
            p = criteriaBuilder.like(criteriaBuilder.lower(root.get(WorkTopic_.name)), "%" + name.toLowerCase() + "%");
        }

        if (creator != null) {
            Predicate p1 = criteriaBuilder.equal(root.get(WorkTopic_.creator), creator);
            if (p != null) {
                p = criteriaBuilder.and(p, p1);
            } else p = p1;

        }
        if (direction != null) {
            Predicate p1 = criteriaBuilder.equal(root.get(WorkTopic_.direction), direction);
            if (p != null) {
                p = criteriaBuilder.and(p, p1);
            } else p = p1;
        }

        if (p != null) {
            query.where(p);
        }
        query.orderBy(criteriaBuilder.asc(root.get(WorkTopic_.name)));
        return entityManager.createQuery(query).getResultList();
    }
}