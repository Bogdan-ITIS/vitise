package ru.kpfu.itis.group11501.vitise.model;

import javax.persistence.*;
import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 04.05.2017
 * Group: 11-501
 * Project: vITISe
 */
@Entity
@Table(name = "work_topic")
public class WorkTopic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "topic_name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "creator_id")
    private User creator;

    @ManyToOne
    @JoinColumn(name = "direction_id")
    private DirectionOfScientificActivity direction;

    @Column(name = "busy", insertable = false)
    private Boolean busy;

    @Transient
    private WorkTopicRequest currentUserRequest;

    @Transient
    private List<WorkTopicRequest> activeRequests;

    @Transient
    private User student;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public DirectionOfScientificActivity getDirection() {
        return direction;
    }

    public void setDirection(DirectionOfScientificActivity direction) {
        this.direction = direction;
    }

    public Boolean getBusy() {
        return busy;
    }

    public void setBusy(Boolean busy) {
        this.busy = busy;
    }

    public WorkTopicRequest getCurrentUserRequest() {
        return currentUserRequest;
    }

    public void setCurrentUserRequest(WorkTopicRequest currentUserRequest) {
        this.currentUserRequest = currentUserRequest;
    }

    public List<WorkTopicRequest> getActiveRequests() {
        return activeRequests;
    }

    public void setActiveRequests(List<WorkTopicRequest> activeRequests) {
        this.activeRequests = activeRequests;
    }

    public User getStudent() {
        return student;
    }

    public void setStudent(User student) {
        this.student = student;
    }
}