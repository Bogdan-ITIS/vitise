package ru.kpfu.itis.group11501.vitise.util;

import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.enums.SelectType;

/**
 * Created by Bogdan Popov on 12.05.2017.
 */
public class UserSearchingResponseAdapter extends SearchingResponse {

    private User user;

    public UserSearchingResponseAdapter(User user, SelectType type) {
        this.user = user;
        this.type = type;
    }

    @Override
    public Long getId() {
        return user.getId();
    }

    @Override
    public void setId(Long id) {
        user.setId(id);
    }

    @Override
    public String getName() {
        return user.getFullName();
    }

    @Override
    public void setName(String name) {
        user.setName(name);
    }
}
