package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.Laboratory;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.enums.SelectType;
import ru.kpfu.itis.group11501.vitise.util.SearchingResponse;

import java.util.List;

/**
 * Created by Наталья on 04.05.2017.
 */
public interface LaboratoryService {
    void add(Laboratory laboratory);

    List<Laboratory> getAll();

    Laboratory getOne(Long id);

    List<User> getStudentsFromLaboratory(Laboratory laboratory);

    Laboratory getLaboratory(User currentUser);

    List<SearchingResponse> getSearchingResponses(List<Laboratory> laboratories, SelectType type);

    List<Laboratory> getLaboratoriesByDirection(String direction);
}
