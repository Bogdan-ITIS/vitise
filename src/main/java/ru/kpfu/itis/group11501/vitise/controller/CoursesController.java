package ru.kpfu.itis.group11501.vitise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.group11501.vitise.model.*;
import ru.kpfu.itis.group11501.vitise.model.enums.StatusName;
import ru.kpfu.itis.group11501.vitise.service.CourseService;
import ru.kpfu.itis.group11501.vitise.service.CoursesRequestsService;
import ru.kpfu.itis.group11501.vitise.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Наталья on 04.05.2017.
 */
@Controller
public class CoursesController {

    private final UserService userService;
    private final CourseService courseService;
    private final CoursesRequestsService coursesRequestsService;

    @Autowired
    public CoursesController(UserService userService, CourseService courseService, CoursesRequestsService coursesRequestsService) {
        this.userService = userService;
        this.courseService = courseService;
        this.coursesRequestsService = coursesRequestsService;
    }

    @RequestMapping(value = "/deanery/courses/create", method = RequestMethod.GET)
    public String createCoursePage(Model model) {
        String[] args = new String[0];
        List<StatusName> statusNameList = new ArrayList<>();
        statusNameList.add(StatusName.WORKER);
        model.addAttribute("teachers", userService.filterUserListByCriteria(true, statusNameList, args));
        model.addAttribute("currentUser", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "create_course";
    }

    @RequestMapping(value = "/deanery/courses/create", method = RequestMethod.POST)
    public String createCourse(@RequestParam Map<String, String> allRequestParams) {
        Course course = new Course();
        Subject subject = new Subject();
        TeachersSubjects teachersSubjects = new TeachersSubjects();
        teachersSubjects.setTeacher(userService.findOne(Long.valueOf(allRequestParams.get("teacher"))));
        subject.setName(allRequestParams.get("name"));
        course.setDescription(allRequestParams.get("description"));
        courseService.add(course, subject, teachersSubjects);
        return "redirect:/courses";
    }

    @RequestMapping(value = "/deanery/courses/requests", method = RequestMethod.GET)
    public String requestsPage(Model model) {
        model.addAttribute("requests", coursesRequestsService.getRequests());
        model.addAttribute("currentUser", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "courses_requests";
    }

    @RequestMapping(value = "/deanery/courses/{id}/approve", method = RequestMethod.POST)
    public String approveRequest(@PathVariable("id") Long id, HttpServletRequest request) {
        CoursesRequests courseRequest = coursesRequestsService.getRequest(id);
        courseRequest.setActive(true);
        coursesRequestsService.update(courseRequest);
        return "redirect:" + request.getHeader("Referer");
    }

    @RequestMapping(value = "/deanery/courses/{id}/delete", method = RequestMethod.POST)
    public String deleteRequest(@PathVariable("id") Long id, HttpServletRequest request) {
        CoursesRequests courseRequest = coursesRequestsService.getRequest(id);
        courseRequest.setActive(false);
        coursesRequestsService.update(courseRequest);
        return "redirect:" + request.getHeader("Referer");
    }

    @RequestMapping(value = "/courses")
    public String allCourses(Model model) {
        model.addAttribute("courses", courseService.getCourses());
        model.addAttribute("currentUser", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "courses";
    }

    @RequestMapping(value = "/courses/{id}")
    public String coursePage(@PathVariable("id") Long courseId, Model model) {
        Course course = courseService.getOne(courseId);
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<User> students = courseService.getStudents(course);
        CoursesRequests request = coursesRequestsService.getRequest(course, currentUser);
        model.addAttribute("course", course);
        model.addAttribute("teachers", courseService.getTeachers(course.getSubject()));
        model.addAttribute("students", students);
        model.addAttribute("students_amount", students.size());
        model.addAttribute("is_student", userService.isStudent(currentUser));
        model.addAttribute("state", coursesRequestsService.getState(request));
        model.addAttribute("request_id", request != null ? request.getId() : null);
        model.addAttribute("currentUser", currentUser);
        return "course";
    }

    @RequestMapping(value = "/courses/enroll", method = RequestMethod.POST)
    public String createRequest(@RequestParam("id") Long courseId) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Course course = courseService.getOne(courseId);
        coursesRequestsService.add(currentUser, course);
        return "redirect:/courses/" + courseId;
    }

    @RequestMapping(value = "/courses/{id}/decline", method = RequestMethod.POST)
    public String declineRequest(@PathVariable("id") String id, @RequestParam("id") Long requestId) {
        coursesRequestsService.delete(requestId);
        return "redirect:/courses/" + id;
    }

    @RequestMapping(value = "/my_courses", method = RequestMethod.GET)
    public String myCoursesPage(Model model) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!userService.isStudent(currentUser)) {
            return "handle404";
        }
        model.addAttribute("courses", courseService.getMyCourses(currentUser));
        return "courses";
    }
}
