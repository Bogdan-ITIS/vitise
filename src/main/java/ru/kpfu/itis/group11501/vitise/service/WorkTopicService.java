package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.DirectionOfScientificActivity;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.WorkTopic;
import ru.kpfu.itis.group11501.vitise.model.WorkTopicRequest;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 04.05.2017
 * Group: 11-501
 * Project: vITISe
 */
public interface WorkTopicService {
    void addTopic(WorkTopic topic);

    WorkTopic getTopic(Long id);

    List<WorkTopic> getAllFreeTopics(User currentUser);

    List<WorkTopic> getAllFreeTopics(User currentUser, String name, User creator, DirectionOfScientificActivity direction);

    List<WorkTopic> getAllProfessorTopics(User user);

    List<User> getAllProfessors();

    WorkTopicRequest getTopicRequest(Long id);

    void addTopicRequest(WorkTopicRequest topicRequest);

    void deleteTopicRequest(WorkTopicRequest topicRequest);

    WorkTopicRequest getTopicRequest(WorkTopic topic, User student);

    List<WorkTopicRequest> getActiveStudentRequests(User user);

    List<WorkTopicRequest> getStudentRequests(User user);

    WorkTopicRequest getConfirmedStudentRequest(User user);

    boolean isStudentHaveTopic(User user);

    void approveRequest(WorkTopicRequest topicRequest);

    void rejectRequest(WorkTopicRequest topicRequest);

    List<WorkTopicRequest> getActiveProfessorRequests(User user);
}
