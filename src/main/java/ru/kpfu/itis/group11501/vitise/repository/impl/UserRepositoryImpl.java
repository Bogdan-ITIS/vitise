package ru.kpfu.itis.group11501.vitise.repository.impl;

import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.group11501.vitise.model.*;
import ru.kpfu.itis.group11501.vitise.repository.UserRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bogdan Popov on 07.04.2017.
 */
@Transactional
public class UserRepositoryImpl implements UserRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<User> findAllByIsActiveAndStatusIdAndName(Boolean isActive, List<Long> idStatusList, String[] args) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        // Что ищем
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        // Где ищем
        Root<UsersStatus> root = query.from(UsersStatus.class);
        Join<UsersStatus, User> join = root.join(UsersStatus_.user);
        Join<UsersStatus, Status> join2 = root.join(UsersStatus_.status);
        query.select(join);
        query.distinct(true);
        Predicate p;
        if (isActive == null) {
            p = criteriaBuilder.isNotNull(join.get(User_.isActive));
        } else {
            p = criteriaBuilder.equal(join.get(User_.isActive), isActive);
        }
        p = getPredicateForSearchingByStatusId(join2, idStatusList, criteriaBuilder, p);
        p = searchByName(join, args, p);
        query.orderBy(criteriaBuilder.asc(join.get(User_.surname)));
        query.where(p);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public List<User> findAllByIsActiveNotNullAndName(String[] args) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<User> root = query.from(User.class);
        query.select(root);
        Predicate p1 = criteriaBuilder.isNotNull(root.get(User_.isActive));
        Predicate p = searchByName(root, args, p1);
        query.where(p);
        return entityManager.createQuery(query).getResultList();
    }

    @Override
    public List<User> findAllNewUsersByNameAndStatus(String[] args, Status status) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
        Root<UsersStatus> root = query.from(UsersStatus.class);
        Join<UsersStatus, User> join = root.join(UsersStatus_.user);
        Join<UsersStatus, Status> join2 = root.join(UsersStatus_.status);
        query.select(join);
        query.distinct(true);
        Predicate p;
        p = criteriaBuilder.isNull(join.get(User_.isActive));
        List<Long> idStatusList = new ArrayList<>();
        idStatusList.add(status.getId());
        p = getPredicateForSearchingByStatusId(join2, idStatusList, criteriaBuilder, p);
        p = searchByName(join, args, p);
        query.where(p);
        return entityManager.createQuery(query).getResultList();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    private Predicate searchByName(Path<User> path, String[] args, Predicate p) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        for (String arg : args) {
            if (!arg.isEmpty()) {
                Predicate p3 = criteriaBuilder.like(criteriaBuilder.lower(path.get(User_.name)), arg.toLowerCase() + "%");
                Predicate p4 = criteriaBuilder.like(criteriaBuilder.lower(path.get(User_.surname)), arg.toLowerCase() + "%");
                Predicate p5 = criteriaBuilder.like(criteriaBuilder.lower(path.get(User_.thirdName)), arg.toLowerCase() + "%");
                p3 = criteriaBuilder.or(p3, p4);
                p5 = criteriaBuilder.or(p4, p5);
                p4 = criteriaBuilder.or(p3, p5);
                p = criteriaBuilder.and(p, p4);
            }
        }
        return p;
    }

    private Predicate getPredicateForSearchingByStatusId(Path<Status> path, List<Long> idStatusList, CriteriaBuilder criteriaBuilder, Predicate p) {
        if (!idStatusList.isEmpty()) {
            Predicate p2 = criteriaBuilder.equal(path.get(Status_.id), idStatusList.remove(0));
            for (Long id : idStatusList) {
                Predicate p1 = criteriaBuilder.equal(path.get(Status_.id), id);
                p2 = criteriaBuilder.or(p2, p1);
            }
            p = criteriaBuilder.and(p, p2);
        }
        return p;
    }
}
