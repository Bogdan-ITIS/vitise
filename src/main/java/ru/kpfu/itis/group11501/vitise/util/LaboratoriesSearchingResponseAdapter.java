package ru.kpfu.itis.group11501.vitise.util;

import ru.kpfu.itis.group11501.vitise.model.Laboratory;
import ru.kpfu.itis.group11501.vitise.model.enums.SelectType;

/**
 * Created by Bogdan Popov on 12.05.2017.
 */
public class LaboratoriesSearchingResponseAdapter extends SearchingResponse {
    private Laboratory laboratory;

    public LaboratoriesSearchingResponseAdapter(Laboratory laboratory, SelectType type) {
        this.laboratory = laboratory;
        this.type = type;
    }

    @Override
    public Long getId() {
        return laboratory.getId();
    }

    @Override
    public void setId(Long id) {
        laboratory.setId(id);
    }

    @Override
    public String getName() {
        return laboratory.getName();
    }

    @Override
    public void setName(String name) {
        laboratory.setName(name);
    }
}
