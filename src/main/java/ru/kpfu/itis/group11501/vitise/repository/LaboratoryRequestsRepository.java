package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.itis.group11501.vitise.model.Laboratory;
import ru.kpfu.itis.group11501.vitise.model.LaboratoryRequests;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.List;

/**
 * Created by Наталья on 05.05.2017.
 */
public interface LaboratoryRequestsRepository extends JpaRepository<LaboratoryRequests, Long> {
    List<LaboratoryRequests> findAllByLaboratoryAndIsActive(Laboratory laboratory, Boolean isActive);

    LaboratoryRequests findOneByLaboratoryAndUser(Laboratory laboratory, User user);

    @Query("select lab from LaboratoryRequests lab where lab.user = :user and not lab.isActive = false")
    LaboratoryRequests findAnyRequest(@Param("user") User user);

    List<LaboratoryRequests> findAllByIsActive(Boolean isActive);

    LaboratoryRequests findOneByUserAndIsActive(User currentUser, Boolean b);
}
