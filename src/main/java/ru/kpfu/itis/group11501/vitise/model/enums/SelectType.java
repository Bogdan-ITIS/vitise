package ru.kpfu.itis.group11501.vitise.model.enums;

/**
 * Created by Bogdan Popov on 12.05.2017.
 */
public enum SelectType {
    LECTURER("Преподаватель", "lecturer"),
    LABORATORY("Лаборатория", "laboratory");

    private final String rusName;
    private final String simpleName;

    SelectType(String rusName, String simpleName) {
        this.rusName = rusName;
        this.simpleName = simpleName;
    }

    public String getRusName() {
        return rusName;
    }

    public String getSimpleName() {
        return simpleName;
    }
}
