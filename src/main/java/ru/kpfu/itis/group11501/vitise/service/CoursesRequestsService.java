package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.Course;
import ru.kpfu.itis.group11501.vitise.model.CoursesRequests;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.List;

/**
 * Created by Наталья on 05.05.2017.
 */
public interface CoursesRequestsService {

    CoursesRequests getRequest(Course course, User user);

    int getState(CoursesRequests coursesRequests);

    void delete(Long id);

    void add(User currentUser, Course course);

    List<CoursesRequests> getRequests();

    CoursesRequests getRequest(Long id);

    void update(CoursesRequests courseRequest);
}
