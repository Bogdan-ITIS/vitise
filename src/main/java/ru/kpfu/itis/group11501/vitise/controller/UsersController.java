package ru.kpfu.itis.group11501.vitise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.group11501.vitise.model.Colleagues;
import ru.kpfu.itis.group11501.vitise.model.DirectionOfScientificActivity;
import ru.kpfu.itis.group11501.vitise.model.News;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.service.*;
import ru.kpfu.itis.group11501.vitise.util.EmailType;
import ru.kpfu.itis.group11501.vitise.util.FileUploader;
import ru.kpfu.itis.group11501.vitise.util.SendEmailTask;
import ru.kpfu.itis.group11501.vitise.util.TokenGenerator;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.Executor;

/**
 * Created by Наталья on 22.03.2017.
 */
@Controller
public class UsersController {
    private static final PasswordEncoder encoder = new BCryptPasswordEncoder();
    private final UserService userService;
    private final ColleaguesService colleaguesService;
    private final DirectionOfScientificActivityService directionOfScientificActivityService;
    private final DirectionOfScientificActivityUserService directionOfScientificActivityUserService;
    private final StudentService studentService;
    private final CommonNewsService commonNewsService;
    private final Executor executor;
    private final MailService mailService;
    private final LaboratoryService laboratoryService;
    private final WorkTopicService topicService;

    @Autowired
    public UsersController(UserService userService, ColleaguesService colleaguesService,
                           DirectionOfScientificActivityService directionOfScientificActivityService,
                           DirectionOfScientificActivityUserService directionOfScientificActivityUserService,
                           StudentService studentService, CommonNewsService commonNewsService,
                           @Qualifier("getAsyncExecutor") Executor executor, MailService mailService,
                           LaboratoryService laboratoryService, WorkTopicService topicService) {
        this.userService = userService;
        this.colleaguesService = colleaguesService;
        this.directionOfScientificActivityService = directionOfScientificActivityService;
        this.directionOfScientificActivityUserService = directionOfScientificActivityUserService;
        this.studentService = studentService;
        this.commonNewsService = commonNewsService;
        this.executor = executor;
        this.mailService = mailService;
        this.laboratoryService = laboratoryService;
        this.topicService = topicService;
    }

    @RequestMapping("/user/{id}")
    public String userPage(@PathVariable(name = "id") String id, Model model) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userService.findOne(Long.valueOf(id));
        User admin = userService.getAdmin();
        if (user == null) {
            return "handle404";
        }
        if (Objects.equals(currentUser.getId(), user.getId())) {
            return "redirect:/profile";
        } else if (admin != null && Objects.equals(admin.getId(), user.getId())) {
            return "handle404";
        } else if (!Objects.equals(currentUser.getId(), user.getId())) {
            Colleagues c = colleaguesService.getColleagues(currentUser, user);
            model.addAttribute("state", colleaguesService.getState(c, currentUser, user));
            model.addAttribute("colleagues", c);
        }
        model.addAttribute("is_admin", admin != null && Objects.equals(admin.getId(), currentUser.getId()));
        model.addAttribute("group", studentService.getStudentGroup(user));
        model.addAttribute("lab", laboratoryService.getLaboratory(user));
        model.addAttribute("userPage", user);
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("newses", commonNewsService.getAllUsersNewsAndEvents(user, currentUser));
        return "profile_user";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String getProfilePage(Model model, HttpServletRequest request) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        userService.setStatus(currentUser);
        model.addAttribute("group", studentService.getStudentGroup(currentUser));
        model.addAttribute("lab", laboratoryService.getLaboratory(currentUser));
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("news", new News());
        model.addAttribute("newses", commonNewsService.getAllUsersNewsAndEvents(currentUser, currentUser, 1));
        if (request.isUserInRole("ROLE_STUDENT")) {
            model.addAttribute("isStudentHaveTopic", topicService.isStudentHaveTopic(currentUser));
        }
        return "profile";
    }

    @RequestMapping(value = "/profile/change", method = RequestMethod.GET)
    public String getChangeProfile(Model model, @RequestParam(value = "error", required = false) Boolean error) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("currentUser", currentUser);
        model.addAttribute("isWorker", userService.isWorker(currentUser));
        Map<String, String> directions = new HashMap<>();
        List<DirectionOfScientificActivity> directionList = directionOfScientificActivityService.findAll();
        List<DirectionOfScientificActivity> userDirectionList = directionOfScientificActivityService.findByUser(currentUser);
        for (DirectionOfScientificActivity d : directionList) {
            if (isContain(userDirectionList, d)) {
                directions.put(d.getName(), "checked");
            } else {
                directions.put(d.getName(), "");
            }
        }
        System.out.println(directions);
        model.addAttribute("directions", directions);
        return "profile_change";
    }

    @RequestMapping(value = "/profile/change", method = RequestMethod.POST)
    public Object ChangeProfile(Model model,
                                @RequestParam("file") MultipartFile file,
                                @RequestParam Map<String, String> allRequestParams, HttpServletRequest request) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!allRequestParams.get("password").equals("")) {
            if (encoder.matches(allRequestParams.get("password"), currentUser.getPassword()) &&
                    Objects.equals(allRequestParams.get("newPassword"), allRequestParams.get("newPasswordConfirmation"))) {
                currentUser.setPassword(encoder.encode(allRequestParams.get("newPassword")));
            }
        }
        currentUser.setName(allRequestParams.get("name"));
        currentUser.setSurname(allRequestParams.get("surname"));
        currentUser.setThirdName(allRequestParams.get("thirdName"));
        currentUser.setTelephoneNumber(allRequestParams.get("telephoneNumber"));

        if (!currentUser.getEmail().equals(allRequestParams.get("email"))) {
            currentUser.setNewEmail(allRequestParams.get("email"));
            currentUser.setToken(TokenGenerator.generate());
            executor.execute(
                    new SendEmailTask(currentUser, request.getHeader("Host"), EmailType.MAIl_CONFIRMATION, mailService));
        }

        String name = FileUploader.uploadFile(file);
        currentUser.setPhotoName(name);
        currentUser.setDescription(allRequestParams.get("description"));
        List<String> directions = new ArrayList<>();
        List<DirectionOfScientificActivity> directionOfScientificActivities = directionOfScientificActivityService.findAll();
        for (DirectionOfScientificActivity d : directionOfScientificActivities) {
            if (allRequestParams.get(d.getName()) != null) {
                directions.add(allRequestParams.get(d.getName()));
            }
        }
        directionOfScientificActivityUserService.add(currentUser, directions);

        userService.save(currentUser);
        return "redirect:/profile";
    }

    @RequestMapping(value = "/users")
    public String getUsers(Model model) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String[] args = new String[0];
        List<User> users = userService.getUsers(args);
        users.remove(currentUser);
        model.addAttribute("users", users);
        model.addAttribute("currentUser", currentUser);
        return "user_list";
    }

    @RequestMapping(value = "/users/search")
    public String searchUser(Model model, @RequestParam(name = "name", required = false) String name) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String[] args = name.split(" ");
        List<User> users = userService.getUsers(args);
        users.remove(currentUser);
        model.addAttribute("users", users);
        model.addAttribute("currentUser", currentUser);
        return "user_list";
    }

    private boolean isContain(List<DirectionOfScientificActivity> directionOfScientificActivities, DirectionOfScientificActivity d) {
        for (DirectionOfScientificActivity direction : directionOfScientificActivities) {
            if (direction.getName().equals(d.getName())) {
                return true;
            }
        }
        return false;
    }

    @RequestMapping(value = "/password_check")
    @ResponseBody
    public String passwordCheck(@RequestParam(name = "password", required = false) String password) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (encoder.matches(password, currentUser.getPassword())) {
            return "";
        }
        return "Wrong password";
    }

    @RequestMapping(value = "/profile/upload_photo", method = RequestMethod.POST)
    public String uploadPhoto(@RequestParam("photoname") MultipartFile photo) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (photo != null) {
            String name = FileUploader.uploadFile(photo);
            currentUser.setPhotoName(name);
        }
        userService.save(currentUser);
        return "redirect:/profile";
    }
}
