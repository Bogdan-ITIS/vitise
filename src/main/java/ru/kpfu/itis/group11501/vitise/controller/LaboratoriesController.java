package ru.kpfu.itis.group11501.vitise.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.group11501.vitise.model.Laboratory;
import ru.kpfu.itis.group11501.vitise.model.LaboratoryRequests;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.enums.StatusName;
import ru.kpfu.itis.group11501.vitise.service.DirectionOfScientificActivityService;
import ru.kpfu.itis.group11501.vitise.service.LaboratoryRequestsService;
import ru.kpfu.itis.group11501.vitise.service.LaboratoryService;
import ru.kpfu.itis.group11501.vitise.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Наталья on 04.05.2017.
 */
@Controller
public class LaboratoriesController {

    private final DirectionOfScientificActivityService directionsService;
    private final UserService userService;
    private final LaboratoryService laboratoryService;
    private final LaboratoryRequestsService laboratoryRequestsService;

    @Autowired
    public LaboratoriesController(DirectionOfScientificActivityService directionsService, UserService userService, LaboratoryService laboratoryService, LaboratoryRequestsService laboratoryRequestsService) {
        this.directionsService = directionsService;
        this.userService = userService;
        this.laboratoryService = laboratoryService;
        this.laboratoryRequestsService = laboratoryRequestsService;
    }

    @RequestMapping(value = "/deanery/laboratories/create", method = RequestMethod.GET)
    public String createLaboratoryPage(Model model) {
        String[] args = new String[0];
        List<StatusName> statusNameList = new ArrayList<>();
        statusNameList.add(StatusName.WORKER);
        model.addAttribute("directions", directionsService.findAll());
        model.addAttribute("teachers", userService.filterUserListByCriteria(true, statusNameList, args));
        model.addAttribute("currentUser", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "create_laboratory";
    }

    @RequestMapping(value = "/deanery/laboratories/create", method = RequestMethod.POST)
    public String createLaboratory(@RequestParam Map<String, String> allRequestParams) {
        Laboratory laboratory = new Laboratory();
        laboratory.setName(allRequestParams.get("name"));
        laboratory.setDescription(allRequestParams.get("description"));
        laboratory.setDirection(directionsService.findOne(Long.valueOf(allRequestParams.get("direction"))));
        laboratory.setHead(userService.getUser(Long.valueOf(allRequestParams.get("teacher"))));
        laboratoryService.add(laboratory);
        return "redirect:/laboratories";
    }

    @RequestMapping(value = "/deanery/laboratories/requests", method = RequestMethod.GET)
    public String requestsPage(Model model) {
        model.addAttribute("requests", laboratoryRequestsService.getRequests());
        model.addAttribute("currentUser", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "laboratories_requests";
    }

    @RequestMapping(value = "/deanery/laboratories/{id}/approve", method = RequestMethod.POST)
    public String approveRequest(@PathVariable("id") Long id, HttpServletRequest request) {
        LaboratoryRequests labRequest = laboratoryRequestsService.getRequest(id);
        labRequest.setActive(true);
        laboratoryRequestsService.update(labRequest);
        return "redirect:" + request.getHeader("Referer");
    }

    @RequestMapping(value = "/deanery/laboratories/{id}/delete", method = RequestMethod.POST)
    public String deleteRequest(@PathVariable("id") Long id, HttpServletRequest request) {
        LaboratoryRequests labRequest = laboratoryRequestsService.getRequest(id);
        labRequest.setActive(false);
        laboratoryRequestsService.update(labRequest);
        return "redirect:" + request.getHeader("Referer");
    }

    @RequestMapping(value = "/laboratories", method = RequestMethod.GET)
    public String allLaboratories(Model model) {
        model.addAttribute("laboratories", laboratoryService.getAll());
        model.addAttribute("currentUser", SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return "laboratories";
    }

    @RequestMapping(value = "/laboratories/{id}", method = RequestMethod.GET)
    public String allLaboratories(@PathVariable("id") Long id, Model model) {
        Laboratory laboratory = laboratoryService.getOne(id);
        List<User> students = laboratoryService.getStudentsFromLaboratory(laboratory);
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        LaboratoryRequests request = laboratoryRequestsService.getRequest(laboratory, currentUser);
        model.addAttribute("laboratory", laboratory);
        model.addAttribute("students", students);
        model.addAttribute("students_amount", students.size());
        model.addAttribute("is_student", userService.isStudent(currentUser));
        model.addAttribute("state", laboratoryRequestsService.getState(request, currentUser));
        model.addAttribute("request_id", request != null ? request.getId() : null);
        model.addAttribute("currentUser", currentUser);
        return "laboratory";
    }

    @RequestMapping(value = "/laboratories/enroll", method = RequestMethod.POST)
    public String createRequest(@RequestParam("id") Long laboratoryId) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Laboratory laboratory = laboratoryService.getOne(laboratoryId);
        laboratoryRequestsService.add(currentUser, laboratory);
        return "redirect:/laboratories/" + laboratoryId;
    }

    @RequestMapping(value = "/laboratories/{id}/decline", method = RequestMethod.POST)
    public String declineRequest(@PathVariable("id") String id, @RequestParam("id") Long requestId) {
        laboratoryRequestsService.delete(requestId);
        return "redirect:/laboratories/" + id;
    }
}
