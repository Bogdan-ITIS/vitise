package ru.kpfu.itis.group11501.vitise.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import ru.kpfu.itis.group11501.vitise.service.StompSessionService;

/**
 * Created by Bogdan Popov on 05.05.2017.
 */
@Component
public class DisconnectStompEventHandler implements ApplicationListener<SessionDisconnectEvent> {
    private StompSessionService stompSessionService;

    @Autowired
    public DisconnectStompEventHandler(StompSessionService stompSessionService) {
        this.stompSessionService = stompSessionService;
    }

    @Override
    public void onApplicationEvent(SessionDisconnectEvent event) {
        StompHeaderAccessor headers = StompHeaderAccessor.wrap(event.getMessage());
        String sessionId = headers.getSessionId();
        stompSessionService.updateMessagingForSession(sessionId);
        stompSessionService.deleteSession(sessionId);
    }
}
