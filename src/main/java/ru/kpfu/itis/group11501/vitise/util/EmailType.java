package ru.kpfu.itis.group11501.vitise.util;

/**
 * Author: Svintenok Kate
 * Date: 28.04.2017
 * Group: 11-501
 * Project: vITISe
 */
public enum EmailType {
    MAIl_CONFIRMATION, DEANERY_REGISTRATION, PASSWORD_RECOVERY
}
