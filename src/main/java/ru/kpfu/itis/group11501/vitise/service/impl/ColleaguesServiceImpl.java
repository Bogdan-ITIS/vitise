package ru.kpfu.itis.group11501.vitise.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.Colleagues;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.repository.ColleaguesRepository;
import ru.kpfu.itis.group11501.vitise.service.ColleaguesService;
import ru.kpfu.itis.group11501.vitise.service.UserService;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Наталья on 22.03.2017.
 */
@Service
public class ColleaguesServiceImpl implements ColleaguesService {

    private final ColleaguesRepository colleaguesRepository;
    private final UserService userService;

    @Autowired
    public ColleaguesServiceImpl(ColleaguesRepository colleaguesRepository, UserService userService) {
        this.colleaguesRepository = colleaguesRepository;
        this.userService = userService;
    }

    @Override
    public Colleagues getColleagues(User userOne, User userTwo) {
        try {
            return colleaguesRepository.getColleagues(userOne, userTwo);
        } catch (NoResultException e) {
            return null;
        }
    }

    @Override
    public List<Colleagues> getRequestSender(User sender) {
        List<Colleagues> requests = colleaguesRepository.getRequestSender(sender);
        this.setUserStatuses(requests);
        return requests;
    }

    @Override
    public List<Colleagues> getRequestReceiver(User receiver) {
        List<Colleagues> receivers = colleaguesRepository.getRequestReceiver(receiver);
        this.setUserStatuses(receivers);
        return receivers;
    }

    @Override
    public List<Colleagues> getAllColleagues(User user) {
        List<Colleagues> colleaguesList = colleaguesRepository.getAllColleagues(user);
        this.setUserStatuses(colleaguesList);
        return colleaguesList;
    }

    @Override
    public List<User> getColleagueUsers(User user) {
        List<User> colleagues = new ArrayList<>();
        for (Colleagues colleague : this.getAllColleagues(user)) {
            if (user.getId() != colleague.getReceiver().getId())
                colleagues.add(colleague.getReceiver());
            else
                colleagues.add(colleague.getSender());
        }
        return colleagues;
    }

    @Override
    public int getRequestsCount(User user) {
        return colleaguesRepository.getRequestsCount(user);
    }

    @Override
    public int getState(Colleagues colleagues, User currentUser, User user) {
        if (colleagues == null) {
            return 0;
        } else if (Objects.equals(colleagues.getSender().getId(), currentUser.getId()) && !colleagues.isActive()) {
            return -1;
        } else if (Objects.equals(colleagues.getSender().getId(), user.getId()) && !colleagues.isActive()) {
            return -2;
        } else {
            return 1;
        }
    }

    @Override
    public void add(User currentUser, User user) {
        Colleagues colleagues = new Colleagues();
        colleagues.setSender(currentUser);
        colleagues.setReceiver(user);
        colleagues.setActive(false);
        colleaguesRepository.save(colleagues);
    }

    @Override
    public void delete(Long id) {
        colleaguesRepository.delete(id);
    }

    @Override
    public void approve(Long id) {
        Colleagues colleagues = colleaguesRepository.findOne(id);
        colleagues.setActive(true);
        colleaguesRepository.save(colleagues);
    }

    private List<Colleagues> setUserStatuses(List<Colleagues> colleaguesList) {
        for (Colleagues colleagues : colleaguesList) {
            userService.setStatus(colleagues.getReceiver());
            userService.setStatus(colleagues.getSender());
        }
        return colleaguesList;
    }
}
