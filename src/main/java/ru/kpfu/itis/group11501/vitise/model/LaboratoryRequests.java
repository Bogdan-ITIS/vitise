package ru.kpfu.itis.group11501.vitise.model;

import javax.persistence.*;

/**
 * Created by Наталья on 04.05.2017.
 */
@Entity
@Table(name = "laboratory_requests")
public class LaboratoryRequests {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "laboratoriesid")
    private Laboratory laboratory;

    @ManyToOne
    @JoinColumn(name = "usersid")
    private User user;

    @Column(name = "is_active")
    private Boolean isActive;

    public LaboratoryRequests() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Laboratory getLaboratory() {
        return laboratory;
    }

    public void setLaboratory(Laboratory laboratory) {
        this.laboratory = laboratory;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }
}
