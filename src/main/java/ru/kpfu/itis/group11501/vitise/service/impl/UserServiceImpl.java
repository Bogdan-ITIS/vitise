package ru.kpfu.itis.group11501.vitise.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.vitise.model.Group;
import ru.kpfu.itis.group11501.vitise.model.Status;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.UsersStatus;
import ru.kpfu.itis.group11501.vitise.model.enums.SelectType;
import ru.kpfu.itis.group11501.vitise.model.enums.StatusName;
import ru.kpfu.itis.group11501.vitise.util.SearchingResponse;
import ru.kpfu.itis.group11501.vitise.pojo.UserPOJO;
import ru.kpfu.itis.group11501.vitise.repository.StatusRepository;
import ru.kpfu.itis.group11501.vitise.repository.UserRepository;
import ru.kpfu.itis.group11501.vitise.repository.UsersStatusRepository;
import ru.kpfu.itis.group11501.vitise.service.StatusService;
import ru.kpfu.itis.group11501.vitise.service.StudentService;
import ru.kpfu.itis.group11501.vitise.service.UserService;
import ru.kpfu.itis.group11501.vitise.util.UserSearchingResponseAdapter;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UsersStatusRepository usersStatusRepository;
    private final StatusService statusService;
    private final StudentService studentService;
    private final StatusRepository statusRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,
                           UsersStatusRepository usersStatusRepository,
                           StatusService statusService,
                           StudentService studentService, StatusRepository statusRepository) {
        this.userRepository = userRepository;
        this.usersStatusRepository = usersStatusRepository;
        this.statusService = statusService;
        this.studentService = studentService;
        this.statusRepository = statusRepository;
    }

    @Override
    public void save(User user) {
        userRepository.save(user);
    }

    @Override
    public User getUser(String email) {
        return userRepository.findOneByEmail(email);
    }

    @Override
    public User getUserByToken(String token) {
        return userRepository.findOneByToken(token);
    }

    @Override
    public List<User> searchNewUserByNameAndStatus(String[] args, Status status) {
        return userRepository.findAllNewUsersByNameAndStatus(args, status);
    }

    @Override
    public List<UserPOJO> convertToPOJO(List<User> userList) {
        List<UserPOJO> pojoList = new ArrayList<>();
        for (User user : userList) {
            UserPOJO pojo = new UserPOJO();
            pojo.setId(user.getId());
            pojo.setName(user.getName());
            pojo.setSurname(user.getSurname());
            pojo.setThirdName(user.getThirdName());
            pojo.setGroupName(studentService.getStudentGroup(user).getName());
            pojoList.add(pojo);
        }
        return pojoList;
    }

    @Override
    public UserPOJO convertToPOJO(User user) {
        UserPOJO pojo = new UserPOJO();
        pojo.setId(user.getId());
        pojo.setName(user.getName());
        pojo.setSurname(user.getSurname());
        pojo.setThirdName(user.getThirdName());
        Group group = studentService.getStudentGroup(user);
        if (group != null) {
            pojo.setGroupName(group.getName());
        } else {
            pojo.setGroupName(null);
        }
        return pojo;
    }

    @Override
    public List<User> getLecturersByDirection(String direction) {
        Status status = statusService.getStatus(StatusName.TEACHER);
        try {
            Long directionId = Long.valueOf(direction);
            return userRepository.findAllByDirectionAndStatusAndIsActive(directionId, status, true);
        }
        catch (NumberFormatException e) {
            return userRepository.findAllByStatus(status);
        }
    }

    @Override
    public List<SearchingResponse> getSearchingResponses(List<User> users, SelectType type) {
        List<SearchingResponse> searchingResponses = new ArrayList<>();
        for (User user : users) {
            SearchingResponse searchingResponse = new UserSearchingResponseAdapter(user, type);
            searchingResponses.add(searchingResponse);
        }
        return searchingResponses;
    }

    @Override
    public User findOne(Long id) {
        User user = userRepository.findOne(id);
        return user;
    }

    @Override
    public User getAdmin() {
        Status status = statusRepository.findOneByName("ADMIN");
        List<UsersStatus> usersStatuses = usersStatusRepository.findAllByStatusId(status.getId());
        if (usersStatuses.size() != 0) {
            return usersStatuses.get(0).getUser();
        }
        return null;
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public List<Status> getStatus(User user) {
        List<Status> statuses = new ArrayList<>();
        List<UsersStatus> usersStatuses = usersStatusRepository.findAllStatusIdByUserId(user.getId());
        for (UsersStatus usersStatus : usersStatuses) {
            statuses.add(usersStatus.getStatus());
        }
        return statuses;
    }

    @Override
    public List<User> getNewUsers(Status status) {
        List<User> users = userRepository.findNewByStatus(status);
        for (User user : users)
            this.setStatus(user);
        return users;
    }

    @Override
    public List<User> getNewUsers() {
        return userRepository.findAllByIsActive(null);
    }

    @Override
    public void approveUser(Long id) {
        User user = getUser(id);
        user.setActive(true);
        userRepository.save(user);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.delete(id);
    }

    @Override
    public boolean isWorker(User user) {
        List<Status> statuses = getStatus(user);
        for (Status status : statuses) {
            if (status.getName().equals("WORKER")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void changeState(Long id) {
        User user = getUser(id);
        if (user.isActive()) {
            user.setActive(false);
        } else {
            user.setActive(true);
        }
        userRepository.save(user);
    }


    @Override
    public List<User> filterUserListByCriteria(Boolean isActive, List<StatusName> statusNameList, String[] args) {
        List<Long> statusIdList = new ArrayList<>();
        for (StatusName statusName : statusNameList) {
            statusIdList.add(statusService.getStatus(statusName).getId());
        }
        return userRepository.findAllByIsActiveAndStatusIdAndName(isActive, statusIdList, args);
    }

    @Override
    public User setStatus(User user) {
        List<Status> statuses = getStatus(user);
        if (this.isWorker(user)) {
            for (Status status : statuses)
                if (!status.getName().equals("WORKER"))
                    user.setStatusName(StatusName.valueOf(status.getName()));
        } else if (this.isStudent(user)) {
            user.setStatusName(StatusName.STUDENT);
            user.setGroup(studentService.getStudentGroup(user));
        } else user.setStatusName(StatusName.valueOf(statuses.get(0).getName()));
        return user;
    }

    public List<User> getUsers(String[] args) {
        return userRepository.findAllByIsActiveNotNullAndName(args);
    }


    @Override
    public boolean isStudent(User user) {
        List<Status> statuses = getStatus(user);
        for (Status status : statuses) {
            if (status.getName().equals("STUDENT")) {
                return true;
            }
        }
        return false;
    }
}