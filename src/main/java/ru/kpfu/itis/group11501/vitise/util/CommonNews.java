package ru.kpfu.itis.group11501.vitise.util;

import ru.kpfu.itis.group11501.vitise.model.NewsFile;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.Date;
import java.util.List;

/**
 * Created by Наталья on 03.04.2017.
 */
abstract public class CommonNews implements Comparable {

    abstract public Long getSubscriptionsCount();

    abstract public boolean isSubscribeStatus();

    abstract public Long getId();

    abstract public String getTopic();

    abstract public String getText();

    abstract public User getAuthor();

    abstract public Date getPubDate();

    abstract public Date getEventDate();

    abstract public boolean isEvent();

    abstract public List<NewsFile> getFiles();

    @Override
    public int compareTo(Object o) {
        CommonNews commonNews = (CommonNews) o;

        Date date1 = this.getPubDate();
        Date date2 = commonNews.getPubDate();

        if (date1.after(date2)) {
            return -1;
        } else if (date1.before(date2)) {
            return 1;
        }
        return 0;
    }
}
