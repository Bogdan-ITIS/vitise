package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.vitise.model.TeachersSubjects;

import java.util.List;

/**
 * Created by Наталья on 04.05.2017.
 */
public interface TeachersSubjectsRepository extends JpaRepository<TeachersSubjects, Long> {

    List<TeachersSubjects> findAllBySubjectId(Long subjectId);
}
