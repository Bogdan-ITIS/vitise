package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.Laboratory;
import ru.kpfu.itis.group11501.vitise.model.LaboratoryRequests;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.List;

/**
 * Created by Наталья on 05.05.2017.
 */
public interface LaboratoryRequestsService {
    LaboratoryRequests getRequest(Laboratory laboratory, User user);

    int getState(LaboratoryRequests laboratoryRequests, User currentUser);

    void delete(Long id);

    void add(User currentUser, Laboratory laboratory);

    LaboratoryRequests getRequest(Long id);

    void update(LaboratoryRequests labRequest);

    List<LaboratoryRequests> getRequests();
}
