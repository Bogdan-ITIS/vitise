package ru.kpfu.itis.group11501.vitise.pojo;

/**
 * Created by Bogdan Popov on 29.04.2017.
 */
public class UserPOJO {
    private Long id;

    private String name;

    private String surname;

    private String thirdName;

    private String groupName;

    public UserPOJO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getThirdName() {
        return thirdName;
    }

    public void setThirdName(String thirdName) {
        this.thirdName = thirdName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
