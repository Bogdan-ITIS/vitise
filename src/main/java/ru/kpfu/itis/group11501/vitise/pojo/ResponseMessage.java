package ru.kpfu.itis.group11501.vitise.pojo;

import ru.kpfu.itis.group11501.vitise.pojo.enums.MessagingType;

/**
 * Created by Bogdan Popov on 26.04.2017.
 */
public class ResponseMessage {
    private String content;
    private String date;
    private String idMessaging;
    private String type;
    private UserPOJO sender;
    private String nameMessaging;

    public ResponseMessage() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIdMessaging() {
        return idMessaging;
    }

    public void setIdMessaging(String idMessaging) {
        this.idMessaging = idMessaging;
    }

    public String getType() {
        return type;
    }

    public void setType(MessagingType type) {
        this.type = type.getSimpleName();
    }

    public UserPOJO getSender() {
        return sender;
    }

    public void setSender(UserPOJO sender) {
        this.sender = sender;
    }

    public String getNameMessaging() {
        return nameMessaging;
    }

    public void setNameMessaging(String nameMessaging) {
        this.nameMessaging = nameMessaging;
    }
}
