package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.vitise.model.Subject;

/**
 * Created by Наталья on 04.05.2017.
 */
public interface SubjectRepository extends JpaRepository<Subject, Long> {
}
