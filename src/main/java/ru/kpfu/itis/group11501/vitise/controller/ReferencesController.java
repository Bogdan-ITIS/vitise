package ru.kpfu.itis.group11501.vitise.controller;

import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.group11501.vitise.model.*;
import ru.kpfu.itis.group11501.vitise.service.CommunicationRelationService;
import ru.kpfu.itis.group11501.vitise.service.ReferenceRequestService;
import ru.kpfu.itis.group11501.vitise.service.ReferenceService;
import ru.kpfu.itis.group11501.vitise.service.UserMessageService;
import ru.kpfu.itis.group11501.vitise.util.ReferenceGenerator;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * Created by Марат on 06.05.2017.
 */
@Controller
public class ReferencesController {
    private final ReferenceRequestService referenceRequestService;
    private final ReferenceService referenceService;
    private final CommunicationRelationService communicationRelationsService;
    private final UserMessageService usersMessagesService;

    @Autowired
    public ReferencesController(ReferenceRequestService referenceRequestService, ReferenceService referenceService, CommunicationRelationService communicationRelationsService, UserMessageService usersMessagesService) {
        this.referenceRequestService = referenceRequestService;
        this.referenceService = referenceService;
        this.communicationRelationsService = communicationRelationsService;
        this.usersMessagesService = usersMessagesService;
    }

    @RequestMapping(value = "/deanery/references_requests", method = RequestMethod.GET)
    public String getRequests(Model model) {
        model.addAttribute("currentUser", (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        model.addAttribute("requests", referenceRequestService.getAllActive());
        return "references_requests";
    }

    @RequestMapping(value = "/references_requests/create", method = RequestMethod.POST)
    public String addRequest(@RequestParam Map<String, String> allRequestParams, HttpServletRequest request) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Reference reference = new Reference();
        reference.setQuantity(1);
        reference.setType(1);
        referenceService.add(reference);
        ReferenceRequest referenceRequest = new ReferenceRequest();
        referenceRequest.setReference(reference);
        referenceRequest.setUser(currentUser);
        referenceRequest.setActive(true);
        referenceRequest.setRequestDate(new Date(System.currentTimeMillis()));
        referenceRequestService.add(referenceRequest);
        return "redirect:/profile";
    }

    @RequestMapping(value = "/deanery/references_requests/{id}/approve", method = RequestMethod.POST)
    public String approveRequest(@PathVariable("id") Long id) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ReferenceRequest referenceRequest = referenceRequestService.getOne(id);
        User user = referenceRequest.getUser();
        referenceRequest.setActive(false);
        Reference reference = referenceRequest.getReference();
        String filename = "mock";
        try {
            filename = ReferenceGenerator.generateReference(user);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        reference.setFilename(filename);
        referenceService.add(reference);
        CommunicationRelation communicationRelations = communicationRelationsService.getOne(currentUser, user);
        if (communicationRelations == null) {
            communicationRelationsService.createCommunicationRelations(currentUser, user);
        }
        communicationRelations = communicationRelationsService.getOne(currentUser, user);
        UserMessage usersMessages = new UserMessage();
        usersMessages.setMessage("<a href=\"/references/" + reference.getFilename() + "\">Справка</a>");
        usersMessages.setCommunicationRelation(communicationRelations);
        usersMessages.setUser(currentUser);
        usersMessagesService.addUsersMessages(usersMessages);
        referenceRequest.setActive(false);
        referenceRequestService.add(referenceRequest);
        return "redirect:/deanery/references_requests";
    }

    @RequestMapping(value = "/deanery/references_request/{id}/delete", method = RequestMethod.POST)
    public String deleteRequest(@PathVariable("id") Long id) {
        ReferenceRequest referenceRequest = referenceRequestService.getOne(id);
        referenceRequestService.remove(referenceRequest);
        return "redirect:/deanery/references_requests";
    }
}
