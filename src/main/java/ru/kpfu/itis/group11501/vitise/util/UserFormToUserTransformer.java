package ru.kpfu.itis.group11501.vitise.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.function.Function;


public class UserFormToUserTransformer implements Function<UserForm, User> {

    private static final PasswordEncoder encoder = new BCryptPasswordEncoder();

    private String firstUpperCase(String word) {
        if (word == null || word.isEmpty()) return "";
        return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
    }

    @Override
    public User apply(UserForm userForm) {
        User user = new User();
        user.setName(firstUpperCase(userForm.getName()));
        user.setSurname(firstUpperCase(userForm.getSurname()));
        user.setEmail(userForm.getEmail());
        user.setPassword(encoder.encode(userForm.getPassword()));
        user.setThirdName(firstUpperCase(userForm.getThirdName()));
        user.setTelephoneNumber(userForm.getTelephoneNumber());
        user.setPassMustBeChanged(false);
        user.setActive(null);
        user.setPhotoName("mock");
        user.setToken(TokenGenerator.generate());
        return user;
    }

}
