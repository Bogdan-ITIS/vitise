package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.pojo.StompSession;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Bogdan Popov on 05.05.2017.
 */
public interface StompSessionService {
    void addSession(StompSession stompSession);

    void deleteSession(String sessionId);

    StompSession getSession(String sessionId);

    List<Matcher> getDestinationsMatcherByPattern(String sessionId, Pattern pattern);

    void updateMessagingForSession(String sessionId);

    StompSession createSession(String sessionId, String destination, User user);

    void deleteDestination(String sessionId, String destination);

    void updateMessagingForSession(String sessionId, String destination);

    List<User> getAllUser(String destination);
}
