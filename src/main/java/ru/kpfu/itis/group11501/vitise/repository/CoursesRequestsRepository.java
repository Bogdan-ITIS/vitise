package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.vitise.model.Course;
import ru.kpfu.itis.group11501.vitise.model.CoursesRequests;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.util.List;

/**
 * Created by Наталья on 05.05.2017.
 */
public interface CoursesRequestsRepository extends JpaRepository<CoursesRequests, Long> {
    List<CoursesRequests> findAllByCourseAndIsActive(Course course, Boolean isActive);

    CoursesRequests findOneByCourseAndUser(Course course, User user);

    List<CoursesRequests> findAllByIsActive(Boolean isActive);

    List<CoursesRequests> findAllByIsActiveAndUser(Boolean isActive, User user);
}
