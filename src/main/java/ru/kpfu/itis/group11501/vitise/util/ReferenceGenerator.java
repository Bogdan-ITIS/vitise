package ru.kpfu.itis.group11501.vitise.util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import ru.kpfu.itis.group11501.vitise.model.User;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Марат on 10.05.2017.
 */
public class ReferenceGenerator {
    public static String generateReference(User user) throws IOException, DocumentException {
        FileInputStream fis;
        Properties property = new Properties();
        fis = new FileInputStream("\\vitise\\src\\main\\resources\\path.properties");
        property.load(fis);
        String webappPath = property.getProperty("webapp.path");
        StringBuilder path = new StringBuilder();
        path.append(webappPath).append(File.separator)
                .append("loadFiles").append(File.separator).append("references");
        File file = new File(String.valueOf(path));
        file.mkdirs();
        String filename = user.getFullName().replaceAll(" ", "_") + ".pdf";
        path.append(File.separator).append(filename);
        Document document = new Document(PageSize.A4, 50, 50, 50, 50);
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(String.valueOf(path)));
        document.open();
        document.add(new Paragraph("Справка",
                FontFactory.getFont(webappPath + "/resources/static/font-awesome/fonts/tahoma.ttf",
                        BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED, 20)));
        document.add(new Paragraph(user.getFullName() + " дейтвительно является студентом Высшей школы " +
                "информационных технологий и информационных систем(основная образовательная программа, направление " +
                "\"Прикладная информатика\") ФГАОУ ВО \"Казанский (Приволжский) Федеральный Университет\". ",
                FontFactory.getFont(webappPath + "/resources/static/font-awesome/fonts/tahoma.ttf",
                        BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED)));
        document.add(new Paragraph("Справка выдана для представления по месту требования.",
                FontFactory.getFont(webappPath + "/resources/static/font-awesome/fonts/tahoma.ttf",
                        BaseFont.IDENTITY_H, BaseFont.NOT_EMBEDDED)));
        document.close();
        return filename;
    }
}
