package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.vitise.model.User;
import ru.kpfu.itis.group11501.vitise.model.WorkTopic;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 04.05.2017
 * Group: 11-501
 * Project: vITISe
 */
public interface WorkTopicRepository extends JpaRepository<WorkTopic, Long>, WorkTopicRepositoryCustom {

    List<WorkTopic> findAllByCreatorOrderByName(User creator);

    List<WorkTopic> findAllByBusyOrderByName(Boolean busy);
}
