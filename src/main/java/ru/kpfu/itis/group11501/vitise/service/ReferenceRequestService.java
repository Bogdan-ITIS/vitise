package ru.kpfu.itis.group11501.vitise.service;

import ru.kpfu.itis.group11501.vitise.model.ReferenceRequest;

import java.util.List;

/**
 * Created by Марат on 06.05.2017.
 */
public interface ReferenceRequestService {
    void add(ReferenceRequest referenceRequest);

    void remove(ReferenceRequest referenceRequest);

    ReferenceRequest getOne(Long id);

    List<ReferenceRequest> getAllActive();
}
