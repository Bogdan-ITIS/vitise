package ru.kpfu.itis.group11501.vitise.util;

import java.util.Random;

/**
 * Author: Svintenok Kate
 * Date: 28.04.2017
 * Group: 11-501
 * Project: vITISe
 */
public class PasswordGenerator {
    private static final String LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String DIGITS = "0123456789";
    private static final String ALL_CHARS = LOWER + UPPER + DIGITS;
    private static final int MIN_LENGTH = 6;
    private static final int MAX_LENGTH = 12;

    public static String generate() {

        Random random = new Random(System.nanoTime());
        int length = MIN_LENGTH + random.nextInt(MAX_LENGTH - MIN_LENGTH);
        StringBuilder password = new StringBuilder(length);

        for (int i = 0; i < length; i++) {
            int position = random.nextInt(ALL_CHARS.length());
            password.append(ALL_CHARS.charAt(position));
        }
        return new String(password);
    }
}