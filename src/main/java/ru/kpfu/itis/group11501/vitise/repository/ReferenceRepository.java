package ru.kpfu.itis.group11501.vitise.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.vitise.model.Reference;

/**
 * Created by Марат on 06.05.2017.
 */
public interface ReferenceRepository extends JpaRepository<Reference, Long> {
}
