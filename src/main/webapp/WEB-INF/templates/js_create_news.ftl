<script>
    function auto_grow(element) {
        element.style.height = "auto";
        element.style.height = (element.scrollHeight) + "px";
    }

    $('.post-input').click(function () {
        $('#buttons').show();
    });

    $('#submit-btn').click(function () {
        if ($("#text").val() && $.trim($("#text").val()).length > 0) {
            if ($('#checkbox').prop('checked')) {
                if ($('#date').val() != "" && new Date($("#date").val()) > (new Date())) {
                    $("#post-form").submit();
                    $('#buttons').hide();
                } else {
                    return false;
                }
            }
            else {
                $("#post-form").submit();
                $('#buttons').hide();
            }
        } else {
            return false;
        }
    });

    $(document).ready($('.datetime').css('visibility', 'hidden'));

    $('#checkbox').click(function () {
        display_datetime();
    });

    var display_datetime = function () {
        if (!$('#checkbox').prop('checked')) {
            $('#post-form').attr('action', '/news');
            $('.datetime').css('visibility', 'hidden');
        }
        else {
            $('.datetime').css('visibility', 'visible');
            $('#post-form').attr('action', '/events/create');
        }
    }

    function changeDate() {
        console.log("kek");
        var d = new Date($("#date").val());
        var now = new Date();
        var error = $('#date-error');
        if (d <= now) {
            error.text("Некорректная дата");
            error.css("visibility", "visible");
        } else {
            error.text('\xa0');
            error.css("visibility", "hidden");
        }
    }

</script>