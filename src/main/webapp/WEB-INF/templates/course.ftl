<#include "base2.ftl">
<#macro title>Курс ${course.subject.name}</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/news-profile.css"></#macro>
<#macro content>
    <#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>
<div class="main-block">
    <div class="line1">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

            <div class="col-md-3 col-sm-4">
                <div class="col-md-12 photo-block">
                    <div class="photo-border">
                        <img class="profile-photo" src="/img/bg3.png" alt="">
                    </div>
                </div>
            </div>

            <div class="col-md-9 col-sm-8">
                <div class="col-md-12 info-block">
                    <h2 style="color: #1b6d85;">${course.subject.name}</h2>
                    <p>Информация о курсе: &nbsp; &nbsp;&nbsp;<span
                            class="about-text">${course.description}</span></p>
                    <p>Преподаватели курса: &nbsp; &nbsp;&nbsp;
                    <#list teachers as t>
                        <a href="/user/${t.id}">${t.fullName}</a>
                    </#list></p>
                    <#if is_student>
                        <#if state == 0>
                            <form method="post" action="/courses/enroll">
                                <input value="${course.id}" name="id" hidden>
                                <button type="submit" class="modify-btn">Записаться на курс</button>
                            </form>
                        <#elseif state == 1>
                            <form method="post" action="/courses/${course.id}/decline">
                                <input hidden name="id" value="${request_id}">
                                <button type="submit" class="modify-btn">Отменить заявку</button>
                            </form>
                        <#elseif state == 2>
                            <p>Вы уже проходите этот курс!</p>
                        <#else>
                            <p>Вашу заявку отклонили</p>
                        </#if>
                    </#if>
                </div>
            </div>

        </div>
    </div> <!-- line1 -->

    <div class="line1">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 post">
            <div class="col-md-12">
                <div class="col-md-12 post-block">
                    <#if students?has_content>
                        <h3>Студенты проходящие этот курс (${students_amount})</h3>
                        <#list students as s>
                            <h4><a href="/user/${s.id}">${s.fullName}</a></h4>
                        </#list>
                    <#else>
                        <h3>Нет студентов проходящих курс. Запишись, будь первым!</h3>
                    </#if>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>