<#include "base2.ftl">
<#macro title>Курсовые</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/fist_of_course_works.css" xmlns="http://www.w3.org/1999/html">
</#macro>
<#macro content>
    <#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>

    <div class="main-block">

        <div class="line1">
            <div class="col-md-6 col-md-offset-3">
                <div class="col-sm-12">
                    <form>
                        <div class="col-md-12 post-block smth">
                        <form method="get" action="/work_topics/search" id="search-form">

                            <div class="search" id="search-input">
                                <button type="submit" class="search-button"><i class="fa fa-search fa-lg s-icon" aria-hidden="true"></i></button>
                                <input type="text" name="name" class="search-input" placeholder="Поиск" <#if topicName??> value="${topicName}" </#if>>
                            </div>
                            <select name="professor" class="select-directions">
                                <#if topicProfessor??>
                                    <option value="0" <#if topicProfessor=0>selected</#if>>Все</option>
                                    <#list professors as professor>
                                        <option value="${professor.id}"
                                                <#if professor.id=topicProfessor>selected</#if>>${professor.fullName}</option>
                                    </#list>
                                <#else>
                                    <option value="0" selected>Все</option>
                                    <#list professors as professor>
                                        <option value="${professor.id}">${professor.fullName}</option>
                                    </#list>
                                </#if>
                            </select>
                            <select name="direction" class="select-directions">
                                <#if topicDirection??>
                                    <option value="0" <#if topicDirection=0>selected</#if>>Все</option>
                                    <#list directions as direction>
                                        <option value="${direction.id}"
                                                <#if direction.id=topicDirection>selected</#if>>${direction.name}</option>
                                    </#list>
                                <#else>
                                    <option value="0" selected>Все</option>
                                    <#list directions as direction>
                                        <option value="${direction.id}">${direction.name}</option>
                                    </#list>
                                </#if>
                            </select>

                        </form>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="line1">
            <div class="col-md-6 col-md-offset-3">
                <div class="col-sm-12 ">
                    <div class="col-xs-12 post-block">
                    <#list workTopics as topic>
                        <div class="theme">
                            <h3 class="theme-name">${topic.name}</h3>
                            <p class="theme-direction">Направление:  ${topic.direction.name}</p>
                            <p class="theme-direction">Руководитель: <a href="/user/${topic.creator.id}"> ${topic.creator.fullName}</a></p>
                            <#if topic.activeRequests?size != 0>
                            <p class="theme-direction">Активных заявок: ${topic.activeRequests?size}</p>
                            </#if>

                            <#if currentUser.statusName = "STUDENT">
                                <input type="hidden" name="topic_id" value="${topic.id}">
                                <#if topic.currentUserRequest??>
                                    <#if topic.currentUserRequest.confirmed??>
                                        <#if !isStudentHaveTopic>
                                            <p>Ваша заявка отклонена :(</p>
                                        </#if>
                                    <#else>
                                        <form method="post" action="/work_topics/delete_choose">
                                            <input type="hidden" name="request_id" value="${topic.currentUserRequest.id}">
                                            <button class="choose-btn" type="submit">Отменить заявку</button>
                                        </form>
                                    </#if>
                                <#elseif !isStudentHaveTopic && userRequestsCount<3>
                                    <form method="post" action="/work_topics/choose">
                                        <input type="hidden" name="topic_id" value="${topic.id}">
                                        <button  class="choose-btn" type="submit">Выбрать</button>
                                    </form>
                                </#if>
                            </#if>
                        </div>
                    </#list>
                    </div>
                </div>
            </div>
        </div>

    </div>

</#macro>

<#--
<div class="main-block">
    <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

        <h3>Cвободные темы:</h3>

        <form method="get" action="/work_topics/search">
            <input type="text" name="name" placeholder="Введите название" <#if topicName??> value="${topicName}" </#if>>

            <select name="direction">
                <#if topicDirection??>
                    <option value="0" <#if topicDirection=0>selected</#if>>Все</option>
                    <#list directions as direction>
                        <option value="${direction.id}"
                                <#if direction.id=topicDirection>selected</#if>>${direction.name}</option>
                    </#list>
                <#else>
                    <option value="0" selected>Все</option>
                    <#list directions as direction>
                        <option value="${direction.id}">${direction.name}</option>
                    </#list>
                </#if>
            </select>
            <select name="professor">
                <#if topicProfessor??>
                    <option value="0" <#if topicProfessor=0>selected</#if>>Все</option>
                    <#list professors as professor>
                        <option value="${professor.id}"
                                <#if professor.id=topicProfessor>selected</#if>>${professor.fullName}</option>
                    </#list>
                <#else>
                    <option value="0" selected>Все</option>
                    <#list professors as professor>
                        <option value="${professor.id}">${professor.fullName}</option>
                    </#list>
                </#if>
            </select>
            <input type="submit" value="Найти">
        </form>

        <hr>
        <#list workTopics as topic>
            <h4> ${topic.name}</h4>
            <p>Направление: ${topic.direction.name}</p>
            <p>Руководитель: <a href="/user/${topic.creator.id}"> ${topic.creator.fullName}</a></p>
            <p>Активных заявок: ${topic.activeRequests?size}</p>

            <#if currentUser.statusName = "STUDENT">
                <input type="hidden" name="topic_id" value="${topic.id}">
                <#if topic.currentUserRequest??>
                    <#if topic.currentUserRequest.confirmed??>
                        <#if !isStudentHaveTopic>
                            <p>Ваша заявка отклонена :(</p>
                        </#if>
                    <#else>
                        <form method="post" action="/work_topics/delete_choose">
                            <input type="hidden" name="request_id" value="${topic.currentUserRequest.id}">
                            <input type="submit" value="Отменить заявку">
                        </form>
                    </#if>
                <#elseif !isStudentHaveTopic && userRequestsCount<3>
                    <form method="post" action="/work_topics/choose">
                        <input type="hidden" name="topic_id" value="${topic.id}">
                        <input type="submit" value="Выбрать">
                    </form>
                </#if>
            </#if>
            <hr>
        </#list>
    </div>
</div>
-->