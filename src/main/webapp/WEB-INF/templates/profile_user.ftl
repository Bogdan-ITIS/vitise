<#include "base2.ftl">
<#macro title>${userPage.fullName}</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/other_user.css"></#macro>
<#macro content>
    <#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>


<div class="main-block">
    <div class="line1">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">


            <div class="col-md-3 col-sm-4">
                <div class="col-md-12 photo-block">

                    <div class="photo-border">
                        <img class="profile-photo" src="/img/bg3.png" alt="">
                    </div>
                    <form action="/messages/dialog/create/${userPage.id}" method="post">
                        <input class="modify-btn" type="submit" value="Написать сообщение"/>
                    </form>
                    <#if !is_admin>
                        <#if state == 0>
                            <form action="/colleagues/${userPage.id}/add" method="post">
                                <input class="delete-colleague" type="submit" value="Добавить в коллеги">
                            </form>
                        <#elseif state == -2>
                            <form action="/colleagues/${colleagues.id}/approve" method="post">
                                <input class="delete-colleague" type="submit" value="Подтвердить">
                            </form>
                        <#else>
                            <form action="/colleagues/${colleagues.id}/delete" method="post">
                                <input class="delete-colleague" type="submit"
                                       value="<#if state == 1>Удалить коллегу<#else>Отменить</#if>">
                            </form>

                        </#if>
                    </#if>
                </div>
            </div>

            <div class="col-md-9 col-sm-8">
                <div class="col-md-12 info-block">
                    <h2 style="color: #1b6d85;">${userPage.fullName}</h2>
                    <#if group??><p>Группа: &nbsp; <span class="about-text">${group.name}</span></p></#if>
                    <p>Телефон:<span class="about-text">${userPage.telephoneNumber}</span></p>
                    <p>E-mail&nbsp;&nbsp; &nbsp; &nbsp;<span class="about-text">${userPage.email}</span></p>
                    <#if lab??><p>Лаборатория:  &nbsp; <a class="about-text" href="/laboratories/${lab.id}">${lab.name}</a></p></#if>
                    <#if userPage.description?? && userPage.description != ""><p>О себе:&nbsp; &nbsp;&nbsp;<span
                            class="about-text">
                    ${userPage.description}
                    </span></p></#if>
                </div>
            </div>


        </div>
    </div> <!-- line1 -->


    <div class="line1">
        <#list newses as n>
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 post">
                <div class="col-md-12">
                    <div class="col-md-12 post-block">
                        <div class="round-photo">
                            <img src="/img/camera.png" alt="">
                        </div>
                        <div class="post-info">
                            <h3 class="post-sign">${n.getAuthor().fullName}
                            </h3> <span>${n.getPubDate()}</span>
                            <#if group??><p class="post-sign">Студент группы  ${group.name}</p></#if>
                        </div>
                        <div class="post-content">
                            <p>${n.getText()}</p>
                        </div>
                        <#if n.isEvent()>
                            <h4>Дата события: ${n.eventDate?date}</h4>
                            <p>Количество участников: ${n.subscriptionsCount}</p>
                            <form action="/events/subscribe" method="post">
                                <input name="event_id" type="hidden" value="${n.getId()}"/>
                                <#if n.subscribeStatus>
                                    <div id="news-buttons" style="width: 100%;">
                                        <input type="submit" value="Отписаться" id="news-submit-btn">
                                    </div>
                                <#else>
                                    <div id="news-buttons" style="width: 100%;">
                                        <input type="submit" value="Записаться" id="news-submit-btn">
                                    </div>
                                </#if>
                            </form>
                            <#if currentUser.id = n.getAuthor().id>
                                <form action="/events/remove" method="post">
                                    <input name="event_id" type="hidden" style="display: none" value="${n.id}"/>
                                    <button type="submit" class="delete-btn" title="Удалить">
                                        <i class="fa fa-2x fa-times" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </#if>
                        <#else>
                            <#if currentUser.id = n.getAuthor().id>
                                <form action="/news/remove" method="post">
                                    <input name="news_id" type="hidden" style="display: none" value="${n.id}"/>
                                    <button type="submit" class="delete-btn" title="Удалить">
                                        <i class="fa fa-2x fa-times" aria-hidden="true"></i>
                                    </button>
                                </form>
                            </#if>
                        </#if>
                    </div>
                </div>
            </div>
        </#list>
    </div>
</div>
</#macro>