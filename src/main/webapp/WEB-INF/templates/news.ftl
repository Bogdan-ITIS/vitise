<#include "base2.ftl">
<#macro title>Новости</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/news-profile.css">
<script src="/js/dateformat.js"></script>
</#macro>
<#macro content>
    <#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>
<div class="main-block">
    <div class="line1">
        <div class="col-xs-12 col-sm-9 col-md-7 col-md-offset-1 col-lg-8 col-lg-offset-1 post-news">
            <div class="col-md-12">
                <div class="col-md-12 post-block">
                    <form role="form" action="/news" id="post-form" method="post" enctype="multipart/form-data">
                        <div class="round-photo">
                            <img src="/img/bg3.png" alt="">
                        </div>
                        <textarea id="text" name="text" cols="1" class="post-input"
                                  placeholder="Здесь вы можете написать свою новость"
                                  oninput="auto_grow(this)" required></textarea>
                        <div id="buttons" style="width: 100%;">
                            <div class="" style="width: 100%;">
                                <input class="checkbox" type="checkbox" name="" id="checkbox" hidden>
                                <label for="checkbox" class="check-label"></label>
                                <span class="remember-sign">Мероприятие</span>
                            </div>

                            <div class="datetime">
                                <input type="date" name="eventDate" id="date" required>
                            </div>

                            <div class="bottom-buttons">
                                <button type="submit" id="submit-btn" class="submit-btn">Опубликовать</button>
                                <label for="add-photo-btn" class="add-photo-label"><img src="/img/camera.png"
                                                                                        alt=""></label>
                                <input type="file" name="file" id="add-photo-btn">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="line0">
        <div class="col-xs-12 col-sm-3 col-sm-push-9 col-md-3 col-md-push-8 col-lg-2 col-lg-push-9">
            <div class="col-xs-12">
                <div class="col-md-12 news-nav">
                    <h4 id="common-filter">Новости <span hidden>common</span></h4>
                    <h4 id="events-filter">Мероприятия <span hidden>events</span></h4>
                    <h4 id="all-filter">Все <span hidden>all</span></h4>
                </div>
            </div>
        </div>


        <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-7 col-md-offset-1 col-md-pull-3 col-lg-8 col-lg-offset-1 col-lg-pull-2 post-news">
            <div class="col-md-12 posts-container"></div>
        </div>
    </div>
</div>
<script>

    function getNews(value) {
        $.ajax({
            'url': '/get_news',
            'data': {
                'filter': value
            },
            'type': 'get',
            'success': function (json) {
                for (var i = 0, len = json.length; i < len; i++) {
                    build_post_block(json[i]);
                }
            }
        })
    };

    var build_post_block = function (a) {
        var post_block = $('<div></div>')
                .addClass('col-md-12')
                .addClass('post-block');

        var round_photo = $('<div></div>')
                .addClass('round-photo');


        var img = $('<img>');
        img.attr('src', '/img/camera.png'); //  paste user photo link here
        img.appendTo(round_photo);

        var post_info = $('<div></div>')
                .addClass('post-info');

        var sign = '';
        if (a['author']['statusName'] == 'TEACHER') {
            sign = 'Преподаватель';
        }

        else if (a['author']['statusName'] == 'STUDENT') {
            sign = 'Студент группы ' + a['author']['group']['name'];
        }

        var post_sign_p = $('<p></p>')
                .addClass('post-sign')
                .text(sign);

        var author_link = $("<a/>").attr('href', '/user/' + a['author']['id']);

        var post_sign_h3 = $('<h3></h3>')
                .addClass('post-sign')
                .text(a['author']['name'] + ' ' + a['author']['surname']);

        author_link.append(post_sign_h3);

        if (a['date'] != null) {
            var pub_date_span = $('<span></span>')
                    .text(dateFormat(new Date(a['date']), "dd.mm.yy в HH:MM"));
        }

        if (a['pubDate'] != null) {
            var pub_date_span = $('<span></span>')
                    .text(dateFormat(new Date(a['pubDate']), "dd.mm.yy в HH:MM"));
        }

        var post_content = $('<div></div>')
                .addClass('post-content');

        var post_text = $('<p></p>')
                .text(a['text']);


        post_text.appendTo(post_content);

        author_link.appendTo(post_info);
        pub_date_span.appendTo(post_info);
        post_sign_p.appendTo(post_info);

        round_photo.appendTo(post_block);
        post_info.appendTo(post_block);
        post_content.appendTo(post_block);

        var event_date = "";
        if (a['eventDate'] != null) {
//            event_date = dateFormat(new Date(a['eventDate']), "dd.mm.yy в HH:MM");
            event_date = dateFormat(new Date(a['eventDate']), "dd.mm.yy");

            var event_date_h4 = $('<h4></h4>')
                    .text('Дата события: ' + event_date);

            var subs_count_p = $('<p></p>')
                    .text('Количество участников: ' + a['subscriptionsCount']);

            var button_div = $('<div></div>')
                    .addClass('news-button');

            var hidden_input = $('<input name="event_id">')
                    .attr('type', 'hidden')
                    .val(a['id']);

            var subscribe_form = $('<form></form>')
                    .attr('action', '/events/subscribe')
                    .attr('method', 'post')
                    .append(hidden_input);
            if (a['subscribeStatus']) {
                button_div
                        .append('<input type="submit" value="Отписаться" id="news-submit-btn">');
            }
            else {
                button_div
                        .append('<input type="submit" value="Записаться" id="news-submit-btn">');
            }

            button_div.appendTo(subscribe_form);


            event_date_h4.appendTo(post_block);
            subs_count_p.appendTo(post_block);
            subscribe_form.appendTo(post_block);
        }

        post_block.appendTo($('.posts-container'));
    };

    $('#all-filter').click(function () {
        $('#all-filter').css('border', '2px solid darkgray');
        $('#events-filter').css('border', 'none');
        $('#common-filter').css('border', 'none');
        $('.posts-container').empty();
        getNews($('#all-filter > span').text());
    });

    $('#events-filter').click(function () {
        $('#events-filter').css('border', '2px solid darkgray');
        $('#all-filter').css('border', 'none');
        $('#common-filter').css('border', 'none');
        $('.posts-container').empty();
        getNews($('#events-filter > span').text());
    });

    $('#common-filter').click(function () {
        $('#common-filter').css('border', '2px solid darkgray');
        $('#all-filter').css('border', 'none');
        $('#events-filter').css('border', 'none');
        $('.posts-container').empty();
        getNews($('#common-filter > span').text());
    });

    $(document).ready($('#all-filter').click());
</script>
    <#include "js_create_news.ftl">
</#macro>