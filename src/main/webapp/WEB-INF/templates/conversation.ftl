<#--
<#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>
<title>беседа</title>
<p><a href="/profile">вернуться на свою страницу</a></p>
<p><a href="/messages/all">вернуться к списку диалогов</a></p>

<h3>${conversation.name}</h3>
<hr/>

<#if conversation.currentMember.activeStatusName.name() != "DELETED">

<form action='/messages/conversation/${conversation.id}/change_status' method="post">
    <input type="hidden" name="user_id" value="${currentUser.id}">
    <#if conversation.currentMember.activeStatusName.name() == "ACTIVE">
        <input class="btn btn-info btn-outline" type="submit" value="Выйти из беседы">
    <#else>
        <input class="btn btn-info btn-outline" type="submit" value="Вернуться в беседу">
    </#if>
</form>
</#if>
<br>
<h3>Участники</h3>
<ul>
<#list conversation.members as member>
    <li><a href="/user/${member.id}"> ${member.name} ${member.surname}</a>
        <#if currentUser.id == conversation.creator.id && member.id != currentUser.id>
            <form action="/messages/conversation/${conversation.id}/delete" method="post">
                <input type="hidden" name="user_id" value="${member.id}">
                <input class="btn btn-info btn-outline" type="submit" value="Удалить">
            </form>
        </#if>
    </li>
</#list>
</ul>
<hr/>

<#if currentUser.id == conversation.creator.id>
<form action="/messages/conversation/${conversation.id}/add" method="post">
    <p>Добавить участника:</p>
    <p><select name="user_id">
        <#list available_users as u>
            <option value="${u.id}">${u.fullName}</option>
        </#list>
    </select></p>
    <input class=" btn btn-info btn-outline" type="submit" value="Добавить">
</form>
<hr/>
</#if>

<#if currentUser.id == conversation.creator.id>
<form action="/messages/conversation/${conversation.id}/change" method="post">
    <p>Изменить название:</p>
    <input type="text" name="name" value="${conversation.name}">
    <input class="btn btn-info btn-outline" type="submit" value="Изменить название">
</form>
<hr/>
</#if>

<#if conversation.currentMember.activeStatusName.name() == "ACTIVE">
    <@sf.form id="form" action='/messages/conversation/${conversation.id}/send' method="post" modelAttribute="message_form"
    enctype="multipart/form-data">
    <fieldset>
        <div class="field">
            <@sf.label path="text">Введите сообщение: </@sf.label>
            <@sf.input id="message" path="text" cssClass="form-control"/>
        </div>
        <input type="file" name="file" id="attach-btn">
        <div class="form-group">
            <button type="submit" class="btn btn-info btn-outline" id="submit-btn">Отправить</button>
        </div>
    </fieldset>
    </@sf.form>
<#elseif conversation.currentMember.activeStatusName.name() == "LEFT_OUT">
<p style="color: red">Вы покинули беседу</p>
<#else>
<p style="color: red">Вы удалены из беседы</p>
</#if>


<#list conversation.messages as message>
<div>
    <p <#if conversation.currentMember.messagesReadingLog.date < message.date && message.user.id != currentUser.id>
            style="color: red"
    </#if>><b>${message.user.name} ${message.user.surname} :</b> ${message.text} <i>${message.date}</i></p>
</div>
</#list>
<script type="text/javascript">
    $('#submit-btn').click(function () {
        console.log("kek");
        if ($("#message").val() && $.trim($("#message").val()).length > 0) {
            $("#form").submit();
        } else {
            return false;
        }
    })
</script>
-->
<#include "base2.ftl">
<#macro title>Беседа</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/conversation.css">
</#macro>
<#macro content>

    <#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>

<div class="main-block">
    <div class="line2">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <div class="col-xs-12">
                <div class="col-xs-12 input-wrapper">
                    <a href="/messages/all"><i class="fa fa-lg fa-chevron-left" aria-hidden="true"></i> &nbsp;
                        &nbsp;</a>
                    <h4 id="title">${conversation.name}</h4>

                    <#if currentUser.id == conversation.creator.id>
                        <input type="text" name="name" class="conv-name-input" value="${conversation.name}"
                               id="conv-name-input" oninput="copy()" hidden>
                    </#if>

                    <input type="text" placeholder="Начните вводить имя или фамилию" class="search-input1"
                           id="message-search-field">

                    <a href="#" id="search-message-btn"><i class="fa fa-lg fa-search" aria-hidden="true"></i></a>
                    <a href="#" id="cancel-search-btn" hidden><i class="fa fa-lg fa-times" aria-hidden="true"></i></a>
                    <span class="more-btn-wrapper" id="more-btn"><i class="fa fa-lg fa-ellipsis-v more-btn"
                                                                    aria-hidden="true"></i></span>
                </div>

            </div>
        </div>
        <div class="more-menu">
            <#if currentUser.id == conversation.creator.id>
                <p id="add-members">Добавить участников</p>
                <p id="manage-members">Управление беседой</p>
            <#else>
                <p id="manage-members">Участники</p>
            </#if>

            <form action='/messages/conversation/${conversation.id}/change_status' method="post">
                <input type="hidden" name="user_id" value="${currentUser.id}">
                <#if conversation.currentMember.activeStatusName.name() == "ACTIVE">
                    <p onclick='this.parentNode.submit()'>Выйти из беседы</p>
                <#else>
                    <p onclick='this.parentNode.submit()'>Вернуться в беседу</p>
                </#if>
            </form>
        </div>
    </div>


    <div class="line2">
        <!--messages-->
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3"
             id="messages-block">
            <div class="col-xs-12">
                <div class="col-xs-12 messages-items-block" id="messages">

                    <#list conversation.messages as message>
                        <div class="message-item"
                            <#if conversation.currentMember.messagesReadingLog.date < message.date && message.user.id != currentUser.id>
                             style="background-color: #f2faff;"
                            </#if>>
                            <div class="round-photo">
                                <img src="/img/mal.png" alt="">
                            </div>
                            <div class="message-text">
                                <h4>
                                    <a href="/user/${message.user.id}">${message.user.surname} ${message.user.name}</a>
                                </h4> <span>${message.date}</span>
                                <h5>${message.text}</h5>
                            </div>

                        </div>
                    </#list>
                </div>
            </div>

            <div class="col-xs-12">
                <div class="col-xs-12 send-buttons">
                            <textarea name="text" id="message" cols="1" class="post-input"
                                <#if conversation.currentMember.activeStatusName.name() == "ACTIVE">
                                      placeholder="Введите текст сообщения"
                                      oninput="auto_grow(this)"
                                <#elseif conversation.currentMember.activeStatusName.name() == "LEFT_OUT">
                                      placeholder="Вы покинули беседу" disabled
                                <#else>
                                      placeholder="Вы удалены из беседы" disabled
                                </#if>
                            ></textarea>
                    <button type="submit" id="custombtn">
                        <i class="fa fa-2x fa-paper-plane" aria-hidden="true"></i>
                    </button>
                    <form action='/messages/conversation/${conversation.id}/upload' method="post" id="form"
                          enctype="multipart/form-data">
                    <input type="file" onchange="file_exists()" name="file" id="attach-btn">
                    <label for="attach-btn"><i class="fa fa-2x fa-paperclip" aria-hidden="true"></i></label>
                        </form>
                </div>
            </div>
        </div>
        <!--end messages-->


        <!--add member-->
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3"
             id="friends-block" hidden>
            <div class="col-xs-12">
                <form action="/messages/conversation/${conversation.id}/add" method="post">
                    <div class="col-xs-12 messages-block friends-block">
                        <#list available_users as u>
                            <div class="message friend can_delete">
                                <div class="round-photo">
                                    <img src="/img/mal.png" alt="">
                                </div>
                                <div class="message-info">
                                    <h4>${u.fullName}</h4>
                                    <input type="checkbox" name="users_id" class="f-check" id="f-check-123"
                                           value="${u.id}">
                                    <label for="f-check-123"></label>
                                </div>
                            </div>
                        </#list>
                    </div>
                    <input type="submit" value="Сохранить" class="create-btn">
                    <input type="reset" value="Отменить" class="create-btn moved" id="cancel-adding">
                </form>
            </div>
        </div>
        <!--end add member-->

        <!--manage members-->
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3"
             id="members-block" hidden>
            <div class="col-xs-12">
                <form action="/messages/conversation/${conversation.id}/change" method="post">
                    <input type="text" name="name" class="conv-name-input" value="${conversation.name}"
                           id="conv-name-hidden"
                           hidden>
                    <div class="col-xs-12 messages-block friends-block">
                        <#list conversation.members as member>
                            <div class="message friend <#if member.id != currentUser.id>can_delete</#if>">
                                <div class="round-photo">
                                    <img src="/img/mal.png" alt="">
                                </div>
                                <div class="message-info">
                                    <h4>${member.fullName}</h4>

                                    <#if currentUser.id == conversation.creator.id>
                                        <input type="checkbox" name="users_id" class="f-check" id="f-check-124"
                                               value="${member.id}" checked>
                                        <label for="f-check-123"></label>
                                    </#if>
                                </div>
                            </div>
                        </#list>

                    </div>

                    <#if currentUser.id == conversation.creator.id>
                        <input type="submit" value="Сохранить" class="create-btn">
                        <input type="reset" value="Отменить" class="create-btn moved" id="cancel-managing" checked>
                    <#else>
                        <input type="reset" value="Назад" class="create-btn moved" id="cancel-managing" checked>
                    </#if>
                </form>
            </div>
        </div>
        <!--end manage members-->
    </div>
    <#--end line2-->
</div>


<script>
    var clicked = false;
    var fileExists = false;

    function file_exists() {
        fileExists = true;
    }

    $("#more-btn").click(function () {
        $(".more-menu").css('visibility', 'visible');
        clicked = true;
    });

    $("#more-btn").hover(function () {
        $(".more-menu").css('visibility', 'visible');
    }, function () {
        if (!clicked) {
            $(".more-menu").css('visibility', 'hidden');
        }
    });

    $(".more-menu").hover(function () {
        $(this).css('visibility', 'visible');
    }, function () {
        $(this).css('visibility', 'hidden');
        clicked = false;
    });

    $('.friend.can_delete').click(function () {
        $(this).find('input[type=checkbox]').prop("checked", !$(this).find('input[type=checkbox]').prop("checked"));
    });

    function auto_grow(element) {
        element.style.height = "auto";
        element.style.height = (element.scrollHeight) + "px";
    }

    $('.post-input').keypress(function () {
        $('#buttons').show();
    });

    $('#submit-btn').click(function () {
        $('#buttons').hide();
        $('.post-input').val('');
    });


    $(function () {
        $('.messages-items-block').scrollTop(1E10);
    });

    $('#search-message-btn').click(function () {
        $(this).hide();
        $('#title').hide();
        $('#conv-name-input').hide();
        $('#message-search-field').show('fast');
        $('#cancel-search-btn').show();
    });

    $('#cancel-search-btn').click(function () {
        $(this).hide();
        $('#title').show('fast');
        $('#message-search-field').hide();
        $('#search-message-btn').show();
    });

    $('#add-members').click(function () {
        $('#messages-block').hide();
        $('#members-block').hide();
        $('#friends-block').show('slow');
        $('#conv-name-input').hide();
        $('#title').show();
    });

    $('#cancel-adding').click(function () {
        $('#messages-block').show('slow');
        $('#friends-block').hide();
        $('#members-block').hide();
    });

    $('#manage-members').click(function () {
        $('#messages-block').hide();
        $('#friends-block').hide();
        $('#members-block').show('slow');

        <#if currentUser.id == conversation.creator.id>
            $('#title').hide();
            $('#conv-name-input').show();
        </#if>

    });

    $('#cancel-managing').click(function () {
        $('#messages-block').show('slow');
        $('#friends-block').hide();
        $('#members-block').hide();
        $('#conv-name-input').hide();
        $('#title').show();
    });

    function copy() {
        var val = $('#conv-name-input').val();
        $('#conv-name-hidden').val(val);
    }

    var ws;
    var stompClient;

    $(document).ready(function () {
        $('#custombtn').click(function () {
            sendForm();
            $('#message').val('');
        });

        ws = new SockJS('/messaging');

        stompClient = Stomp.over(ws);

        stompClient.connect({}, function (frame) {

            stompClient.subscribe("/topic/conversation/${conversation.id}", function (message) {
                var m = JSON.parse(message.body);
                $('#messages').append('<div class="message-item"><div class="round-photo"> <img src="/img/mal.png" alt=""> </div> <div class="message-text"> <h4><a href="/user/' + m.sender.id + '">' + m.sender.surname + ' ' + m.sender.name + '</a> </h4> <span>' + m.date + '</span> <h5>' + m.content + '</h5> </div></div>');
                $('.messages-items-block').scrollTop(1E10);
            });
        }, function (error) {
            console.log("STOMP protocol error " + error);
        })

    });


    function sendForm() {
        stompClient.send("/app/conversation/send/${conversation.id}", {}, JSON.stringify({'content': $('#message').val()}));
        if (fileExists) {
            $('#form').submit();
        }
    }

</script>
</#macro>