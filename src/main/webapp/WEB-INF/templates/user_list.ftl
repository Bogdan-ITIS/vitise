<#include "base2.ftl">
<#macro title>Пользователи</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/news-profile.css">
</#macro>
<#macro content>
<div class="main-block">
    <div class="line1">
        <div class="col-md-6 col-md-offset-3 post-block">
            <form action="/users/search">
                <input name="name" class="search-input1" type="text" placeholder="Введите имя или фамилию"/>
                <input type="submit" value="Найти" class="search-button1">
            </form>
        </div>
    </div>

    <div class="line1">
        <div class="col-md-6 col-md-offset-3 post-block">
            <#list users as user>
                <h3><a href="/user/${user.getId()}">${user.getFullName()}</a></h3>
            <#else >
                Empty list
            </#list>
        </div>
    </div>
</div>
</#macro>