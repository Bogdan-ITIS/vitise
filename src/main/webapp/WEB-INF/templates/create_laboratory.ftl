<#include "base2.ftl">
<#macro title>Создание лаборатории</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/login.css">
<link rel="stylesheet" href="/css/labs.css">
</#macro>
<#macro content>
<div class="main-block">

    <div class="col-lg-6 col-md-8 col-lg-offset-3 col-md-offset-2 col-sm-8 col-sm-offset-2 login-block no-mh">

        <h3 class="title">Создание лаборатории</h3>
        <form action="/deanery/laboratories/create" method="post">
            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <p class="input-label">Название</p>
                <div class="input-wrapper2" style="margin-top: 30px">
                    <input type="text" class="login-input" name="name" placeholder="Введите название">
                </div>
                <p>&nbsp; </p>
                <div class="selects">
                    <p class="input-label">Научное направление</p>
                    <div class="input-wrapper">
                        <select class="login-input" name="direction">
                            <#list directions as d>
                                <option value="${d.id}">${d.name}</option>
                            </#list>
                        </select>
                    </div>
                </div>
                <p>&nbsp; </p>
                <div class="selects">
                    <p class="input-label">Научный руководитель</p>
                    <div class="input-wrapper">
                        <select class="login-input" name="teacher">
                            <#list teachers as t>
                                <option value="${t.id}">${t.fullName}</option>
                            </#list>
                        </select>
                    </div>
                </div>

            </div>

            <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                <p class="input-label">Описание</p>
                <textarea name="description" placeholder="Напишите о лаборатории дополнительную информацию"
                          class="input-wrapper1"></textarea>
            </div>

            <div class="row">
                <div class="col-md-6 col-lg-6 col-xs-12 col-sm-12">
                    <div class="col-xs-12">
                    <#--<div class="col-md-8 col-lg-7 col-xs-12 col-sm-12 col-md-offset-4 col-lg-offset-5">-->
                        <h3 class="links cancel"><a href="/laboratories">Отмена</a></h3>
                        <input type="submit" value="Создать" class="login-btn">
                    </div>
                <#--</div>-->
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $("#signup-btn").click(function () {
        $("#modal-form").show();
    })

    var mouse_is_inside = false;
    $(document).ready(function () {
        $('#modal-form').hover(function () {
            mouse_is_inside = true;
        }, function () {
            mouse_is_inside = false;
        });

        $("body").mouseup(function () {
            if (!mouse_is_inside) $('#modal-form').hide();
        });
    });
</script>
</#macro>
