<#--<#list requests as req>-->
<#--<h4>Заявка от студента <a href="/user/${req.user.id}">${req.user.fullName}</a></h4>-->
<#--<form method="post" action="/deanery/references_requests/${req.id}/approve">-->
    <#--<input type="submit" value="Подтвердить заявку">-->
<#--</form>-->
<#--<form method="post" action="/deanery/references_request/${req.id}/delete">-->
    <#--<input type="submit" value="Отклонить заявку">-->
<#--</form>-->
<#--<#else>-->
<#--<h2>Заявок на получение справок нет</h2>-->
<#--</#list>-->

<#include "base2.ftl">
<#macro title>Заявки на получение справок</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/dialogs.css">
<link rel="stylesheet" href="/css/requests.css">
</#macro>
<#macro content>
<div class="main-block">
    <div class="line2">
        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2" id="messages-block">
            <div class="col-xs-12">
                <div class="col-xs-12 messages-items-block1">
                    <#list requests as req>
                        <div class="message-item">
                            <div class="round-photo">
                                <img src="/img/mal.png" alt="">
                            </div>
                            <div class="message-text">
                                <h4><a href="/user/${req.user.id}">${req.user.fullName}</a></h4>
                                <form method="post" action="/deanery/references_request/${req.id}/delete">
                                    <input type="submit" value="Отклонить"
                                           class="news-submit-btn">
                                </form>
                                <form method="post" action="/deanery/references_requests/${req.id}/approve">
                                    <input type="submit" value="Одобрить"
                                           class="news-submit-btn">
                                </form>
                            </div>
                        </div>
                    </#list>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.friend').click(function () {
        $(this).find('input[type=checkbox]').prop("checked", !$(this).find('input[type=checkbox]').prop("checked"));
    });

    function auto_grow(element) {
        element.style.height = "auto";
        element.style.height = (element.scrollHeight) + "px";
    }

    $('.post-input').keypress(function () {
        $('#buttons').show();
    });

    $('#submit-btn').click(function () {
        $('#buttons').hide();
        $('.post-input').val('');
    });


    $(function () {
        $('.messages-items-block').scrollTop(1E10);
    });

    $('#search-message-btn').click(function () {
        $(this).hide();
        $('#title').hide();
        $('#message-search-field').show('fast');
        $('#cancel-search-btn').show();
    });

    $('#cancel-search-btn').click(function () {
        $(this).hide();
        $('#title').show('fast');
        $('#message-search-field').hide();
        $('#search-message-btn').show();
    });


</script>

</#macro>

