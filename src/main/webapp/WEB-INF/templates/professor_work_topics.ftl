<#include "base2.ftl">
<#macro title>Курсовые</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
    <link rel="stylesheet" href="/css/fist_of_course_works.css">
</#macro>
<#macro content>
    <#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>

<div class="main-block">

    <div class="line1">
        <div class="col-md-6 col-md-offset-3">
            <div class="col-sm-12 ">
                <div class="buttons">
                    <button class="left-btn" id="themes-btn">Темы</button>
                    <button class="right-btn" id="requests-btn">Заявки</button>
                </div>
                <div class="col-xs-12 post-block">

                    <div class="themes" id="themes">

                        <form method="post" action="/work_topics/create">


                        <div class="theme">
                            <form action="">
                                <input name="name" type="text" placeholder="Введите название темы" class="theme-input">
                                <button type="submit" class="add-btn">Добавить</button>
                                <br><br>
                                <label>Научное направление:    </label>
                                <select name="direction" style="margin-left: 20px">
                                    <#list directions as direction>
                                        <option value="${direction.id}">${direction.name}</option>
                                    </#list>
                                </select>
                            </form>
                            <br><br>
                        </div>

                        </form>

                        <#list work_topics as topic>

                        <div class="theme">
                            <h3 class="theme-name">${topic.name}</h3>
                            <p class="theme-direction">${topic.direction.name}</p>
                            <#if topic.busy>
                                <p class="theme-direction">Выполняет: <a href="/user/${topic.student.id}">${topic.student.fullName}</a></p>
                            <#elseif topic.activeRequests?size != 0>
                                <p class="theme-direction"> Активных заявок: ${topic.activeRequests?size}</p>
                            </#if>
                        </div>
                        </#list>

                    </div>

                    <div class="requests" id="requests"  hidden>

                        <#list activeRequests as request>

                        <div class="theme request"  id="themes">
                            <h3 class="theme-name">Тема: ${request.topic.name}</h3>
                            <p class="theme-name"><a href="/user/${request.student.id}">${request.student.fullName}</a> <i>${request.date}</i></p>

                            <form method="post" action="/work_topics/request/${request.id}/approve">
                                <button class="choose-btn" type="submit">Одобрить</button>
                            </form>
                            <form method="post" action="/work_topics/request/${request.id}/reject">
                                <button class="choose-btn" type="submit">Отклонить</button>
                            </form>
                        </div>
                        </#list>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $('#requests-btn').click(function () {
        $('#requests').show();
        $('#themes').hide();
        $('#requests-btn').css('background-color', 'white');
        $('#requests-btn').css('color', '#1b7181');
        $('#themes-btn').css('background-color', '#1b7181');
        $('#themes-btn').css('color', 'white');
    });

    $('#themes-btn').click(function () {
        $('#themes').show();
        $('#requests').hide();
        $('#themes-btn').css('background-color', 'white');
        $('#themes-btn').css('color', '#1b7181');
        $('#requests-btn').css('background-color', '#1b7181');
        $('#requests-btn').css('color', 'white');
    });
</script>
</#macro>

<#--
   <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

   <h3>Добавление темы</h3>
   <form method="post" action="/work_topics/create">
       <textarea cols="50" name="name" type="text" placeholder="Введите тему работы"></textarea>

       <p>Научное направление:</p>

       <select name="direction">
       <#list directions as direction>
           <option value="${direction.id}">${direction.name}</option>
       </#list>
       </select>
       <input type="submit" value="Создать тему">
   </form>

   <hr><hr>
   <h3>Мои темы:</h3>
   <hr>
   <#list work_topics as topic>
       <h4> ${topic.name}</h4>
       <p>Направление: ${topic.direction.name}</p>
       <#if topic.busy>
           <b>Выполняет: <a href="/user/${topic.student.id}">${topic.student.fullName}</a></b>
       <#else>
           <#if topic.activeRequests?has_content>
           <b>Заявки:</b>
           <ul>
               <#list topic.activeRequests as request>
               <li>
                   <p><a href="/user/${request.student.id}">${request.student.fullName}</a> <i>${request.date}</i></p>
                   <form method="post" action="/work_topics/request/${request.id}/approve">
                       <input type="submit" value="Подтвердить">
                   </form>
                   <form method="post" action="/work_topics/request/${request.id}/reject">
                       <input type="submit" value="Отклонить">
                   </form>
               </li>
               </#list>
           </ul>
           </#if>
       </#if>
       <hr>
   </#list>
   </div>
   -->