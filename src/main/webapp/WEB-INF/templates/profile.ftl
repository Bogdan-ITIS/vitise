<#include "base2.ftl">
<#macro title>Профиль</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/news-profile.css"></#macro>
<#macro content>
    <#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>
<div class="main-block">
    <div class="line1">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

            <div class="col-md-3 col-sm-4">
                <div class="col-md-12 photo-block">

                    <div class="photo-border">
                        <#if currentUser.photoName??&&currentUser.photoName!=""&&currentUser.photoName!="mock">
                            <img class="profile-photo" src="${currentUser.photoName}">
                        <#else>
                            <img class="profile-photo" src="/img/bg3.png" alt="">
                        </#if>
                        <!--<div class="profile-photo"-->
                        <!--style="background-color: #8A8490; width: 100%; height: 	100%;"></div>-->
                    </div>
                    <form method="post" action="/profile/upload_photo" enctype="multipart/form-data">
                        <input type="file" name="photoname" id="add-profile-photo">
                        <label for="add-profile-photo" class="upload-btn">Загрузить фото</label>
                        <button type="submit" class="save-btn">Сохранить</button>
                    </form>
                </div>
            </div>

            <div class="col-md-9 col-sm-8">
                <div class="col-md-12 info-block">
                    <h2 style="color: #1b6d85;">${currentUser.fullName}</h2>
                    <#if group??><p>Группа: &nbsp; <span class="about-text">${group.name}</span></p></#if>
                    <p>Телефон:<span class="about-text">${currentUser.telephoneNumber}</span></p>
                    <p>E-mail&nbsp;&nbsp; &nbsp; &nbsp;<span class="about-text">${currentUser.email}</ span></p>
                    <#if lab??><p>Лаборатория:  &nbsp; <a class="about-text" href="/laboratories/${lab.id}">${lab.name}</a></p></#if>
                    <#if currentUser.description?? && currentUser.description != ""><p>О себе:&nbsp; &nbsp;&nbsp;<span
                            class="about-text">
                    ${currentUser.description}
                    </span></p></#if>
                    <#if currentUser.statusName == "STUDENT" && !isStudentHaveTopic>
                    <h4 style="color: red">Тема курсовой не выбрана! Вы можете оставить заявки на темы <a href="/work_topics/search">здесь</a>.</h4>
                    </#if>
                    <a href="/profile/change">
                        <button class="modify-btn">Редактировать информацию</button>
                    </a>
                </div>
            </div>

        </div>
    </div> <!-- line1 -->

    <div class="line1">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
            <div class="col-md-12">
                <div class="col-md-12 post-block">
                    <form role="form" action="/news" id="post-form" name="post-form" method="post"
                          enctype="multipart/form-data">
                        <div class="round-photo">
                            <img src="/img/bg3.png" alt="">
                        </div>
                        <textarea id="text" name="text" cols="1" class="post-input"
                                  placeholder="Здесь вы можете написать свою новость"
                                  oninput="auto_grow(this)" required></textarea>
                        <div id="buttons" style="width: 100%;">
                            <div class="" style="width: 100%;">
                                <input class="checkbox" type="checkbox" name="" id="checkbox" hidden>
                                <label for="checkbox" class="check-label"></label>
                                <span class="remember-sign">Мероприятие</span>
                            </div>

                            <div class="datetime">
                                <input type="date" name="eventDate" id="date" required onchange="changeDate()">
                            </div>
                            <p class="signup-error-text" id="date-error">&nbsp;</p>

                            <div class="bottom-buttons">
                                <button type="submit" id="submit-btn" class="submit-btn">Опубликовать</button>
                                <label for="add-photo-btn" class="add-photo-label"><img src="/img/camera.png"
                                                                                        alt=""></label>
                                <input type="file" name="file" id="add-photo-btn">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="line1">
        <#if newses??>
            <#list newses as n>
                <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 post">
                    <div class="col-md-12">
                        <div class="col-md-12 post-block">
                            <div class="round-photo">
                                <img src="/img/camera.png" alt="">
                            </div>
                            <div class="post-info">
                                <h3 class="post-sign">${n.getAuthor().fullName}
                                </h3> <span>${n.getPubDate()}</span>
                                <#if group??><p class="post-sign">Студент группы  ${group.name}</p></#if>
                            </div>
                            <div class="post-content">
                                <p>${n.getText()}</p>
                                <#if n.getFiles()??>
                                    <#list n.files as f>
                                        <p><a href="${f.filename}" download>Скачать файл</a>
                                    <p><a href="${f.filename}">Открыть файл</a>
                                    </#list>
                                </#if>
                            </div>
                            <#if n.isEvent()>
                                <h4>Дата события: ${n.eventDate?date}</h4>
                                <p>Количество участников: ${n.subscriptionsCount}</p>
                                <form action="/events/subscribe" method="post">
                                    <input name="event_id" type="hidden" value="${n.getId()}"/>
                                    <#if n.subscribeStatus>
                                        <div id="news-buttons" style="width: 100%;">
                                            <input type="submit" value="Отписаться" id="news-submit-btn">
                                        </div>
                                    <#else>
                                        <div id="news-buttons" style="width: 100%;">
                                            <input type="submit" value="Записаться" id="news-submit-btn">
                                        </div>
                                    </#if>
                                </form>
                                <form action="/events/remove" method="post">
                                    <input name="event_id" type="hidden" style="display: none" value="${n.id}"/>
                                    <button type="submit" class="delete-btn" title="Удалить">
                                        Удалить
                                    </button>
                                </form>
                            <#else>
                                <form action="/news/remove" method="post">
                                    <input name="news_id" type="hidden" style="display: none" value="${n.id}"/>
                                    <button type="submit" class="delete-btn" title="Удалить">
                                        Удалить
                                    </button>
                                </form>
                            </#if>
                        </div>
                    </div>
                </div>
            </#list>
        </#if>
    </div>
</div>
    <#include "js_create_news.ftl">
</#macro>