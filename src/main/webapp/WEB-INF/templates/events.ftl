<#include "base2.ftl">
<#macro title>Мои события</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/news-profile.css"></#macro>
<#macro content>
<div class="main-block">
    <div class="line1">
        <#if events??>
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 post">
                <div class="col-md-12">
                    <h3>Мои события</h3>
                </div>
            </div>
            <#list events as e>
                <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 post">
                    <div class="col-md-12">
                        <div class="col-md-12 post-block">
                            <div class="round-photo">
                                <img src="/img/camera.png" alt="">
                            </div>
                            <div class="post-info">
                                <h3 class="post-sign">${e.getAuthor().fullName}
                                </h3> <span>${e.getPubDate()}</span>
                            </div>
                            <div class="post-content">
                                <p>${e.getText()}</p>
                            </div>

                            <h4>Дата события: ${e.eventDate?date}</h4>
                            <p>Количество участников: ${e.subscriptionsCount}</p>
                            <form action="/events/subscribe" method="post">
                                <input name="event_id" type="hidden" value="${e.getId()}"/>
                                <#if e.subscribeStatus>
                                    <div id="news-buttons" style="width: 100%;">
                                        <input type="submit" value="Отписаться" id="news-submit-btn">
                                    </div>
                                <#else>
                                    <div id="news-buttons" style="width: 100%;">
                                        <input type="submit" value="Записаться" id="news-submit-btn">
                                    </div>
                                </#if>
                            </form>
                            <form action="/events/remove" method="post">
                                <input name="event_id" type="hidden" style="display: none" value="${e.id}"/>
                                <button type="submit" class="delete-btn" title="Удалить">
                                    Удалить
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </#list>
        </#if>
    </div>
</div>
</#macro>