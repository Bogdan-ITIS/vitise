<#include "base2.ftl">
<#macro title>Поиск</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/colleagues.css">
<link rel="stylesheet" href="/css/dialogs.css">
</#macro>
<#macro content>
<div class="main-block">
    <div class="line0">
        <div class="col-xs-12 col-sm-3 col-sm-push-9 col-md-3 col-md-push-8 col-lg-2 col-lg-push-9 line0">
            <div class="col-xs-12">
                <div class="col-md-12 news-nav">
                    <select name="entity" id="entity" oninput="setEntity()">
                        <#list entities as entity>
                            <option value="${entity.simpleName}">${entity.rusName}</option>
                        <#else>
                        </#list>
                    </select>
                    <select name="direction" id="direction" oninput="setDirection()">
                        <option value="all">Все</option>
                        <#list directions as direction>
                            <option value="${direction.id}">${direction.name}</option>
                        <#else>
                        </#list>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-7 col-md-offset-1 col-md-pull-3 col-lg-8 col-lg-offset-1 col-lg-pull-2 post-news">
            <#if currentUser.statusName == "DEANERY">
                <div class="col-xs-12 buttons">
                    <a href="/deanery/laboratories/create">Создать лабораторию</a>
                </div>
            </#if>
            <div id="entities">
            </div>
        </div>
        <script>


            var direction;
            var entity;

            $(document).ready(function () {
                setDirection();
                setEntity();
                search();
            });

            function search() {
                $.post("/search/" + entity + "/" + direction, {},
                        onAjaxSuccess
                );
            }

            function onAjaxSuccess(data) {
                var output = "";
                if (data.length == 0) {
                    document.getElementById("entities").innerHTML = output;
                }
                else {
                    for (var i = 0; i < data.length; i++) {
                        var href = "";
                        var body = '<div class="col-md-12 post-block">';
                        if (data[i].type == 'LABORATORY') {
                            href = "/laboratories/" + data[i].id;
                        }
                        else {
                            href = "/user/" + data[i].id;
                            body = body + '<div class="round-photo"> <img src="/img/camera.png" alt=""> </div>'
                        }
                        body = body + '<div class="post-info"> <h3 class="post-sign" style="margin-right: 20px">';
                        output = output + body + '<a href="' + href + '">' + data[i].name + '</a></h3></div></div>';
                    }
                    document.getElementById("entities").innerHTML = output;
                }
            }

            function setEntity() {
                var sel = document.getElementById("entity");
                entity = sel.options[sel.selectedIndex].value;
                search();
            }

            function setDirection() {
                var sel = document.getElementById("direction");
                direction = sel.options[sel.selectedIndex].value;
                search();
            }
        </script>
    </div>
</div>
</#macro>