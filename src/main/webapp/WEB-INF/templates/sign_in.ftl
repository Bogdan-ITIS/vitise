<#include "base1.ftl">
<#macro title>Вход</#macro>
<#macro content>
    <#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>
    <#assign c=JspTaglibs["http://www.springframework.org/security/tags"]>

<div class="main-block">

    <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 login-block">
        <h1 class="title">Вход в систему</h1>

        <div class="modal-form" id="modal-form">
            <a href="/sign_up/stud"><p>Студент</p></a>
            <a href="/sign_up/worker"><p>Сотрудник</p></a>
        </div>

        <#if error??>
            <div class="error">
                <p>${SPRING_SECURITY_LAST_EXCEPTION.message}</p>
            </div>
        </#if>


        <@sf.form action='/login/process' method="post" modelAttribute="authForm">
            <fieldset>
                <p class="input-label">E-mail</p>
                <div class="input-wrapper">
                    <@sf.input path="email" type="text" class="login-input" placeholder="Введите e-mail"/>
                    <@sf.errors path="email" cssClass="help-block"/>
                </div>

                <p>&nbsp; </p>

                <p class="input-label">Пароль</p>
                <div class="input-wrapper">
                    <@sf.input path="password" type="password" class="pass-input" placeholder="Введите пароль"/>
                    <@sf.errors path="password" cssClass="help-block"/>
                </div>

                <div class="col-md-12 line">
                    <input class="checkbox" type="checkbox" name="rememberMe" id="checkbox" hidden>
                    <label for="checkbox" class="check-label"></label>
                    <span class="remember-sign">Запомнить меня</span>
                    <a data-toggle="modal" data-target="#forgot-password-modal"><span
                            class="forgot-sign">Забыли пароль?</span></a>
                </div>
                <div class="col-md-12 line">
                    <input type="submit" value="Войти" class="login-btn" id="login-btn">
                </div>
            </fieldset>
        </@sf.form>
        <div class="col-md-12">
            <h3 class="links"><span id="signup-btn">Регистрация</span></h3>
        </div>
    <#--<div class="col-md-12">-->
    <#--<p class="error-text">Пользователя с таким e-mail не существует</p>-->
    <#--</div>-->
    </div>

    <!-- Modal -->
    <div class="modal fade" id="forgot-password-modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Восстановление пароля</h4>
                </div>

                <form action='/password_recovery' method="post">
                    <div class="modal-body">
                        <p>Введите свою почту. На нее будет выслан новый пароль</p>
                        <input name="email" type="text" class="login-input" placeholder="Введите e-mail" autofocus>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
                        <button type="submit" class="btn btn-primary">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<script>
    $("#signup-btn").click(function () {
        $("#modal-form").show();
    })

    var mouse_is_inside = false;
    $(document).ready(function () {
        $('#modal-form').hover(function () {
            mouse_is_inside = true;
        }, function () {
            mouse_is_inside = false;
        });

        $("body").mouseup(function () {
            if (!mouse_is_inside) $('#modal-form').hide();
        });
    });
</script>
</#macro>



