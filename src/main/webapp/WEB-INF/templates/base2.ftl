<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><@title/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link rel="stylesheet" href="/css/base.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<@link/>
    <script src="/js/jquery-3.1.1.min.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/js/jquery.maskedinput.js" type="text/javascript"></script>
    <script src="/js/soundmanager2-jsmin.js"></script>
    <script src="/js/sockjs.min.js"></script>
    <script src="/js/stomp.min.js"></script>
</head>
<body>


<nav class="navbar navbar-default navbar-fixed-top header">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <!--<i class="fa fa-bars fa-2x" aria-hidden="true"></i>-->
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img src="/img/logo1.png" alt="" class="logo">
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav" style="margin-left: 50px;">
                <li><a href="/news">Новости</a></li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">Деканат <span class="caret"></span></a>
                    <ul class="dropdown-menu green-menu">
                    <#if currentUser.statusName == "DEANERY">
                        <li><a href="/deanery/references_requests">Заявки на справки</a></li>
                        <li><a href="/deanery/laboratories/requests">Заявки в лаборатории</a></li>
                        <li><a href="/deanery/courses/requests">Заявки на курсы</a></li>
                        <li><a href="/sign_up_request">Заявки на регистрацию</a></li>
                    <#else>
                        <li>
                            <a class="sertificate-btn" data-toggle="modal"
                                    data-target="#sertificateModal">Получить справку
                            </a>
                        </li>
                    </#if>
                    </ul>
                </li>
            </ul>

            <ul class="nav navbar-nav navbar-right padded-right">
                <li class="dropdown navbar-right">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false"><@name/>
                        <span id="notifications-number"></span>
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/profile">Моя страница</a></li>
                        <li><a href="/colleagues">Коллеги<span id="requests-number"></span></a></li>
                        <li><a href="/messages/all">Сообщения<span id="messages-number"></span></a></li>
                        <li><a href="/events">События</a></li>
                        <li><a href="/work_topics">Курсовые<span id="topic-requests-number"></span></a></li>
                        <li><a href="/courses">Курсы</a></li>
                        <li><a href="/search">Лаборатории</a></li>
                        <li><a href="/users">Пользователи</a></li>
                        <li><a href="/profile/change">Настройки</a></li>
                        <li><a href="/support">Поддержка</a></li>
                        <li><a href="/logout">Выход</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>

</nav>
<@content/>
<div id="sertificateModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header no-border">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h3 class="modal-title center">Заявка на получение справки</h3>
            </div>
            <form action="/references_requests/create" method="post">
                <div class="modal-body no-border">
                    <h4> &nbsp; Количество: &nbsp;</h4>
                    <select name="" id="">
                        <option value="">1</option>
                        <option value="">2</option>
                        <option value="">3</option>
                        <option value="">4</option>
                        <option value="">5</option>
                    </select>
                </div>
                <div class="modal-footer no-border">
                    <button type="button" class="cancel-btn" data-dismiss="modal">Отменить</button>
                    <button type="submit" class="send-btn">Отравить</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="footer">
    <h4 class="footer-text">(c) 2017 Казанский (Приволжский ) Федеральный Университет</h4>
    <div class="social">
        <a class="img-link" href="https://www.instagram.com/itis_kfu/"><img src="/img/insta-icon.png" alt=""></a>
        <a class="img-link" href="https://vk.com/itis_kpfu"><img src="/img/vk-icon.png" alt=""></a>
        <a class="img-link" href="https://twitter.com/future__it"><img src="/img/twitter-icon.png" alt=""></a>
    </div>
</div>

<script>
    var messages = 0;
    var requests = 0;
    var topic_requests = 0;
    var notifications = messages + requests + topic_requests;
    var prev_notifications = 0;

    var getNumberOfRequestsOnLoad = function () {
        $.ajax({
            'url': '/colleagues/get_number',
            'data': {
                'requests': 0
            },
            'type': 'get',
            'success': function (json) {
                if (json > 0) {
                    $("#requests-number").text(' (' + json + ')');
                    requests = json;
                    prev_notifications = notifications;
                    notifications = requests + messages + topic_requests;

                    $('#notifications-number').text(' (' + notifications + ')');
                }
                if (json == 0)
                    requests = 0;
            },
            'complete': function (json) {
                getNumberOfRequests()
            }
        })
    };

    var getNumberOfMessagesOnLoad = function () {
        $.ajax({
            'url': '/messages/get_number',
            'data': {
                'messages': 0
            },
            'type': 'get',
            'success': function (json) {
                if (json > 0) {
                    $("#messages-number").text(' (' + json + ')');
                    messages = json;
                    prev_notifications = notifications
                    notifications = requests + messages + topic_requests;

                    $('#notifications-number').text(' (' + notifications + ')');
                    if (json == 0) {
                        topics_requests = 0;
                    }
                }
            },
            'complete': function (json) {
                getNumberOfMessages();
            }
        })
    };

    var getNumberOfTopicRequestsOnLoad = function () {
        $.ajax({
            'url': '/work_topics/get_requests_number',
            'data': {
                'topic_requests': 0
            },
            'type': 'get',
            'success': function (json) {
                if (json > 0) {
                    $("#topic-requests-number").text(' (' + json + ')');
                    topic_requests = json;
                    prev_notifications = notifications
                    notifications = requests + messages + topic_requests;

                    $('#notifications-number').text(' (' + notifications + ')');
                    if (json == 0) {
                        topic_requests = 0;
                    }
                }
            },
            'complete': function (json) {
                getNumberOfTopicRequests();
            }
        })
    };

    var getNumberOfRequests = function () {
        $.ajax({
            'url': '/colleagues/get_number',
            'data': {
                'requests': 0
            },
            'type': 'get',
            'success': function (json) {
                if (json > 0) {
                    $("#requests-number").text(' (' + json + ')');
                    requests = json;
                    prev_notifications = notifications;
                    notifications = requests + messages + topic_requests;

                    if (notifications > prev_notifications) {
                        messageSound.play();
                        prev_notifications = notifications
                    }

                    $('#notifications-number').text(' (' + notifications + ')');
                }
                if (json == 0)
                    requests = 0;
            },
            'complete': function (json) {
                setTimeout(getNumberOfRequests, 5000);
            }
        })
    };


    var getNumberOfMessages = function () {
        $.ajax({
            'url': '/messages/get_number',
            'data': {
                'messages': 0
            },
            'type': 'get',
            'success': function (json) {
                if (json > 0) {
                    $("#messages-number").text(' (' + json + ')');
                    messages = json;
                    prev_notifications = notifications
                    notifications = requests + messages + topic_requests;

                    if (notifications > prev_notifications) {
                        messageSound.play();
                        prev_notifications = notifications
                    }

                    $('#notifications-number').text(' (' + notifications + ')');
                    if (json == 0) {
                        messages = 0;
                    }
                }
            },
            'complete': function (json) {
                setTimeout(getNumberOfMessages, 5000);
            }
        })
    };

    var getNumberOfTopicRequests = function () {
        $.ajax({
            'url': '/work_topics/get_requests_number',
            'data': {
                'topic_requests': 0
            },
            'type': 'get',
            'success': function (json) {
                if (json > 0) {
                    $("#topic-requests-number").text(' (' + json + ')');
                    topic_requests = json;
                    prev_notifications = notifications;
                    notifications = requests + messages + topic_requests;

                    if (notifications > prev_notifications) {
                        messageSound.play();
                        prev_notifications = notifications
                    }

                    $('#notifications-number').text(' (' + notifications + ')');
                }
                if (json == 0)
                    topic_requests = 0;
            },
            'complete': function (json) {
                setTimeout(getNumberOfTopicRequests, 5000);
            }
        })
    };

    var messageSound;
    soundManager.setup({
        url: '/sounds/message.mp3',
        onready: function () {
            messageSound = soundManager.createSound({
                id: 'aSound',
                url: '/sounds/message.mp3'
            });
        },
        ontimeout: function () {
        }
    });

    $(document).ready(function () {
        getNumberOfRequestsOnLoad();
        getNumberOfMessagesOnLoad();
        getNumberOfTopicRequestsOnLoad();
        if (notifications > 0)
            prev_notifications = notifications;
        $('#notifications-number').text(' (' + notifications + ')');
    });

</script>

</body>
</html>