<#include "base2.ftl">
<#macro title>Заявки в лаборатории</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/dialogs.css">
<link rel="stylesheet" href="/css/requests.css">
</#macro>
<#macro content>
<div class="main-block">
    <div class="line2" style="padding-top: 30px">
        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
            <div class="col-xs-12">
                <div class="col-xs-12 input-wrapper">
                    <a href="#"><i class="fa fa-lg fa-chevron-left" aria-hidden="true"></i> &nbsp; &nbsp;</a>
                    <h4 id="title"
                        style="color:rgba(7, 84, 133, 0.8); overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                        Заявки в лаборатории</h4>
                    <input type="text" placeholder="Начните вводить имя или фамилию" class="search-input1"
                           id="message-search-field">
                    <a href="#" id="search-message-btn"><i class="fa fa-lg fa-search" aria-hidden="true"></i></a>
                    <a href="#" id="cancel-search-btn" hidden><i class="fa fa-lg fa-times" aria-hidden="true"></i></a>
                </div>
            </div>
        </div>
    </div>

    <div class="line2">
        <div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2" id="messages-block">
            <div class="col-xs-12">
                <div class="col-xs-12 messages-items-block1">
                    <#list requests as req>
                        <div class="message-item">
                            <div class="round-photo">
                                <img src="/img/mal.png" alt="">
                            </div>
                            <div class="message-text">
                                <h4><a href="/user/${req.user.id}">${req.user.fullName}</a></h4>
                                <form method="post" action="/deanery/laboratories/${req.id}/delete">
                                    <input type="submit" value="Отклонить"
                                           class="news-submit-btn">
                                </form>
                                <form method="post" action="/deanery/laboratories/${req.id}/approve">
                                    <input type="submit" value="Одобрить"
                                           class="news-submit-btn">
                                </form>
                                <h5>${req.user.group.name}</h5>
                                <h5>Заявка на поступление в лабораторию <a href="/laboratories/${req.laboratory.id}">${req.laboratory.name}</a></h5>
                            </div>
                        </div>
                    </#list>
                </div>
            </div>
        </div>

    </div>

</div>
<script>
    $('.friend').click(function () {
        $(this).find('input[type=checkbox]').prop("checked", !$(this).find('input[type=checkbox]').prop("checked"));
    });

    function auto_grow(element) {
        element.style.height = "auto";
        element.style.height = (element.scrollHeight) + "px";
    }

    $('.post-input').keypress(function () {
        $('#buttons').show();
    });

    $('#submit-btn').click(function () {
        $('#buttons').hide();
        $('.post-input').val('');
    });


    $(function () {
        $('.messages-items-block').scrollTop(1E10);
    });

    $('#search-message-btn').click(function () {
        $(this).hide();
        $('#title').hide();
        $('#message-search-field').show('fast');
        $('#cancel-search-btn').show();
    });

    $('#cancel-search-btn').click(function () {
        $(this).hide();
        $('#title').show('fast');
        $('#message-search-field').hide();
        $('#search-message-btn').show();
    });


</script>

</#macro>
