<#include "base2.ftl">
<#macro title>Лаборатория ${laboratory.name}</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/news-profile.css"></#macro>
<#macro content>
    <#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>
<div class="main-block">
    <div class="line1">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

            <div class="col-md-3 col-sm-4">
                <div class="col-md-12 photo-block">
                    <div class="photo-border">
                        <img class="profile-photo" src="/img/bg3.png" alt="">
                    </div>
                </div>
            </div>

            <div class="col-md-9 col-sm-8">
                <div class="col-md-12 info-block">
                    <h2 style="color: #1b6d85;">${laboratory.name}</h2>
                    <p>Информация о лаборатории: &nbsp; &nbsp;&nbsp;<span
                            class="about-text">${laboratory.description}</span></p>
                    <p>Научный руководитель: &nbsp; &nbsp;&nbsp;<a href="/user/${laboratory.head.id}">${laboratory.head.fullName}</a></p>
                    <p>Направление: &nbsp; &nbsp;&nbsp;<a href="#">${laboratory.direction.name}</a></p>
                    <#if is_student>
                        <#if state == 0>
                            <form method="post" action="/laboratories/enroll">
                                <input value="${laboratory.id}" name="id" hidden>
                                <button type="submit" class="modify-btn">Записаться в лабораторию</button>
                            </form>
                        <#elseif state == 1>
                            <form method="post" action="/laboratories/${laboratory.id}/decline">
                                <input hidden name="id" value="${request_id}">
                                <button type="submit" class="modify-btn">Отменить заявку</button>
                            </form>
                        <#elseif state == 2>
                            <p>Вы уже состоите в этой лаборатории</p>
                        <#else>
                            <p>Вы не можете подать заявку</p>
                        </#if>
                    </#if>
                </div>
            </div>

        </div>
    </div> <!-- line1 -->

    <div class="line1">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 post">
            <div class="col-md-12">
                <div class="col-md-12 post-block">
                    <#if students?has_content>
                        <h3>Студенты в лаборатории (${students_amount})</h3>
                        <#list students as s>
                            <h4><a href="/user/${s.id}">${s.fullName}</a></h4>
                        </#list>
                    <#else>
                        <h3>Нет студентов в лабораотрии. Запишись, будь первым!</h3>
                    </#if>
                </div>
            </div>
        </div>
    </div>
</div>
</#macro>