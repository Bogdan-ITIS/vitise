<#include "base2.ftl">
<#macro title>Курсовые</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
    <link rel="stylesheet" href="/css/fist_of_course_works.css">
</#macro>
<#macro content>
    <#assign sf=JspTaglibs["http://www.springframework.org/tags/form"]>

<div class="main-block">

    <div class="main-block">

        <div class="line1">
        <div class="col-md-6 col-md-offset-3">
            <div class="col-sm-12 ">
                <div class="col-xs-12 post-block">

                    <h3><a href="/work_topics/search">Поиск по свободным темам</a></h3><hr>

                    <#if studentTopic??>
                        <h3>Моя тема:</h3><hr>

                        <div class="theme">
                            <h3 class="theme-name">${studentTopic.name}</h3>
                            <p class="theme-direction">Направление:  ${studentTopic.direction.name}</p>
                            <p class="theme-direction">Руководитель: <a href="/user/${studentTopic.creator.id}"> ${studentTopic.creator.fullName}</a></p>
                        </div>

                    <#else>
                        <#if userRequests?has_content>
                            <h3>Мои заявки:</h3><hr>
                            <#list userRequests as request>

                                <div class="theme">
                                    <h3 class="theme-name">Тема: ${request.topic.name}</h3>
                                    <p class="theme-direction">Направление:  ${request.topic.direction.name}</p>
                                    <p class="theme-direction">Руководитель: <a href="/user/${request.topic.creator.id}"> ${request.topic.creator.fullName}</a></p>
                                    <p class="theme-direction">Дата подачи заявки: <i>${request.date}</i></p>

                                    <#if request.confirmed??>
                                        <p class="theme-direction">Статус: заявка отклонена</p>
                                    <#else>
                                        <p class="theme-direction">Статус: активна</p>
                                        <form method="post" action="/work_topics/delete_choose">
                                            <input type="hidden" name="request_id" value="${request.id}">
                                            <button class="choose-btn" type="submit">Отменить заявку</button>
                                        </form>
                                    </#if>
                                </div>
                            </#list>
                        </#if>
                    </#if>

                </div>
            </div>
        </div>
        </div>
    </div>

</#macro>