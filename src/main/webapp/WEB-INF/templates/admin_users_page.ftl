<#include "base2.ftl">
<#macro title>Профиль</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/colleagues.css">
<link rel="stylesheet" href="/css/dialogs.css">
</#macro>
<#macro content>
<div class="main-block">


    <div class="line0">

        <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-7 col-md-offset-5 col-md-pull-3 col-lg-8 col-lg-offset-4 col-lg-pull-2 ">
            <div class="col-xs-12">
                <div class="col-xs-12 input-wrapper">

                    <form action="/admin/users/filter">

                        <a href="#"><i class="fa fa-lg fa-search" aria-hidden="true"></i></a>
                        <input type="text" name="searchField">
                        <select name="status">
                            <#if statusNames?size gt 1>
                                <option value="all">Все</option>
                            </#if>
                            <#list statusNames as statusName>
                                <option value="${statusName.ordinal()}">${statusName.getNameOnRus()}</option>
                            <#else>
                            </#list>
                        </select>
                        <select name="isActive">
                            <option value="null">Все</option>
                            <option value="true">Активные</option>
                            <option value="false">Архивированные</option>
                        </select>
                        <input type="submit" value="Фильтр">

                    </form>

                </div>
            </div>
        </div>
    </div>

    <div class="line0">

        <div class="col-xs-12 col-sm-9 col-sm-pull-3 col-md-7 col-md-offset-5 col-md-pull-3 col-lg-8 col-lg-offset-4 col-lg-pull-2 post-news">
            <div class="col-md-12" id="students">

                <#list users as u>

                    <div class="col-md-12 post-block">
                        <div class="round-photo">
                            <img src="/img/camera.png" alt="">
                        </div>
                        <div class="post-info">
                            <h3 class="post-sign" style="margin-right: 20px"><a href="/user/${u.id}">${u.fullName}</a>
                            </h3>
                        </div>
                        <form action="/admin/users/archive" method="post">
                            <input type="hidden" name="id" value="${u.id}">
                            <input type="submit" id="news-submit-btn"
                                   <#if u.isActive()>style="background-color: red" value="Архивировать"
                                   <#else> style="background-color: green" value="Разархивировать"</#if>>
                        </form>
                    </div>

                <#else>
                    Нет пользователей
                </#list>

            </div>
        </div>
    </div>

</div>
</#macro>