<#include "base2.ftl">
<#macro title>Лаборатории</#macro>
<#macro name>${currentUser.name}</#macro>
<#macro link>
<link rel="stylesheet" href="/css/news-profile.css"></#macro>
<#macro content>
<div class="main-block">
    <div class="line1">
        <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 post">
            <div class="col-md-12">
                <h3>Лаборатории</h3>
            </div>
            <#if currentUser.statusName == "DEANERY">
                <div class="col-xs-12 buttons">
                    <a href="/deanery/laboratories/create">Создать лабораторию</a>
                </div>
            </#if>
        </div>
        <#list laboratories as lab>
            <div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 post">
                <div class="col-md-12">
                    <div class="col-md-12 post-block">
                        <div class="post-info">
                            <h3 class="post-sign"><a href="/laboratories/${lab.id}">${lab.name}</a></h3>
                            <h5>Научное направление: ${lab.direction.name}</h5>
                        </div>
                    </div>
                </div>
            </div>
        </#list>
    </div>
</div>
</#macro>